-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 27, 2018 at 08:10 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chhaimale`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `name`, `image`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(2, 'BBQ', 'DpFzjWHN.jpg', '1', '1', '2018-06-27 22:02:22', '2018-08-03 04:34:47'),
(3, 'Boating', 'Vr21UdfQ.jpg', '1', '1', '2018-07-31 22:11:33', '2018-08-03 04:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `amenities`
--

CREATE TABLE `amenities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) NOT NULL,
  `edited_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `amenities`
--

INSERT INTO `amenities` (`id`, `name`, `image`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(1, 'Free Wifi', '70WjzWg9.png', 1, 1, NULL, '2018-07-31 23:16:50'),
(2, 'Bathroom', 'tMukenr2.png', 1, 1, '2018-07-31 23:13:14', '2018-07-31 23:13:14');

-- --------------------------------------------------------

--
-- Table structure for table `bookedrooms`
--

CREATE TABLE `bookedrooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `room_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rooms_type_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_charge` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_in` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `check_out` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occupancy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `org` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookedrooms`
--

INSERT INTO `bookedrooms` (`id`, `room_id`, `rooms_type_number`, `sub_total`, `service_charge`, `vat`, `total`, `check_in`, `check_out`, `occupancy`, `first_name`, `last_name`, `address1`, `address2`, `city`, `zip`, `country`, `phone`, `email`, `org`, `payment_method`, `card_type`, `card_number`, `card_month`, `card_year`, `created_at`, `updated_at`) VALUES
(7, '[2,3]', '[{\"standard_room123\":[\"4\"]},{\"group_room\":[\"2\"]}]', '27002', '2700.2000000000003', '3861.286', '33563.486000000004', '1970-01-01', '1970-01-01', '[{\"standard_room123_adult\":\"1\",\"standard_room123_child\":\"0\",\"standard_room123_children\":\"0\"},{\"group_room_adult\":\"1\",\"group_room_child\":\"0\",\"group_room_children\":\"0\"}]', 'a', 'a', 'a', 'a', 'a', 'a', 'NP', '21', 'admin@sms.com', 'as', 'NA', 'NA', 'NA', 'NA', 'NA', '2018-09-02 02:58:00', '2018-10-03 05:05:52'),
(8, '[2,3]', '[{\"standard_room123\":[\"4\"]},{\"group_room\":[\"2\",\"3\"]}]', '58004', '5800.400000000001', '8294.572', '58004', '09/02/2018', '09/03/2018', '[{\"standard_room123_adult\":\"1\",\"standard_room123_child\":\"0\",\"standard_room123_children\":\"0\"},{\"group_room_adult\":\"1\",\"group_room_child\":\"0\",\"group_room_children\":\"0\"}]', 'a', 'a', 'a', 'a', 'a', '121', 'NP', '123', 'admin@gmail.com', 'asdad', 'cashpayment', 'NA', 'NA', 'NA', 'NA', '2018-09-02 03:04:33', '2018-09-02 03:04:33'),
(9, '[4]', '[{\"wooden_room\":[\"5\"]}]', '4500', '450', '643.5', '4500', '09/05/2018', '09/07/2018', '[{\"wooden_room_adult\":\"1\",\"wooden_room_child\":\"0\",\"wooden_room_children\":\"0\"}]', 'a', 'a', 'a', 'a', 'a', 'a', 'NP', '12', 'admin@gmail.com', 'asa', 'cashpayment', 'NA', 'NA', 'NA', 'NA', '2018-09-03 01:11:00', '2018-09-03 01:11:00'),
(10, '[2,3]', '[{\"standard_room123\":[\"4\"]},{\"group_room\":[\"2\",\"3\"]}]', '58004', '5800.400000000001', '8294.572', '58004', '09/03/2018', '09/04/2018', '[{\"standard_room123_adult\":\"1\",\"standard_room123_child\":\"0\",\"standard_room123_children\":\"0\"},{\"group_room_adult\":\"1\",\"group_room_child\":\"0\",\"group_room_children\":\"0\"}]', 'a', 'a', 'a', 'a', 'a', 'a', 'a', '21', 'admin@gmail.com', 'asda', 'cashpayment', 'a', 'a', 'a', 'a', '2018-09-03 05:41:36', '2018-09-03 05:41:36'),
(11, '[2]', '[{\"standard_room123\":[\"4\"]}]', '50004', '5000.400000000001', '7150.572', '50004', '09/05/2018', '09/06/2018', '[{\"standard_room123_adult\":\"1\",\"standard_room123_child\":\"0\",\"standard_room123_children\":\"0\"}]', 'Sarita', 'Shrestha', '123 Kusunti Rd', 'NA', 'Kathmandu', 'NA', 'NP', '9841322124', 'saritas@gmail.com', 'NA', 'cashpayment', 'NA', 'NA', 'NA', 'NA', '2018-09-05 17:56:18', '2018-09-05 17:56:18'),
(12, '[2]', '[{\"standard_room123\":[\"4\"]}]', '50004', '5000.400000000001', '7150.572', '50004', '09/16/2018', '09/17/2018', '[{\"standard_room123_adult\":\"1\",\"standard_room123_child\":\"0\",\"standard_room123_children\":\"0\"}]', 'NAYANA', 'SHRESTHA', '123 KTM', 'NA', 'KATHMANDU', 'NA', 'NP', '9841223321', 'NAYANAS@GMAIL.COM', 'NA', 'cashpayment', 'NA', 'NA', 'NA', 'NA', '2018-09-16 10:48:00', '2018-09-16 10:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `time_from` date DEFAULT NULL,
  `time_to` date DEFAULT NULL,
  `additional_information` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `room_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `time_from`, `time_to`, `additional_information`, `created_at`, `updated_at`, `deleted_at`, `customer_id`, `room_id`) VALUES
(1, '2018-08-23', '2018-08-30', 'dasda', '2018-08-23 03:54:36', '2018-08-23 03:54:36', NULL, 1, 3),
(2, '2018-08-23', '2018-08-31', 'dasda', '2018-08-23 03:54:36', '2018-08-23 03:54:36', NULL, 0, 2),
(3, '2018-08-28', '2018-08-29', NULL, NULL, NULL, NULL, 1, 4),
(4, '2018-08-28', '2018-08-29', NULL, NULL, NULL, NULL, NULL, 5),
(8, '2018-09-02', '2018-09-03', NULL, NULL, NULL, NULL, 2, 4),
(9, '2018-09-02', '2018-09-03', NULL, NULL, NULL, NULL, 1, 2),
(10, '2018-09-02', '2018-09-03', NULL, NULL, NULL, NULL, 1, 3),
(11, '2018-09-05', '2018-09-07', NULL, NULL, NULL, NULL, NULL, 5),
(12, '2018-09-03', '2018-09-04', NULL, NULL, NULL, NULL, NULL, 4),
(13, '2018-09-03', '2018-09-04', NULL, NULL, NULL, NULL, NULL, 2),
(14, '2018-09-03', '2018-09-04', NULL, NULL, NULL, NULL, NULL, 3),
(15, '2018-09-05', '2018-09-06', NULL, NULL, NULL, NULL, NULL, 4),
(16, '2018-09-16', '2018-09-17', NULL, NULL, NULL, NULL, NULL, 4),
(20, '1970-01-01', '1970-01-01', NULL, NULL, NULL, NULL, NULL, 4),
(21, '1970-01-01', '1970-01-01', NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resort_contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `office_contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_details`
--

INSERT INTO `company_details` (`id`, `name`, `address`, `resort_contact`, `office_contact`, `email`, `facebook_link`, `instagram_link`, `google_link`, `twitter_link`, `map_link`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(1, 'CHHAIMALE RESORT', 'Ramche Bhanjyang-6,Kathmandu  City Office: Newa:Nuga:Complex,  Om Bahal,Kathmandu,Nepal', '01-6924909/9851181409', '+977-4268121/Fax +977 4265205', 'booking@chhaimaleresort.com', 'fb.com Update', 'insta.com Update', 'googlelnk Update', 'twitter_link Update', 'maps.google.com Update', '1', '1', NULL, '2018-08-05 12:04:40');

-- --------------------------------------------------------

--
-- Table structure for table `conferences`
--

CREATE TABLE `conferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `org` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cfname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attendance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startdate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `starttime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `endtime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enddate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `setup_suggestion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breakfasttime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lunchtime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dinnertime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meal_suggestion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `standard_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `standard_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wooden_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wooden_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tented_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tented_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room_suggestion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) DEFAULT '0',
  `edited_by` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `paymentmethod` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `conferences`
--

INSERT INTO `conferences` (`id`, `title`, `first_name`, `last_name`, `address1`, `address2`, `city`, `zip`, `country`, `phone`, `email`, `org`, `cfname`, `attendance`, `startdate`, `starttime`, `endtime`, `enddate`, `style`, `setup_suggestion`, `breakfasttime`, `lunchtime`, `dinnertime`, `meal_suggestion`, `standard_room_occupant`, `standard_room_quantity`, `wooden_room_occupant`, `wooden_room_quantity`, `tented_room_occupant`, `tented_room_quantity`, `group_room_occupant`, `group_room_quantity`, `room_suggestion`, `added_by`, `edited_by`, `created_at`, `updated_at`, `paymentmethod`, `card_type`, `card_number`, `card_month`, `card_year`) VALUES
(2, 'mr', 'assadsa', 'dasasd', 'asdsadsa', 'asdsad', 'adasasda', 'asdasdas', 'NP', '1221', 'admin@gmail.com', '22', 'asdadsad', '21', '08/16/2018', '[\"asdadsa\",\"asdasd\"]', '[\"sadasda\",\"as\"]', '08/17/2018', '[\"layout2\",\"layout3\"]', 'asdadasdas', '[\"2\",\"1\"]', '[\"2\",\"1\"]', '[\"2\",\"1\"]', 'asdasdsa', 'double', '2', 'single', '2', 'tripple', '3', 'more', '12', 'asdadsa', 0, 0, '2018-08-16 02:28:34', '2018-08-16 02:28:34', 'credit-card', '121', '1223', 'jan', '2018'),
(3, 'mr', 'Kundan', 'karna', 'Koteshwor', 'ktm', 'ktm', '000977', 'NP', '9843581383', 'admin@gmail.com', 'Prabidhi Labs', 'Annual Conference', '100', '08/18/2018', '[\"10\",\"10\",\"9\",\"8\"]', '[\"5\",\"5\",\"6\",\"4\"]', '08/21/2018', '[\"layout1\",\"layout2\",\"layout3\",\"layout2\"]', 'Please, Get everything ready in time.', '[\"10\",\"9\",\"7\",\"6\"]', '[\"2\",\"2\",\"1\",\"12\"]', '[\"5\",\"6\",\"6\",\"5\"]', 'Same', 'double', '5', 'tripple', '10', 'tripple', '4', 'more', '10', 'asdasda', 0, 0, '2018-08-16 03:14:16', '2018-08-16 03:14:16', 'credit-card', '12113', '12222222', 'Dec', '2028'),
(4, 'mr', 'asdadas', 'asdadsa', 'asdasda', 'NA', 'asdasda', 'asdasda', 'NP', '122', 'admn@gmail.com', 'asdasd', 'ssasdsada', '212', '08/16/2018', '[null,null]', '[null,null]', '08/17/2018', 'null', 'test', '[null,null]', '[null,null]', '[null,null]', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'NA', 0, 0, '2018-08-16 04:37:54', '2018-08-16 04:37:54', 'credit-card', '1221', '2222', 'Oct', '2019'),
(5, 'mr', 'asdadas', 'dadad', 'asdad', 'sadasda', 'asadsadas', 'qsasdsa', 'NP', '1211', 'admiN@gmail.com', '122131', 'test', '121', '08/16/2018', '[\"asdada\",null]', '[\"12\",null]', '08/17/2018', '[\"layout3\"]', 'NA', '[\"12\",null]', '[\"2\",null]', '[\"2\",null]', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'NA', 0, 0, '2018-08-16 04:56:17', '2018-08-16 04:56:17', 'cashpayment', 'NA', 'NA', 'NA', 'NA'),
(6, 'mr', 'asdadas', 'dasasdas', 'dasdada', 'dasdsaasd', 'asdasdas', 'sadadssa', 'NP', '1211', 'admin@gmail.com', 'asdasda', 'aadsad', '1', '08/16/2018', '[null,null]', '[null,null]', '08/17/2018', 'null', 'NA', '[null,null]', '[null,null]', '[null,null]', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'NA', 0, 0, '2018-08-16 04:58:14', '2018-08-16 04:58:14', 'cashpayment', 'NA', 'NA', 'NA', 'NA'),
(7, 'mr', 'asdasd', 'asdasdas', 'asdasda', 'asdasdsa', 'asdadssa', 'asdasdas', 'NP', '121', 'admin@gmail.com', 'ASDASDAS', 'Test', '100', '08/18/2018', '[\"12\",\"2\"]', '[\"1\",\"5\"]', '08/19/2018', '[\"layout1\",\"layout3\"]', 'TEst', '[\"12\",\"1\"]', '[\"2\",\"23\"]', '[\"5\",\"3\"]', 'asd', 'double', '1', 'double', '2', 'tripple', '3', 'more', '4', 's', 0, 0, '2018-08-17 06:32:59', '2018-08-17 06:32:59', 'credit-card', '122', '2222', 'NA', 'NA'),
(8, 'mr', 'asdadsa', 'asdasdas', 'asdadsa', 'asdasda', 'asdasd', 'asdasdsa', 'NP', '12', 'admiN@gmail.com', 'asdasdas', 'sfasa', '1213', '08/17/2018', '[\"12\",\"12\"]', '[\"2\",\"2\"]', '08/18/2018', '[\"layout2\",\"layout2\"]', 'asdasda', '[\"12\",\"as\"]', '[\"2\",\"s\"]', '[\"2\",\"s\"]', 'asdasda', 'single', '1', 'single', '1', 'single', '1', 'single', '1', 'asdasd', 0, 0, '2018-08-17 06:38:28', '2018-08-17 06:38:28', 'credit-card', '212', '22', 'Oct', '2025'),
(9, 'mr', 'asaSA', 'asASA', 'aSas', 'AsASa', 'ASas', 'AasASA', 'NP', '111', 'admiN@gmail.com', 'asdasdsa', 'aasa', '1', '08/17/2018', '[null,null]', '[null,null]', '08/18/2018', 'null', 'NA', '[null,null]', '[null,null]', '[null,null]', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'NA', 0, 0, '2018-08-17 06:41:46', '2018-08-17 06:41:46', 'cashpayment', 'NA', 'NA', 'NA', 'NA'),
(10, 'mr', 'sass', 'sss', 'sss', 'ss', 'sss', 'sss', 'NP', '121', 'admin@gmail.com', 'asdasdsa', 'saas', '12', '08/17/2018', '[null,null]', '[null,null]', '08/18/2018', 'null', 'NA', '[null,null]', '[null,null]', '[null,null]', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 0, 0, '2018-08-17 06:43:30', '2018-08-17 06:43:30', 'cashpayment', 'NA', 'NA', 'NA', 'NA'),
(11, 'mr', 'asdadssa', 'asdad', 'adsaasd', 'asda', 'sdasdasd', 'asdsada', 'NP', '121', 'admin@gmail.com', 'asdasdsa', 'asdad', '12', '08/17/2018', '[null,null]', '[null,null]', '08/18/2018', 'null', 'NA', '[null,null]', '[null,null]', '[null,null]', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 0, 0, '2018-08-17 06:44:45', '2018-08-17 06:44:45', 'cashpayment', 'NA', 'NA', 'NA', 'NA'),
(12, 'mr', 'ABIN', 'rimal', 'Maharajung - 03', 'NA', 'Kathmandu', '44600', 'NP', '9845431044', 'abin.rimal06@gmail.com', 'NA', 'Meeting', '12', '08/20/2018', '[\"12\",\"12\",\"1\",\"3\"]', '[\"1\",\"1\",\"2\",\"4\"]', '08/23/2018', '[\"layout2\",\"layout1\",\"layout2\",\"layout2\"]', 'ASDFGHJKL', '[\"2\",\"2\",\"2\",\"2\"]', '[\"2\",\"2\",null,null]', '[null,null,null,null]', 'ADSFSADFSA', 'double', '2', 'tripple', '3', 'tripple', '3', 'more', '10', 'SDADSDAFAAADF', 0, 0, '2018-08-20 06:06:00', '2018-08-20 06:06:00', 'cashpayment', 'NA', 'NA', 'NA', 'NA'),
(13, 'mr', 'a', 'a', 'a', 'a', 'a', 'a', 'NP', '12', 'admin@sms.com', 'a', 'adssada', '123', '09/10/2018', '[null,null]', '[null,null]', '09/11/2018', 'null', 'NA', '[null,null]', '[null,null]', '[null,null]', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'NA', 0, 0, '2018-09-10 16:10:17', '2018-09-10 16:10:17', 'cashpayment', 'NA', 'NA', 'NA', 'NA'),
(14, 'mr', 'Abin', 'Rimal', 'maharajgunj', 'chitwan', 'dfasdjfa', '12324', 'NP', '13245364', 'abin.rimal@gmail.cpom', 'NA', 'NGO', '12', '10/04/2018', '[\"2\",\"1\"]', '[\"3\",\"12\"]', '10/05/2018', '[\"layout2\",\"layout1\"]', 'NA', '[\"11\",\"12\"]', '[null,null]', '[null,null]', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 0, 0, '2018-10-04 15:20:51', '2018-10-04 15:20:51', 'cashpayment', 'NA', 'NA', 'NA', 'NA');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `mobile`, `subject`, `message`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'Abin Rimal', 'abin@prabidhilabs.com', '9845431044', 'Just Hi', 'Hello team, How is all doing ?', '1', NULL, NULL),
(2, 'kundan Karna', 'tukibati@gmail.com', '98435813838', 'Want Room', '2 room for 3 days', '1', '2018-08-06 03:33:35', '2018-08-06 03:33:35'),
(3, 'kundan Karna', 'tukibati@gmail.com', '98435813838', 'Want Room', '2 room for 3 days', '1', '2018-08-06 03:33:53', '2018-08-06 03:33:53'),
(4, 'kundan Karna', 'tukibati@gmail.com', '98435813838', 'Want Room', '2 room for 3 days', '1', '2018-08-06 03:34:10', '2018-08-06 03:34:10');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `address` varchar(191) NOT NULL,
  `website` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `phone` varchar(191) NOT NULL,
  `vat_no` varchar(191) NOT NULL,
  `remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `edited_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `sub_title`, `image`, `description`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(2, 'Conference Hall', 'State-of-the-art facilities', 'vmunnyV7.jpg', '[\"1\",\"2\",\"3\",\"4\"]', '1', '1', '2018-08-02 00:26:18', '2018-09-25 19:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `event_descriptions`
--

CREATE TABLE `event_descriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_descriptions`
--

INSERT INTO `event_descriptions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Projector Facalities', NULL, NULL),
(2, 'Free High-Speed Internet Access', NULL, NULL),
(3, 'Free Bright Side Breakfast', NULL, NULL),
(4, '100 Guest Capacity', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, '32-inch Flat Screen TV w/Premium Cable Channels', NULL, NULL),
(2, 'Free High-Speed Internet Access', NULL, NULL),
(3, 'Free Bright Side Breakfast', NULL, NULL),
(4, 'Bright Morning Pillowtop Beds', NULL, NULL),
(5, 'Convenient Work Desk', NULL, NULL),
(6, 'Separate Area w/Sleeper Sofa', NULL, NULL),
(7, 'Microwave & Mini Refrigerator', NULL, NULL),
(8, 'Coffee Maker', NULL, NULL),
(9, 'Hairdryer', NULL, NULL),
(10, 'Iron w/Ironing Board', NULL, NULL),
(11, 'Free Local Calls', NULL, NULL),
(12, 'Alarm Clock', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE `foods` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `edited_by` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `created_at`, `updated_at`, `added_by`, `edited_by`, `name`, `price`, `image`) VALUES
(3, '2018-08-02 00:23:16', '2018-08-05 11:27:33', 1, 1, 'Nepali Khana', '800', 'zIuJjM6E.jpg'),
(4, '2018-08-05 11:27:52', '2018-08-05 11:27:52', 1, 1, 'Special thakali food', '1000', 'mAbYD9lv.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gallerys`
--

CREATE TABLE `gallerys` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallerys`
--

INSERT INTO `gallerys` (`id`, `title`, `image`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(2, 'ourevent', 'HgdcQNTa.jpg', '1', '1', '2018-08-02 00:21:22', '2018-08-02 00:21:39'),
(3, 'Party', '4Uf4eWqq.jpg', '1', '1', '2018-08-05 11:41:20', '2018-08-05 11:41:20'),
(4, 'BBQ', 'wey3IKjR.jpg', '1', '1', '2018-08-05 11:41:28', '2018-08-05 11:41:28'),
(5, 'Bar', '96TmCysK.jpg', '1', '1', '2018-08-05 11:41:36', '2018-08-05 11:41:36'),
(6, 'room', 'bHONnIK3.jpg', '1', '1', '2018-08-05 11:41:48', '2018-08-05 11:41:48'),
(7, 'Restaurant', 'in0KUP9B.jpg', '1', '1', '2018-08-05 11:41:59', '2018-08-05 11:41:59');

-- --------------------------------------------------------

--
-- Table structure for table `guestevents`
--

CREATE TABLE `guestevents` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `org` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paymentmethod` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cardtype` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cardnumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attendance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `standard_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `standard_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tented_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tented_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wooden_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wooden_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_room_occupant` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_room_quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_suggestion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adult_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adult_more` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child_more` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `children_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `children_more` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `package_suggestion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guestevents`
--

INSERT INTO `guestevents` (`id`, `title`, `first_name`, `last_name`, `address1`, `address2`, `city`, `postalcode`, `country`, `phone`, `email`, `org`, `paymentmethod`, `cardtype`, `cardnumber`, `card_month`, `card_year`, `event_type`, `attendance`, `start_date`, `end_date`, `standard_room_occupant`, `standard_room_quantity`, `tented_room_occupant`, `tented_room_quantity`, `wooden_room_occupant`, `wooden_room_quantity`, `group_room_occupant`, `group_room_quantity`, `room_suggestion`, `package_name`, `adult_number`, `adult_more`, `child_number`, `child_more`, `children_number`, `children_more`, `package_suggestion`, `created_at`, `updated_at`) VALUES
(1, 'mr', 'sadsadsa', 'dsaasda', 'asdsada', 'asdads', 'saadsads', 'asdasd', 'NP', '121', 'admin@sms.com', 'sadsada', 'credit-card', '1213', '1231132', 'Mar', '2024', 'NA', '1231', '08/16/2018', '08/16/2018', 'single', '1', 'single', '1', 'single', '1', 'single', '1', 'sadadsa', 'Rooms with Breakfast,Lunch & Dinner', '4', NULL, '1', NULL, NULL, NULL, 'sadsada', '2018-08-16 10:51:44', '2018-08-16 10:51:44'),
(2, 'mr', 'test', 'eeteq', 'sadad', 'asdsada', 'asdadsa', '12sad', 'NP', '1231', 'adad@gmail.com', 'sadasda', 'credit-card', '1231', '123131', 'Nov', '2027', 'NA', '123', '08/16/2018', '08/25/2018', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'single', 'NA', 'NA', 'NA', '1', NULL, '0', NULL, '0', NULL, 'NA', '2018-08-16 11:09:55', '2018-08-16 11:09:55'),
(4, 'mr', 'Dd', 'Ff', 'Ff', 'Ff', 'Vv', 'G', 'NP', '1', 'tukibatti@gmail.com', 'Ff', 'cashpayment', 'NA', 'NA', 'NA', 'NA', 'Picnic', '100', '08/17/2018', '08/17/2018', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', NULL, NULL, NULL, NULL, NULL, NULL, 'NA', '2018-08-17 06:56:04', '2018-08-17 06:56:04');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `task` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `task`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(2, 'Test', '1', 1, '2018-08-02 00:20:40', '2018-08-02 00:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2018_06_19_113259_create_upcoming_events_table', 2),
(4, '2018_06_19_115306_create_events_table', 3),
(5, '2018_06_19_115906_create_restaurants_table', 4),
(6, '2018_06_19_121246_create_gallerys_table', 5),
(7, '2018_06_19_121709_create_user1s_table', 6),
(8, '2018_06_19_122243_create_contacts_table', 7),
(9, '2018_06_19_122816_create_settings_table', 8),
(10, '2018_06_19_123500_create_snippets_table', 9),
(11, '2018_06_19_124128_create_company_details_table', 10),
(12, '2018_06_20_011817_create_sliders_table', 11),
(13, '2018_06_20_012201_create_subscriptions_table', 12),
(14, '2018_06_20_012427_create_amenities_table', 13),
(15, '2018_06_20_012810_create_rooms_table', 14),
(16, '2018_06_20_013714_create_activities_table', 15),
(17, '2018_06_20_014010_create_testimonials_table', 16),
(18, '2018_06_20_014603_create_logs_table', 17),
(19, '2018_06_20_014813_create_foods_table', 18),
(20, '2018_06_21_004701_create_snippets_table', 19),
(23, '2017_01_27_000400_create_investment_table', 1),
(24, '2017_01_27_000416_create_restock_table', 1),
(25, '2017_01_27_000433_create_product_table', 1),
(26, '2017_01_27_000445_create_currency_table', 1),
(29, '2018_07_23_063144_create_conference_table', 20),
(30, '2018_08_03_133333_create_facilities_table', 21),
(31, '2018_08_03_175310_create_event_description_table', 22),
(32, '2018_08_06_140722_create_jobs_table', 23),
(33, '2018_08_16_111441_create_guestevents_table', 24),
(34, '2018_08_22_170528_create_bookedrooms_table', 25),
(35, '2018_08_24_045854_create_room_numbers_table', 26),
(36, '2018_11_19_053829_create_packages_table', 27);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `description`, `price`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Package one', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', '500', '1', '2018-11-19 00:42:18', '2018-11-21 04:35:45'),
(2, 'Package Two', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', '1000', '1', '2018-11-19 00:50:14', '2018-11-21 04:50:12');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('', '', NULL),
('abin@prabidhilabs.com', '$2y$10$OKH/UN9sklDamuWLSZ5AXug6ofl7Jj9gpRDiIh4LH589p.AM5k/uW', '2018-08-03 03:21:30');

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_header` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_header` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chef_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chef_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `chef_photo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `chef_message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `restaurants`
--

INSERT INTO `restaurants` (`id`, `page_header`, `sub_header`, `chef_name`, `chef_title`, `chef_photo`, `chef_message`, `featured_image`, `featured_title`, `featured_message`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(1, 'JANA: BAHA: RESTRO AND BAR', 'YOU DON’T NEED A SILVER FORK TO EAT GOOD FOOD', 'Ram Kumar Shrestha', 'TASTEFUL EXPERIENCES SINCE ’98', 'gTn9Tr1g.jpg', 'Liking to cook dinner for your friends and loved ones might be a good starting point, but it won\'t be enough if you want to make it as a line cook. Cooking the same dishes for strangers for a lengthy shift requires more than just a general \"like\" of the kitchen, you\'ve got to love it.', 'KtfRW8gV.jpg', 'DRINKS AND BAR', 'They’re seas gathering behold the years saying make and divide fill given whales fill female moved, blessed. Midst one from divide whales seasons cattle male own saying to night fruit own creeping second earth be lesser without deep beast female..', '1', '1', NULL, '2018-08-05 11:16:48');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `starting_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facilities` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_rooms` int(11) NOT NULL,
  `availability` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_price` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `type`, `image`, `starting_price`, `last_price`, `criteria`, `facilities`, `description`, `total_rooms`, `availability`, `status`, `sales_price`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(2, 'Standard Room123', 'pJPjL1RB.jpg', '20002', '2000', '18+2', '[\"1\",\"3\",\"5\"]', '32-inch Flat Screen TV w/Premium Cable Channels\r\nFree High-Speed Internet Access\r\nFree Bright Side Breakfast', 12, '2', 'open2', '20002', '2', '2', '2018-08-03 07:45:16', '2018-08-21 10:22:50'),
(3, 'Group Room', 'lTYGLpwO.jpg', '2500', '3000', '18+', '[\"1\",\"3\",\"5\"]', 'asd', 1, '4', 'booked', '2000', '1', '1', '2018-08-03 08:55:34', '2018-08-03 08:55:34'),
(4, 'WOODEN ROOM', 'HrJhS1nc.jpg', '2000', '1500', '18+', '[\"1\",\"3\",\"5\"]', 'None', 2, '3', 'Open', '2000', '1', '1', '2018-08-03 09:26:38', '2018-08-03 09:26:38'),
(5, 'Tented Room', 'foufyag3.jpg', '3500', '3000', '18+', '[\"1\",\"3\",\"5\"]', 'sasasa', 4, '2', 'open', '2000', '1', '1', '2018-08-03 09:45:53', '2018-08-03 09:45:53'),
(6, 'Normal Room', '9GipfQR1.jpg', '2000', '12000', '18+', '[\"2\",\"3\",\"4\"]', 'asdadsa', 2, '2', 'booked', '1231', '2', '2', '2018-10-03 04:58:04', '2018-10-05 07:33:58');

-- --------------------------------------------------------

--
-- Table structure for table `room_numbers`
--

CREATE TABLE `room_numbers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_type_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_numbers`
--

INSERT INTO `room_numbers` (`id`, `name`, `room_type_id`, `created_at`, `updated_at`) VALUES
(2, '102', '3', '2018-08-24 00:22:45', '2018-08-24 00:50:11'),
(3, '102', '3', '2018-08-24 00:22:45', '2018-08-24 00:50:11'),
(4, '104', '2', '2018-08-24 00:22:45', '2018-08-24 00:50:11'),
(5, '105', '4', '2018-08-24 00:22:45', '2018-08-24 00:50:11');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_collapsed` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `copyright_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_title`, `logo`, `logo_collapsed`, `meta_title`, `meta_description`, `keywords`, `copyright_text`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(1, 'Chhaimale', 'FXCHh3cL.png', '2uREwztj.png', 'Chhaimale', 'Chhaimaile Resort is located in Chhaimale, Dakshinkaali, just 22 kilometers away from Kathmandu. Located in a peaceful environmental, we deliver a natural environment with quality services, comfortable rooms, excellent and hygienic foods at relatively cheap price.', 'resort,holiday', 'Chhaimale Resort 2018  All rights reserved.', '1', '1', NULL, '2018-08-02 00:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `link_text`, `order`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(2, 'RzKdKqUE.jpg', 'Chhaimale House', 2, '1', '1', '2018-08-02 00:12:46', '2018-08-03 05:07:54'),
(4, 'RWTCAT5v.jpg', 'banner2', 2, '1', '1', '2018-08-03 05:18:33', '2018-08-03 05:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `email`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(1, '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edited_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `position`, `company`, `rating`, `picture`, `phone_number`, `comment`, `added_by`, `edited_by`, `created_at`, `updated_at`) VALUES
(2, 'Kundan Karna', 'Editor', 'Prabidhilabs', '5', 'YRZ6W6Lh.png', '982222222', 'Test Purpose', '1', '1', '2018-08-02 00:11:48', '2018-08-02 00:11:48'),
(3, 'Abin Rimal', 'CEO', 'Prabidhi Labs', '5', 'fTap2Elp.jpg', '98435813838', 'Best resort in the town', '1', '1', '2018-08-10 02:21:31', '2018-08-13 22:57:52');

-- --------------------------------------------------------

--
-- Table structure for table `upcoming_events`
--

CREATE TABLE `upcoming_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `criteria` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_seats` int(11) NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `upcoming_events`
--

INSERT INTO `upcoming_events` (`id`, `title`, `description`, `date`, `amount`, `criteria`, `total_seats`, `image`, `meta_title`, `meta_description`, `keywords`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'NEW YEAR PARTY AT CHHAIMALE', 'It\'s the big countdown when the ball drops. I\'m talking about New Year\'s Eve, of course! There is no other holiday I know of that involves a giant party with a \"drink till you\'re silly\" mentality followed by a day in which you make huge goals about your next year\'s productivity. If you don\'t usually remember your New Year\'s Eve.', '2018-08-07', 3000.00, '18+', 200, '6gZIh0ly.jpg', 'Chhaimale', 'chhaimale', 'chhaimale', '1', '1', '2018-08-02 00:46:12', '2018-08-02 00:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kundan Karna', 'tukibati@gmail.com', '$2y$10$fcfCPRVF6IGgEjU2pJIB6.Kk0BXOwHeNaLMOhuG.8M1THJIK0cS1q', 'vZLGrvdJ0AHicaGO3GNTbQh8NkMLrSEdj3P0RUzhvP05qiK4I53wOwKbvn0Q', NULL, '2018-09-04 06:36:48'),
(2, 'shailendra Tuladhar', 'shailendtulad@gmail.com', '$2y$10$fcfCPRVF6IGgEjU2pJIB6.Kk0BXOwHeNaLMOhuG.8M1THJIK0cS1q', 'NQ8ZAjvSKwPAapUAv5eUBQBANVANgBKZAjSCUmUH7V9QqJovdMfzlwykqP7H', NULL, '2018-08-03 03:29:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amenities`
--
ALTER TABLE `amenities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookedrooms`
--
ALTER TABLE `bookedrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_deleted_at_index` (`deleted_at`),
  ADD KEY `110461_5a676fa2321c7` (`customer_id`),
  ADD KEY `110461_5a676fa239ffd` (`room_id`);

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conferences`
--
ALTER TABLE `conferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_descriptions`
--
ALTER TABLE `event_descriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallerys`
--
ALTER TABLE `gallerys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guestevents`
--
ALTER TABLE `guestevents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_numbers`
--
ALTER TABLE `room_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upcoming_events`
--
ALTER TABLE `upcoming_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `amenities`
--
ALTER TABLE `amenities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bookedrooms`
--
ALTER TABLE `bookedrooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `conferences`
--
ALTER TABLE `conferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event_descriptions`
--
ALTER TABLE `event_descriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `foods`
--
ALTER TABLE `foods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gallerys`
--
ALTER TABLE `gallerys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `guestevents`
--
ALTER TABLE `guestevents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `room_numbers`
--
ALTER TABLE `room_numbers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `upcoming_events`
--
ALTER TABLE `upcoming_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
