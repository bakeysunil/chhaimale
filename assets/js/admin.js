var admin = {};

admin.csrfToken = $('meta[name="csrf_token"]').attr('content');

admin.init = function () {
    toastr.options = {
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "1500",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    $('[data-toggle="tooltip"]').tooltip();

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
};

admin.addImageImport = function () {
    // Import image
    var inputImage = document.getElementById('inputImage');
    var previewThumb = document.getElementById('previewThumb');
    var previewImage = previewThumb.children.item('img');
    var btnChoose = $("#btnChoose");
    var btnRemove = $("#btnRemove");
    var URL = window.URL || window.webkitURL;
    var blobURL;

    if (URL) {
        inputImage.onchange = function () {
            var files = this.files;
            var file;

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    previewImage.src = blobURL;
                    previewThumb.classList.remove('hidden');
                    btnChoose.addClass('btn-sm').find('span').html('Change');
                    btnRemove.removeClass('hidden');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        };
    }

    btnRemove.on('click', function () {
        inputImage.value = null;
        previewImage.src = null;
        previewThumb.classList.add('hidden');
        btnChoose.removeClass('btn-sm').find('span').html('Select image');
        btnRemove.addClass('hidden');
    });
};

admin.editImageImport = function () {
    // Import image
    var inputImage = $('#inputImage');
    var inputIsImageRemoved = $('#inputIsImageRemoved');
    var previewThumb = $('#previewThumb');
    var previewImage = $(previewThumb).find('img');
    var btnChoose = $("#btnChoose");
    var btnRemove = $("#btnRemove");
    var URL = window.URL || window.webkitURL;
    var blobURL;

    if (URL) {
        inputImage.on('change', function () {
            var files = this.files;
            var file;

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    previewImage.attr('src', blobURL);
                    inputIsImageRemoved.val(null);
                    previewThumb.removeClass('hidden');
                    btnChoose.addClass('btn-sm').find('span').html('Change');
                    btnRemove.removeClass('hidden');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    }

    btnRemove.on('click', function () {
        inputImage.val(null);
        inputIsImageRemoved.val(1);
        previewImage.attr('src', null);
        previewThumb.addClass('hidden');
        btnChoose.removeClass('btn-sm').find('span').html('Select featured image');
        btnRemove.addClass('hidden');
    });
};

admin.editorInit = function () {
    tinymce.init({
        selector: '.editor',
        height: 500,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'bold italic fontsizeselect forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | print',
        image_advtab: true,
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
};

admin.init();