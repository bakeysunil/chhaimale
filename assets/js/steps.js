$(function() {
   $("#day-container").empty();
});



var form = $("#conferencehall");
form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    autoFocus: true,
    saveState: true,
    onStepChanging: function (event, currentIndex, newIndex)
    {
       
       if($("#steps-uid-0-p-0").hasClass('current')){
             $('#day-container').empty();
        $("#meal-container").empty();
        var start = $('#conference_start').datepicker('getDate');
        var end   = $('#conference_end').datepicker('getDate');
        var days   = (end - start)/1000/60/60/24;
            customDaysCreate(days,start);

        }
        // if($("#steps-uid-0-p-1").hasClass('current')){
        //     clear();
        // }

        form.validate().settings.ignore = ":disabled,:hidden";
         return form.valid();
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
        $("#conferencehall").submit();
        alert("Submitted!");
    }
});

function clear(){
    
    //console.log(x);
    if($("#steps-uid-0-p-0").find('#front')){
        alert('hello');
    }
    console.log(x);

}
function customDaysCreate(day, date) {
    for(i=0;i<=day;i++){
        var x =  i + 1;
        var y = date;
        y.setDate(y.getDate()+i);
        var dateString =y.getFullYear() + '-' + (y.getMonth() + 1) + '-' + y.getDate();
        let printHtml = ' <div class="conference-room">\n' +
            '                        <div class="row">\n' +
            '                            <div class="col-md-6">\n' +
            '                                <div class="date">\n' +
            '                                    <p><span class="day">Day ' + x + '</span> ' + dateString + ' </p>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                            <div class="col-md-6">\n' +
            '                                <div class="add">\n' +
            '                                    <a data-toggle="collapse" href="#conferenceroom'+x+'" role="button" aria-expanded="false" aria-controls="conferenceroom">\n' +
            '                                        + Add Conference Rooms\n' +
            '                                    </a>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <div id="conferenceroom'+x+'" class="collapse">\n' +
            '                        <div class="card card-body">\n' +
            '                            <div class="col-md-6">\n' +
            '                                <div class="row">\n' +
            '                                    <div class="col-md-6">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label for="usr">Conference Start Time</label>\n' +
            '                                            <input type="time" class="form-control" name="conferencestart[]">\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-6">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label for="usr">Conference End Time</label>\n' +
            '                                            <input type="time" class="form-control" name="conferenceend[]">\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-12">\n' +
            '                                        <p>Select a setup style for the Meeting Room*</p>\n' +
            '                                        <div class="setuptype">\n' +
            '                                            <div class="cc-selector">\n' +
            '                                                <input class="layout_image" id="layout1'+ x +'" type="radio" name="layout[]"  value="layout1" />\n' +
            '                                                <label class="drinkcard-cc layout1" for="layout1'+ x +'"></label>\n' +
            '                                                <input class="layout_image" id="layout2'+ x +'" type="radio" name="layout[]"  value="layout2" />\n' +
            '                                                <label class="drinkcard-cc layout2" for="layout2'+ x +'"></label>\n' +
            '                                                <input class="layout_image" id="layout3'+ x +'" type="radio" name="layout[]"  value="layout3" />\n' +
            '                                                <label class="drinkcard-cc layout3" for="layout3'+ x +'"></label>\n' +
            '                                            </div>\n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <!--Row Endss -->\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>';

        $("#day-container").append(printHtml);
        y.setDate(y.getDate()-i);
    }
    for(i=0;i<=day;i++){
        var x =  i + 1;
        var y = date;
        y.setDate(y.getDate()+i);
        var dateString =y.getFullYear() + '-' + (y.getMonth() + 1) + '-' + y.getDate();
        let printHtml = ' <div class="conference-room">\n' +
            '                    <div class="row">\n' +
            '                        <div class="col-md-6">\n' +
            '                            <div class="date">\n' +
            '                                <p><span class="day">Day ' + x +'</span>'+ dateString +'</p>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <div class="col-md-6">\n' +
            '                            <div class="add">\n' +
            '                                <a data-toggle="collapse" href="#meal'+x+'" role="button" aria-expanded="false" aria-controls="meal">\n' +
            '                                    + Add Meal Time \n' +
            '                                </a>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '                <div class="collapse" id="meal'+x+'">\n' +
            '                    <div class="card card-body">\n' +
            '                        <!-- Heading Section  -->\n' +
            '                        <div class="col-md-6">\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <div class="checkbox">\n' +
            '                                        <p>Meal Type</p>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <p>Serving Time</p>\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <!-- BreakFast Checkbox and Serving Time  -->\n' +
            '                        <div class="col-md-6">\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <div class="checkbox">\n' +
            '                                        <label>\n' +
            '                                            <input type="checkbox" value="breakfast" class="breakfast " name="breakfast">BreakFast</label>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <input type="time" name="breakfasttime[]" class="breakfasttime form-control" style="display: none" >\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <!-- Breakfast Checkbox and Serving Time Ends -->\n' +
            '                        <!-- Lunch Checkbox and Serving Time  -->\n' +
            '                        <div class="col-md-6">\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <div class="checkbox">\n' +
            '                                        <label>\n' +
            '                                            <input type="checkbox" value="lunch" class="lunch">lunch</label>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <input type="time" name="lunchtime[]" class="lunchtime form-control" style="display: none">\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <!-- Lunch Checkbox and Serving Time Ends-->\n' +
            '                        <!-- Dinner Checkbox and Serving Time  -->\n' +
            '                        <div class="col-md-6">\n' +
            '                            <div class="row">\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <div class="checkbox">\n' +
            '                                        <label>\n' +
            '                                            <input type="checkbox" value="dinner" class="dinner">dinner</label>\n' +
            '                                    </div>\n' +
            '                                </div>\n' +
            '                                <div class="col-md-4 offset-md-2">\n' +
            '                                    <input type="time" name="dinnertime[]" class="dinnertime form-control" style="display: none">\n' +
            '                                </div>\n' +
            '                            </div>\n' +
            '                        </div>\n' +
            '                        <!-- Lunch Dinner and Serving Time Ends -->\n' +
            '\n' +
            '                    </div>\n' +
            '                </div>';

        $("#meal-container").append(printHtml);
        y.setDate(y.getDate()-i);
    }

}


$(document).ready(function(){
   form.children('div').steps('remove',3);
});
$('#add_rooms').click(function(){
    if($('#add_rooms').is(':checked')){

            form.children('div').steps("insert", 3,{
                title:"Sleeping Room <br> &nbsp;&nbsp;<small> Need Sleeping Rooms</small>",
                content:'<section>\n' +
                '                <div class="rooms_content">\n' +
                '                    <h5>Please Add Your Sleeping Rooms</h5>\n' +
                '                    <p>Tell us what sleeping rooms you need and their quantity.</p>\n' +
                '                    <h6>Please select the room you want</h6>\n' +
                '                    <div class="conference-room">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-6">\n' +
                '                                <p>Please add the room Your want</p>\n' +
                '                            </div>\n' +
                '                            <div class="col-md-6">\n' +
                '                                <div class="add">\n' +
                '                                    <a data-toggle="collapse" href="#rooms" role="button" aria-expanded="false" aria-controls="rooms">\n' +
                '                                        + Add Rooms\n' +
                '                                    </a>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="collapse" id="rooms">\n' +
                '                        <div class="card card-body">\n' +
                '\n' +
                '                            <!--Standard Room   -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <label>Room Type</label>\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="standard_room" id="standard_room">Standard Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <label> Occupant</label>\n' +
                '                                        <select class="form-control" id="standard_occupant" class="form-control" style="display: none;" name="standard_room_occupant">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <label> Quantity</label>\n' +
                '                                        <input type="number" id="standard_quantity" class="form-control" style="display: none;" name="standard_room_quantity">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <!-- Standard Room Ends -->\n' +
                '                            <!-- Wooden Room  -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="wooden_room" id="wooden_room" >wooden Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <select class="form-control"  id="wooden_occupant" style="display: none;" name="wooden_room_occupant">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '                                            <option value="tripple">Tripple</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <input type="number" class="form-control" name="wooden_room_quantity" id="wooden_quantity" style="display: none" name="wooden_room_quantity">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <!-- Wooden Room Ends-->\n' +
                '                            <!-- Tented Room  -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="tented_room" id="tented_room">Tented Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <select class="form-control" id="tented_occupant" style="display: none;" name="tented_room_occupant">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '                                            <option value="tripple">Tripple</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <input type="number"  class="form-control"   name="tented_room_quantity" id="tented_quantity" style="display: none">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' + 
                '                            </div>\n' +
                '                            <!--Tented Rooms Ends -->\n' +
                '                            <!-- Group Family Tradition Rooms Start -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="group_room" id="group_room">Group Family Tradation Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <select class="form-control" id="group_occupant" style="display: none;" name="group_room_occupant">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '                                            <option value="tripple">Tripple</option>\n' +
                '                                            <option value="more">More</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <input type="number" class="form-control" id="group_quantity" style="display: none;" name="group_room_quantity">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-6">\n' +
                '                        <div class="form-group">\n' +
                '                            <label for="comment">Any Message</label>\n' +
                '                            <textarea class="form-control" rows="4" id="comment" name="room_suggestion"></textarea>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </section>'
            });
    }
    else{
        form.children('div').steps('remove',3);

    }
});


// Date Picker


        $("#conference_end").datepicker({
            minDate: new Date()
        });

        $("#conference_start").datepicker({
            minDate: new Date(),
            onSelect: function(dateText, inst) {
                var selectedDate = $(this).datepicker("getDate");
                $("#conference_end").datepicker("option", "minDate", selectedDate);
            }
        });

        $(function() {
            $("#conference_start").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "0");
        });

        $(function() {
            $("#conference_end").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "1");
        });
 

 // $(function() {
 //        $("#breakfast").click(function() {
 //            if ($(this).is(":checked")) {
 //                $("#breakfasttime").show();
 //            } else {
 //                $("#breakfasttime").hide();
 //            }
 //        });
 //    });
$(document).on("click",".breakfast", function(){
    
 
   if ($(this).is(":checked")) {
                $(this).parent().parent().parent().parent().find('.breakfasttime').css('display','block');
                 
            } else {
                
                $(this).parent().parent().parent().parent().find('.breakfasttime').css('display','none');
            }
  });
$(document).on("click",".lunch", function(){
   if ($(this).is(":checked")) {
                 $(this).parent().parent().parent().parent().find('.lunchtime').css('display','block');
                 
            } else {
                $(this).parent().parent().parent().parent().find('.lunchtime').css('display','none');
            }
  });
    // $(function() {
    //     $("#lunch").click(function() {
    //         if ($(this).is(":checked")) {
    //             $("#lunchtime").show();
    //         } else {
    //             $("#lunchtime").hide();
    //         }
    //     });
    // });

// $(document).on("click","#lunch", function(){
//    if ($(this).is(":checked")) {
//                 $("#lunchtime").show();
                 
//             } else {
//                 $("#standard_occupant").hide();
//                  $("#lunchtime").hide();
//             }
//   });
    // $(function() {
    //     $("#dinner").click(function() {
    //         if ($(this).is(":checked")) {
    //             $("#dinnertime").show();
    //         } else {
    //             $("#dinnertime").hide();
    //         }
    //     });
    // });

$(document).on("click",".dinner", function(){
   if ($(this).is(":checked")) {
                $(this).parent().parent().parent().parent().find('.dinnertime').css('display','block');
                 
                 
            } else {
                  $(this).parent().parent().parent().parent().find('.dinnertime').css('display','none');
            }
  });
    // Rooms 

    //     $(function() {
    //     $("#standard_room").on('click',function() {
    //         if ($(this).is(":checked")) {
    //             $("#standard_occupant").show();
    //              $("#standard_quantity").show();
    //         } else {
    //             $("#standard_occupant").hide();
    //              $("#standard_quantity").hide();
    //         }
    //     });
    // });


  $(document).on("click","#standard_room", function(){
   if ($(this).is(":checked")) {
                $("#standard_occupant").show();
                 $("#standard_quantity").show();
            } else {
                $("#standard_occupant").hide();
                 $("#standard_quantity").hide();
            }
  });

    // $(function() {
    //     $("#wooden_room").click(function() {
    //         if ($(this).is(":checked")) {
    //             $("#wooden_quantity").show();
    //              $("#wooden_occupant").show();
    //         } else {
    //            $("#wooden_quantity").hide();
    //              $("#wooden_occupant").hide();
    //         }
    //     });
    // });


  $(document).on("click","#wooden_room", function(){
   if ($(this).is(":checked")) {
                $("#wooden_quantity").show();
                 $("#wooden_occupant").show();
            } else {
                $("#wooden_quantity").hide();
                 $("#wooden_occupant").hide();
            }
  });
    //    $(function() {
    //     $("#tented_room").click(function() {
    //         if ($(this).is(":checked")) {
    //             $("#tented_quantity").show();
    //              $("#tented_occupant").show();
    //         } else {
    //            $("#tented_quantity").hide();
    //              $("#tented_occupant").hide();
    //         }
    //     });
    // });

 $(document).on("click","#tented_room", function(){
   if ($(this).is(":checked")) {
                $("#tented_quantity").show();
                 $("#tented_occupant").show();
            } else {
                $("#tented_quantity").hide();
                 $("#tented_occupant").hide();
            }
  });
 // $(function() {
 //        $("#group_room").click(function() {
 //            if ($(this).is(":checked")) {
 //                $("#group_quantity").show();
 //                 $("#group_occupant").show();
 //            } else {
 //               $("#group_quantity").hide();
 //                 $("#group_occupant").hide();
 //            }
 //        });
 //    });

 $(document).on("click","#group_room", function(){
   if ($(this).is(":checked")) {
                $("#group_quantity").show();
                 $("#group_occupant").show();
            } else {
                $("#group_quantity").hide();
                 $("#group_occupant").hide();
            }
  });




          
    function show1() {
        document.getElementById('card_detail').style.display = 'none';
    }

    function show2() {
        document.getElementById('card_detail').style.display = 'block';
    }

    
   
