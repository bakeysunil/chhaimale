
var form = $("#eventbooking");
form.children("div").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    autoFocus: true,
    saveState: true,
    onStepChanging: function (event, currentIndex, newIndex)
    {
 
        form.validate().settings.ignore = ":disabled,:hidden";
         return form.valid();
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
         $("#eventbooking").submit();
        alert("Submitted!");
    }
});


$(document).ready(function(){
   form.children('div').steps('remove',1);
});
$('#eventaddrooms').click(function(){
    if($('#eventaddrooms').is(':checked')){

            form.children('div').steps("insert",1,{
                title:"Sleeping Room <br> &nbsp;  &nbsp; &nbsp;&nbsp;<small> Need Sleeping Rooms",
                content:'<section>\n' +
                '                <div class="rooms_content">\n' +
                '                    <h5>Please Add Your Sleeping Rooms</h5>\n' +
                '                    <p>Tell us what sleeping rooms you need and their quantity.</p>\n' +
                '                    <h6>Please select the room you want</h6>\n' +
                '                    <div class="conference-room">\n' +
                '                        <div class="row">\n' +
                '                            <div class="col-md-6">\n' +
                '                                Please add the room you want\n' + 
                '                            </div>\n' +
                '                            <div class="col-md-6">\n' +                          
                '                                <div class="add">\n' +
                '                                    <a data-toggle="collapse" href="#rooms" role="button" aria-expanded="false" aria-controls="rooms">\n' +
                '                                        + Add Rooms\n' +
                '                                    </a>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="collapse" id="rooms">\n' +
                '                        <div class="card card-body">\n' +
                '\n' +
                '                            <!--Standard Room   -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <label>Room Type</label>\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="standard_room" id="standard_room">Standard Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <label> Occupant</label>\n' +
                '                                        <select class="form-control" id="standard_occupant" class="form-control" name="standard_room_occupant" style="display: none;">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <label> Quantity</label>\n' +
                '                                        <input type="number" name="standard_room_quantity" id="standard_quantity" class="form-control" style="display: none">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <!-- Standard Room Ends -->\n' +
                '                            <!-- Wooden Room  -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="wooden_room" id="wooden_room">wooden Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <select class="form-control"  id="wooden_occupant" style="display: none;" name="wooden_room_occupant">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '                                            <option value="tripple">Tripple</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <input type="number" class="form-control" name="wooden_room_quantity" id="wooden_quantity" style="display: none">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                            <!-- Wooden Room Ends-->\n' +
                '                            <!-- Tented Room  -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="tented_room" id="tented_room">Tented Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <select class="form-control" id="tented_occupant" name="tented_room_occupant" style="display: none;">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '                                            <option value="tripple">Tripple</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <input type="number"  class="form-control"  name="tented_room_quantity" id="tented_quantity" style="display: none">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' + 
                '                            </div>\n' +
                '                            <!--Tented Rooms Ends -->\n' +
                '                            <!-- Group Family Tradition Rooms Start -->\n' +
                '                            <div class="col-md-9">\n' +
                '                                <div class="row">\n' +
                '                                    <div class="col-md-4">\n' +
                '                                        <div class="checkbox">\n' +
                '                                            <label>\n' +
                '                                                <input type="checkbox" value="group_room" id="group_room">Group Family Tradation Room</label>\n' +
                '                                        </div>\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <select class="form-control" id="group_occupant" name="group_room_occupant" style="display: none;">\n' +
                '                                            <option value="single">Single </option>\n' +
                '                                            <option value="double">Double</option>\n' +
                '                                            <option value="tripple">Tripple</option>\n' +
                '                                            <option value="more">More</option>\n' +
                '\n' +
                '                                        </select>\n' +
                '\n' +
                '                                    </div>\n' +
                '\n' +
                '                                    <div class="col-md-2">\n' +
                '                                        <input type="number" class="form-control" name="group_room_quantity" id="group_quantity" style="display: none">\n' +
                '                                    </div>\n' +
                '\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="col-md-6">\n' +
                '                        <div class="form-group">\n' +
                '                            <label for="comment">Any Message</label>\n' +
                '                            <textarea class="form-control" rows="4" id="comment" name="room_suggestion"></textarea>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </section>'
            });
    }
    else{
        form.children('div').steps('remove',1);

    }
});





$(document).on("click","#breakfast", function(){
   if ($(this).is(":checked")) {
                $("#breakfasttime").show();
                 
            } else {
                $("#standard_occupant").hide();
                 $("#breakfasttime").hide();
            }
  });
$(document).on("click","#lunch", function(){
   if ($(this).is(":checked")) {
                $("#lunchtime").show();
                 
            } else {
                $("#standard_occupant").hide();
                 $("#lunchtime").hide();
            }
  });
  

$(document).on("click","#lunch", function(){
   if ($(this).is(":checked")) {
                $("#lunchtime").show();
                 
            } else {
                $("#standard_occupant").hide();
                 $("#lunchtime").hide();
            }
  });


$(document).on("click","#dinner", function(){
   if ($(this).is(":checked")) {
                $("#dinnertime").show();
                 
            } else {
                $("#standard_occupant").hide();
                 $("#dinnertime").hide();
            }
  });


  $(document).on("click","#standard_room", function(){
   if ($(this).is(":checked")) {
                $("#standard_occupant").show();
                 $("#standard_quantity").show();
            } else {
                $("#standard_occupant").hide();
                 $("#standard_quantity").hide();
            }
  });



  $(document).on("click","#wooden_room", function(){
   if ($(this).is(":checked")) {
                $("#wooden_quantity").show();
                 $("#wooden_occupant").show();
            } else {
                $("#wooden_quantity").hide();
                 $("#wooden_occupant").hide();
            }
  });

 $(document).on("click","#tented_room", function(){
   if ($(this).is(":checked")) {
                $("#tented_quantity").show();
                 $("#tented_occupant").show();
            } else {
                $("#tented_quantity").hide();
                 $("#tented_occupant").hide();
            }
  });

 $(document).on("click","#group_room", function(){
   if ($(this).is(":checked")) {
                $("#group_quantity").show();
                 $("#group_occupant").show();
            } else {
                $("#group_quantity").hide();
                 $("#group_occupant").hide();
            }
  });

    
   
