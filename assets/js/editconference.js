$("#end_date").datepicker({
    minDate: new Date()
});


$("#start_date").datepicker({
    onSelect: function(dateText, inst) {
        display();
    }
});

$("#end_date").on("change",function(){
    display();
});
function display() {
    var start = $('#start_date').datepicker('getDate');
    var end   = $('#end_date').datepicker('getDate');
    var days   = (end - start)/1000/60/60/24 + 1;
    $('.cfroom_title').css('display','block');
    $('#cfroom').empty();
    $('#cf_meal').empty();
    for(i=1;i<=days;i++){
        var printHtml = '<div class="row">\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group}}">\n' +
            '                                            <label>Days</label>\n' +
            '                                            <input class="form-control" type="text" name=""  value="Day '+ i +'" readonly">\n' +
            '                                            \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label>Start Time</label>\n' +
            '                                            <input class="form-control" type="text" name="conferencestart[]">\n' +
            '                                           \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label>End Time</label>\n' +
            '                                            <input class="form-control" type="text" name="conferenceend[]">\n' +
            '                                          \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label>Layout</label>\n' +
            '                                            <select class="form-control" name="layout[]" id="">\n' +
            '                                                <option value="">Layout 1</option>\n' +
            '                                                <option value="">Layout 2</option>\n' +
            '                                                <option value="">Layout 3</option>\n' +
            '                                                <option value="">Layout 4</option>\n' +
            '                                            </select>\n' +
            '                                           \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '\n' +
            '\n' +
            '                                </div>';
        $("#cfroom").append(printHtml);

        var printMeal = '  <div class="row">\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label>Days</label>\n' +
            '                                            <input class="form-control" type="text" name=""  value="Day 1" readonly }}">\n' +
            '                                            \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label>BreakFast</label>\n' +
            '                                            <input class="form-control" type="text" name="breakfasttime[] placeholder="serving time">\n' +
            '                                           \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label>Lunch</label>\n' +
            '                                            <input class="form-control" type="text" name="lunchtime[]" placeholder="serving time"  >\n' +
            '                                           \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    <div class="col-md-3">\n' +
            '                                        <div class="form-group">\n' +
            '                                            <label>Dinner</label>\n' +
            '                                            <input class="form-control" type="text" name="dinnertime[] placeholde"serving time">\n' +
            '                                           \n' +
            '                                        </div>\n' +
            '                                    </div>\n' +
            '                                    </div>';
        $("#cf_meal").append(printMeal);
    }
    $('.cf_suggestion').css('display','block');

}