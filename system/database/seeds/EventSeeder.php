<?php

use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('event_descriptions')->insert([
            'name' => '100 Guest Capacity',
        ]);
    }
}
