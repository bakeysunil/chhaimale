<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookedroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookedrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('room_id');
            $table->string('days');
            $table->string('sub_total');
            $table->string('service_charge');
            $table->string('vat');
            $table->string('total');
            $table->string('check-in');
            $table->string('check-out');
            $table->string('occupancy');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address1');
            $table->string('address2');
            $table->string('city');
            $table->string('zip');
            $table->string('country');
            $table->string('phone');
            $table->string('email');
            $table->string('org');
            $table->string('payment_method');
            $table->string('card_type');
            $table->string('card_number');
            $table->string('card_month');
            $table->string('card_year');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookedrooms');
    }
}
