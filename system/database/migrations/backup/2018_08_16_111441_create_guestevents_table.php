<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuesteventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guestevents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address1');
            $table->string('address2');
            $table->string('city');
            $table->string('postalcode');
            $table->string('country');
            $table->string('phone');
            $table->string('email');
            $table->string('org');
            $table->string('paymentmethod');
            $table->string('cardtype');
            $table->string('cardnumber');
            $table->string('card_month');
            $table->string('card_year');
            $table->string('event_type');
            $table->string('attendance');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('standard_room_occupant');
            $table->string('standard_room_quantity');
            $table->string('tented_room_occupant');
            $table->string('tented_room_quantity');
            $table->string('wooden_room_occupant');
            $table->string('wooden_room_quantity');
            $table->string('group_room_occupant');
            $table->string('group_room_quantity');
            $table->string('room_suggestion');
            $table->string('package_name');
            $table->string('adult_number');
            $table->string('adult_more');
            $table->string('child_number');
            $table->string('child_more');
            $table->string('children_number');
            $table->string('children_more');
            $table->string('package_suggestion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guestevents');
    }
}
