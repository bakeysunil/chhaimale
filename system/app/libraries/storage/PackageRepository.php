<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 19/11/2018
 * Time: 12:07
 */

namespace App\libraries\storage;
use App\model\packages;

class PackageRepository extends Repository
{
    public function __construct(packages $packages)
    {
        $this->model = $packages;
    }

}