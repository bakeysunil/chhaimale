<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:42
 */

namespace App\libraries\storage;


use App\model\activity;

class ActivitiesRepository extends Repository
{
    public function __construct(activity $activity)
    {
        $this->model= $activity;
    }

}