<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:46
 */

namespace App\libraries\storage;


use App\model\amenity;

class AmenitiesRepository extends Repository
{
    public function __construct(amenity $amenities)
    {
        $this->model = $amenities;
    }
}