<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:50
 */

namespace App\libraries\storage;


use App\model\testimonials;

class TestimonialRepository extends Repository
{
            public function __construct(testimonials $testimonial)
            {
                $this->model = $testimonial;
            }
}