<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:46
 */

namespace App\libraries\storage;


use App\model\company_detail;

class CompanyRepository extends Repository
{
    public function __construct(company_detail $company)
    {
        $this->model = $company;
    }

}