<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:48
 */

namespace App\libraries\storage;
use App\model\room_number;

class RoomNumberRepository extends Repository
{
    public function __construct(room_number $room_number)
    {
        $this->model =$room_number;
    }

}