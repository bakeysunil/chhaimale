<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:48
 */

namespace App\libraries\storage;


use App\model\foods;

class FoodRepository extends  Repository
{
    public function __construct(foods $food)
    {
        $this->model = $food;

    }
}