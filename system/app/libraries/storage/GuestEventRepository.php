<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 16/08/2018
 * Time: 17:18
 */

namespace App\libraries\storage;



use App\model\guestevent;

class GuestEventRepository extends Repository
{
    public function __construct(guestevent $guestevent)
    {
        $this->model = $guestevent;

    }

}