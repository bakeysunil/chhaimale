<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:50
 */

namespace App\libraries\storage;


use App\model\upcoming_events;

class UpcomingEventRepository extends  Repository
{
        public function __construct(upcoming_events $upcoming_event)
        {
            $this->model = $upcoming_event;

        }
}