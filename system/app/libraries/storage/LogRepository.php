<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:48
 */

namespace App\libraries\storage;


use App\model\logs;

class LogRepository extends Repository
{
    public function __construct(logs $log)
    {
        $this->model = $log;
    }

}