<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:49
 */

namespace App\libraries\storage;
use App\User;

class UserRepository extends Repository
{

    public function __construct(User $user)
    {
        $this->model = $user;
    }
}