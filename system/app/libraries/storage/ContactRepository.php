<?php
/**
 * Created by PhpStorm.
 * User: Kundan
 * Date: 24/07/2018
 * Time: 13:47
 */

namespace App\libraries\storage;


use App\model\contact;

class ContactRepository extends Repository
{
    public function __construct(contact $contact)
    {
        $this->model = $contact;
    }
}