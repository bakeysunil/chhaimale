<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class logs extends Model
{
	use Helper;
    protected $fillable = [
        'task','added_by','edited_by'
    ];
}
