<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class room_number extends Model
{
    use Helper;
    protected $fillable = [
       'name','room_type_id','booked_form','booked_to'
    ];
    public function bookings()
    {
        return $this->hasMany(booking::class, 'room_id');
    }

}
