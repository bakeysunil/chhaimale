<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class customers extends Model
{
	use Helper;
    protected $fillable = [
      'name','address','website','email','phone','vat_no','remarks','added_by','edited_by'
    ];
}
