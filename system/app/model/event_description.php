<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class event_description extends Model
{
    protected $fillable = [
        'name'
    ];
}
