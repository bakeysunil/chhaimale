<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class guestevent extends Model
{
    protected $fillable = [
        'title','first_name','last_name','address1','address2','city','postalcode','country','phone','email','org','paymentmethod','cardtype','cardnumber','card_month','card_year','event_type','attendance','start_date','end_date','standard_room_occupant',
        'standard_room_quantity','tented_room_occupant','tented_room_quantity','wooden_room_occupant','wooden_room_quantity','group_room_occupant','group_room_quantity','room_suggestion','package_name','adult_number','adult_more','child_number','child_more','children_number','children_more','package_suggestion'
    ];
}
