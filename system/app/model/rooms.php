<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use facility;


class rooms extends Model
{
	use Helper;
    protected $fillable = [
      'type','starting_price','last_price','criteria','facilities','description','total_rooms','availability','status',
        'sales_price','added_by','edited_by','image'
    ];

    /**
     * Get the facility record associated with the room.
     */
    public static function findFacility($f){
    	    $data =  \App\facility::whereId($f)->get();  
    	    return $data[0]->name;  	
    }

   
}
