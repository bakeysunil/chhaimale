<?php

namespace App\model;

use App\event_description;
use App\facility;
use App\User;

trait Helper 
{

    public function getUserName($key) {
 		$data =  json_decode(User::where('id', $key)->select('name')->get());
 		return $data[0]->name;
	}
	public function getRoomType($key){
        $data = json_decode(rooms::where('id',$key)->select('type')->get());
        return $data[0]->type;
    }
    public function getRoomNumber($key){
        $data = json_decode(room_number::where('id',$key)->select('name')->get());
        return $data[0]->name;
    }
    public function getEventfacility($key){
        $data = json_decode(event_description::where('id',$key)->select('name')->get());
          
        return $data[0]->name;
    }
    public function getRoomFacility($key){
        $data = json_decode(facility::where('id',$key)->select('name')->get());
        return $data[0]->name;
    }
	
}
