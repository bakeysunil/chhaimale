<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class foods extends Model
{
	use Helper;
    protected $fillable = [
        'name','image','added_by','edited_by','price'
    ];
}
