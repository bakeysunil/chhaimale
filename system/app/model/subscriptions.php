<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class subscriptions extends Model
{
	use Helper;
    protected $fillable = [
        'email','added_by','edited_by'
    ];
}
