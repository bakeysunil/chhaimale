<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class conference extends Model
{
	use Helper;
    protected $fillable = [
      'title','first_name','last_name','address1','address2','city','zip','country','phone','email','org','cfname','attendance','startdate','starttime','endtime','breakfasttime','lunchtime','dinnertime','standard_room_occupant','standard_room_quantity',
      	'wooden_room_occupant','wooden_room_quantity','tented_room_occupant','tented_room_quantity','group_room_occupant','group_room_quantity','enddate','style','setup_suggestion','meal','meal_suggestion','room_detail','room_suggestion','credit_card','added_by','edited_by','paymentmethod','card_type','card_number','card_month','card_year'
    ];
}
