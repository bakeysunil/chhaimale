<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class activity extends Model
{
	use Helper;
	
    protected $table = 'activities';
    protected $fillable = [
      'name','image','added_by','edited_by'
    ];

	
}
