<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
	use Helper;
    protected $fillable = [
      'name','email','mobile','subject','message','added_by'
    ];
}
