<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class settings extends Model
{
	use Helper;
    protected $fillable = [
        'site_title','logo','logo_collapsed','meta_title','meta_description','keywords','copyright_text',
        'added_by','edited_by'
    ];
}
