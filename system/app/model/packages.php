<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class packages extends Model
{
    use Helper;
    protected $fillable = [
       'name','description','price','created_by'
    ];
}
