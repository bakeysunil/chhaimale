<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class sliders extends Model
{
	use Helper;
    protected $fillable = [
      'image','link_text','order','added_by','edited_by','name'
    ];
}
