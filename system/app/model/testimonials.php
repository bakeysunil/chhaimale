<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class testimonials extends Model
{
	use Helper;
    protected $fillable = [
      'name','position','company','rating','picture','phone_number','added_by','edited_by','comment'
    ];
}
