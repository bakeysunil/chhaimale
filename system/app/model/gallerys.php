<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class gallerys extends Model
{
	use Helper;
    protected $fillable = [
      'title','image','added_by','edited_by'
    ];
}
