<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class events extends Model
{
	use Helper;
    protected $fillable = [
      'title','sub_title','description','added_by','edited_by','image'
    ];

    /**
     * Get the facility record associated with the room.
     */
    public static function findDescription($f){
        $data =  \App\event_description::whereId($f)->get();
        return $data[0]->name;
    }
}
