<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class company_detail extends Model
{
	use Helper;
    protected $fillable = [
      'name','address','resort_contact','office_contact','email','facebook_link','instagram_link','google_link','twitter_link'
        ,'map_link','added_by','edited_by'
    ];
}
