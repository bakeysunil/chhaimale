<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class bookedroom extends Model
{
    use Helper;
    protected $fillable = [
        'room_id','rooms_type_number','sub_total','service_charge','vat','total','check_in','check_out','occupancy','first_name',
        'last_name','address1','address2','city','zip','country','phone','email','org','payment_method','card_type',
        'card_number','card_month','card_year'
    ];
}
