<?php

namespace App\Http\Controllers;


use App\libraries\storage\PackageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackagesController extends Controller
{
    private $packageRepo;

    public function __construct(PackageRepository $packages)
    {
        $this->packageRepo = $packages;
    }


    public function index()
    {
        $packages = $this->packageRepo->all();
       return view('admin.package.index')->with(compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('admin.package.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);
        $data=[
            'name'=>$request->input('name'),
            'description'=>$request->input('description'),
            'price'=>$request->input('price'),
            'created_by'=>Auth::user()->id,


        ];
        $this->packageRepo->create($data);
        return redirect()->route('admin.packages');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function show(packages $packages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$package = $this->packageRepo->find($id)) {
            return view('errors.503');
        }
        return view('admin.package.update')->with(compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);
        $data=[
            'name'=>$request->input('name'),
            'description'=>$request->input('description'),
            'price'=>$request->input('price'),



        ];
        $this->packageRepo->update($id,$data);
        return redirect()->route('admin.packages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\packages  $packages
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {

    }
}
