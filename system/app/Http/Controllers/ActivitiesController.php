<?php

namespace App\Http\Controllers;

use App\libraries\storage\ActivitiesRepository;
use App\model\activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ActivitiesController extends Controller
{
    public  $activityRepo,  $img_path;
    public $title = 'Activity';

    public function __construct(ActivitiesRepository $activityRepo)
    {

        $this->activityRepo = $activityRepo;
        $this->img_path = 'assets/uploads/activity/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allActivity = $this->activityRepo->all();

        return view('admin.activities.index', compact('allActivity', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.activities.add', compact('title'));
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('activity_name'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('activity_image'))
        {
            $extension = $request->file('activity_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('activity_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->activityRepo->create($data);
        return redirect()->route('admin.activities');
    }

    public function edit($id)
    {
        if (!$activity = $this->activityRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.activities.update')->with(compact('activity','img_path'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        if (!$activity = $this->activityRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'name'=>$request->input('activity_name'),
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('activity_image'))
        {
            $extension = $request->file('activity_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('activity_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }

        $this->activityRepo->update($id,$data);
        return redirect()->route('admin.activities');

    }

    public function delete($id)
    {
        if (!$activity = $this->activityRepo->find($id)) {
            return view('errors.503');
        }
        $this->activityRepo->delete($id);
        return redirect()->route('admin.activities');
    }
}
