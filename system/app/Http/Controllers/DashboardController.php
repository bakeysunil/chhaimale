<?php

namespace App\Http\Controllers;

use App\model\bookedroom;
use App\model\guestevent;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {

    }

    public function index(){

        $data = bookedroom::get()->groupBy(function($d) {
            return Carbon::parse($d->created_at)->format('m');
        });
        $bookedroom = $data->toArray();
        $event = guestevent::get()->groupBy(function($d) {
            return Carbon::parse($d->created_at)->format('m');
        });
        $guestevent = $event->toArray();
        return view('admin.dashboard')->with(compact('bookedroom','guestevent'));
    }
}
