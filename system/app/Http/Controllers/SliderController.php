<?php

namespace App\Http\Controllers;

use App\libraries\storage\SliderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SliderController extends Controller
{
    public  $sliderRepo,  $img_path;
    public $title = 'Slider';

    public function __construct(SliderRepository $sliderRepo)
    {
        $this->sliderRepo = $sliderRepo;
        $this->img_path = 'assets/uploads/slider/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allslider = $this->sliderRepo->all();

        return view('admin.slider.index', compact('allslider', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.slider.add', compact('title'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'slider_text' => 'required|min:5',
            'slider_order' => 'required',
            'slider_image' => 'required',


        ]);
        $data=[
            'link_text'=>$request->input('slider_text'),
            'order'=>$request->input('slider_order'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('slider_image'))
        {
            $extension = $request->file('slider_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('slider_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->sliderRepo->create($data);
        return redirect()->route('admin.slider');
    }

    public function edit($id)
    {
        if (!$slider = $this->sliderRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.slider.update')->with(compact('slider','img_path'));

    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'slider_text' => 'required|min:5',
            'slider_order' => 'required',
            'slider_image' => 'required',
        ]);
        $id = $request->input('id');
        if (!$slider = $this->sliderRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'link_text'=>$request->input('slider_text'),
            'order'=>$request->input('slider_order'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('slider_image'))
        {
            $extension = $request->file('slider_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('slider_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->sliderRepo->update($id,$data);
        return redirect()->route('admin.slider');
    }

    public function delete($id)
    {
        if (!$slider = $this->sliderRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->sliderRepo->delete($id);
        }
        return redirect()->route('admin.slider');
    }
}
