<?php

namespace App\Http\Controllers;


use App\model\bookedroom;
use App\model\rooms;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
        public $logo;
        public function __construct()
        {
            $setting = DB::table('settings')
                ->get();

            $this->logo = $setting[0]->logo;
        }
        
    public function index(){
        $amenities = DB::table('amenities')
                    ->get();
        $activities = DB::table('activities')
                    ->limit(6)
                    ->get();
        $testimonial = DB::table('testimonials')
                    ->get();
        $banner = DB::table('sliders')
                ->get();
        $logo = $this->logo;
        $room = DB::table('rooms')
                ->get();

        return view('chhaimale.index')->with(compact('amenities','activities','logo','testimonial','banner','room'));
    }
    public function conferencehall(){
        $logo = $this->logo;
        return view('chhaimale.conferencehall_booking')->with(compact('logo'));
    }
    public function contact(){
        $logo = $this->logo;
        $data = DB::table('company_details')
                ->get();
        return view('chhaimale.contactus')->with(compact('logo','data'));
    }
    public function event(){
        $logo = $this->logo;
        $upcomingevents = DB::table('upcoming_events')
            ->limit(2)
            ->orderBy('id','desc')
            ->get();
        $events = DB::table('events')
            ->limit(4)
            ->orderBy('id','desc')
            ->get();
        return view('chhaimale.event')->with(compact('logo','upcomingevents','events'));
    }
    public function eventbook(){
        $logo = $this->logo;
        return view('chhaimale.event_booking')->with(compact('logo'));
    }
    public function form(){
        $logo = $this->logo;
        return view('chhaimale.fill_form')->with('logo');
    }
    public function gallery(){
        $logo = $this->logo;
        $data = DB::table('gallerys')
            ->get();
        return view('chhaimale.gallery')->with(compact('logo','data'));
    }
    public function roombook(){
        $logo = $this->logo;
        return view('chhaimale.room_booking')->with(compact('logo'));
    }
    public function roomdetail(){
        $logo = $this->logo;
        $rooms = DB::table('rooms')
            ->get();
        return view('chhaimale.room_booking_detail')->with(compact('logo'));
    }
    public function room(){
        $logo = $this->logo;
        $rooms = DB::table('rooms')
            ->get();
        return view('chhaimale.room')->with(compact('logo','rooms'));
    }
   
    public function search(){
        $logo = $this->logo;
        $rooms = DB::table('rooms')
                ->get();
        return view('chhaimale.search_rooms')->with(compact('logo','rooms'));
    }
    public function resturant(){
        $logo = $this->logo;
        $data = DB::table('restaurants')
            ->get();
        $food = DB::table('foods')
            ->limit(6)
            ->orderBy('id','desc')
            ->get();
        return view('chhaimale.resturant')->with(compact('logo','data','food'));
    }

    public function conference()
    {
        $logo = $this->logo;
        $events = DB::table('events')
            ->limit(4)
            ->orderBy('id','desc')
            ->get();
        return view('chhaimale.conference')->with(compact('logo','events'));
    }

    public function roomDetailView($id){
        $logo = $this->logo;
        $room = rooms::find($id);        
        return view('chhaimale.roomdetailview')->with(compact('logo','room'));
    }



}
