<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private $user;
    private $auth;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function showLogin(){
        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        $data=[
            'email'=>$request->input('email'),
            'password'=>$request->input('password')
        ];
        $credentials = $data;
        if (Auth::attempt($credentials, $request->has('remember'))) {
            return redirect()->intended('admin/dashboard');
        }
        return redirect()
            ->back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors(['auth' => \Lang::get('auth.failed')]);
    }

    public function showLogout() {
        Auth::logout();
        return redirect()->route('admin.login')
            ->with('flash_notice', 'You are successfully logged out');
    }
}
