<?php

namespace App\Http\Controllers;

use App\libraries\storage\EventRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    public  $eventRepo,  $img_path;
    public $title = 'Event';

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepo = $eventRepo;
        $this->img_path = 'assets/uploads/events/';
    }

    public function index()
    {
        $title = $this->title;

        $allevent = $this->eventRepo->all();

        return view('admin.event.index', compact('allevent', 'title'));

    }

    public function view($id)
    {
        $title = $this->title;
        $img_path = $this->img_path;
        if (!$event = $this->eventRepo->find($id)) {
            return view('errors.503');
        }

        return view('admin.event.single',compact('event', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;
        $description =DB::table('event_descriptions')
            ->get();

        return view('admin.event.add', compact('title','description'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'event_title' => 'required',
            'event_image'=>'required',
            'event_subtitle' => 'required',
            'event_description' => 'required',

        ]);
        $data=[
            'title'=>$request->input('event_title'),
            'sub_title'=>$request->input('event_subtitle'),
            'description'=>json_encode($request->input('event_description')),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        if($request->hasFile('event_image'))
        {
            $extension = $request->file('event_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('event_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->eventRepo->create($data);
        return redirect()->route('admin.event');
    }

    public function edit($id)
    {
        if (!$event = $this->eventRepo->find($id)) {
            return view('errors.503');
            }

        $description =DB::table('event_descriptions')
            ->get();
            $img_path = $this->img_path;
        return view('admin.event.update')->with(compact('event','description','img_path'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $this->validate($request,[
            'event_title' => 'required',
            'event_subtitle' => 'required',
            'event_description' => 'required',

        ]);
      
        
        if (!$testimonial = $this->eventRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'title'=>$request->input('event_title'),
            'sub_title'=>$request->input('event_subtitle'),
            'description'=>json_encode($request->input('event_description')),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        if($request->hasFile('event_image'))
        {
            $extension = $request->file('event_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('event_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->eventRepo->update($id,$data);
        return redirect()->route('admin.event');
    }

    public function delete($id)
    {
        if (!$event = $this->eventRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->eventRepo->delete($id);
        }
        return redirect()->route('admin.event');
    }
}
