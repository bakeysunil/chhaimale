<?php

namespace App\Http\Controllers;

use App\libraries\storage\SubRepository;
use Illuminate\Http\Request;

class SubsController extends Controller
{
    private $sub;

    public function __construct(SubRepository $sub)
    {
        $this->sub = $sub;
    }

    public function index()
    {

    }

    public function add()
    {

    }

    public function save()
    {

    }

    public function edit()
    {

    }

    public function update($id)
    {

    }

    public function delete($id)
    {

    }
}
