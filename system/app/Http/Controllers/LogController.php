<?php

namespace App\Http\Controllers;

use App\libraries\storage\LogRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LogController extends Controller
{
    public  $logRepo;
    public $title = 'Logs';

    public function __construct(LogRepository $logRepo)
    {
        $this->logRepo = $logRepo;
    }

    public function index()
    {
        $title = $this->title;

        $alllogs = $this->logRepo->all();

        return view('admin.logs.index', compact('alllogs', 'title'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.logs.add', compact('title'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'log_task' => 'required',
        ]);
        $data=[
            'task'=>$request->input('log_task'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        $this->logRepo->create($data);
        return redirect()->route('admin.logs');
    }

    public function edit($id)
    {
        if (!$log = $this->logRepo->find($id)) {
            return view('errors.503');
        }
        return view('admin.logs.update')->with(compact('log'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'log_task' => 'required',
        ]);
        $id = $request->input('id');
        $data=[
            'task'=>$request->input('log_task'),
            'edited_by'=>Auth::user()->id
        ];
        $this->logRepo->update($id,$data);
        return redirect()->route('admin.logs');
    }

    public function delete($id)
    {
        if (!$log = $this->logRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->logRepo->delete($id);
        }
        return redirect()->route('admin.logs');
    }
}
