<?php

namespace App\Http\Controllers;

use App\libraries\storage\ContactRepository;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendEmailJob;

class ContactController extends Controller
{
    public  $contacts,  $img_path;
    public $title = 'Messages';

    public function __construct(ContactRepository $contacts)
    {
        $this->contacts = $contacts;
       
    }

    public function index()
    {
    
        $title = $this->title;

        $messages = $this->contacts->all();

        return view('admin.contact.index', compact('messages', 'title'));
    }
    public function view($id)
    {
        $title = $this->title;
        $img_path = $this->img_path;
        if (!$message = $this->contacts->find($id)) {
            return view('errors.503');
        }

        return view('admin.contact.single',compact('message', 'title'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.contact.add', compact('title'));
    }

    public function save(Request $request)
    {
        $data =[
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'mobile'=>$request->input('mobile'),
            'subject'=>$request->input('subject'),
            'message'=>$request->input('message'),
        ];

        if($user = Auth::user())
        {
            $data['added_by'] = Auth::User()->id;
        }
        else{
            $data['added_by'] = 'Visitor';
        }

        
        $stat = $this->contacts->create($data);

        if($user = Auth::user()){
            return redirect()->route('admin.contact');
        }else{
            
            return redirect()->route('contact')->with('message','Submitted Successfully');
        }


    }

    public function reply($id)
    {
        $data = $this->contacts->find($id);
        return view('admin.contact.reply')->with(compact('data'));
    }

    public function send()
    {
        $emailJob = (new SendEmailJob())->delay(Carbon::now()->addSeconds(3));
        $this->dispatch($emailJob);



        return redirect()->route('admin.contact')->with('message','Message Sent Successfully');
    }
    public function delete($id)
    {
        if (!$message = $this->contacts->find($id)) {
            return view('errors.503');
        }
        $this->contacts->delete($id);
        return redirect();
    }
}
