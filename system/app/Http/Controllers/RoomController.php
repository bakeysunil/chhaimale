<?php

namespace App\Http\Controllers;

use App\libraries\storage\RoomRepository;
use App\model\packages;
use App\model\room_number;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;


class RoomController extends Controller
{
    public  $roomRepo,  $img_path;
    public $title = 'Rooms';
    public $logo;

    public function __construct(RoomRepository $roomRepo)
    {
        $this->roomRepo = $roomRepo;
        $this->img_path = 'assets/uploads/rooms/';
         $setting = DB::table('settings')
                ->get();
            $this->logo = $setting[0]->logo;

    }

    public function index()
    {
        $title = $this->title;

        $allrooms = $this->roomRepo->all();

        return view('admin.room.index', compact('allrooms', 'title'));

    }

    public function view($id)
    {
        $title = $this->title;
        $img_path = $this->img_path;

        if (!$room = $this->roomRepo->find($id)) {
            return view('errors.503');
        }

        return view('admin.room.single',compact('room', 'title','img_path','logo'));
    }
    public  function check(Request $request){
        //checking the room availability from room numbers according to the date provided
        $time_from = date('Y-m-d',strtotime($request->input('check_in'))); //date format
        $time_to = date('Y-m-d',strtotime($request->input('check_out')));
        $date = [
            'check-in' => $request->input('check_in'),
            'check-out' => $request->input('check_out'),

        ];

        $this->storedate($date); // stroing the date in session
        if ($request->isMethod('POST')) {

            //Method 1 to get room available using using DB ::select
            //$data = DB::select('SELECT * FROM `room_numbers` WHERE NOT EXISTS(select room_id from `bookings` where date(`time_from`) >= ? AND date(`time_to`) <=? AND room_id = room_numbers.id)',[$time_from,$time_to]);

            //Method 2 Using Eloquent Laravel
            $data = room_number::whereDoesntHave('bookings', function ($q) use ($time_from, $time_to) {
                $q->where(function ($q2) use ($time_from, $time_to) {
                    $q2->whereDate('time_from', '>=', $time_from)
                        ->WhereDate('time_to', '<=', $time_to);
                });
            })->get();
        } else {
            $data = null;
        }

        // getting the rooms that are available according to date
        $room_types = DB::table('rooms')
                    ->get(); // get rooms types

        foreach($room_types as $room_type){
            $room_name = strtolower(str_replace(' ', '_', $room_type->type));
            $room_count = 1;
            $room_type_count[$room_name] = 0;
            $room_number_select[$room_name] = array();
            foreach ($data as $d){

                $room_id = $d->room_type_id;
                if($room_type->id == $room_id){
                    $room_type_count[$room_name] =$room_count++;
                    array_push($room_number_select[$room_name],$d);


                }

            }
        } // getting number of room available according to the types

        $logo = $this->logo;
        $rooms = DB::table('rooms')
            ->get();
        $packages = packages::get();

        return view('chhaimale.search_result')->with(compact('logo','rooms','room_type_count','room_number_select','packages'));
    }
//// functions regarding rooms bookings
    public function storedate($date)
    {
        // storing the date
        session()->forget('date'); // cleaning the date variable in session
        session::push('date',$date); //inserting the date var for fuether use
    }

    public function storedata($data)
    {
        session()->forget('data'); //forgetitng the data var. so there will be no mismatch
        // session()->forget('date'); // pushing the data in session

        session::push('data',$data);
    }
    public function storedatainfo($data)
    {
        session()->forget('datainfo'); //forgetitng the data var. so there will be no mismatch
        // session()->forget('date'); // pushing the data in session

        session::push('datainfo',$data);
    }
///// end of functions
    public function search(Request $request)
    {


        $title = $this->title;
        $img_path = $this->img_path;
          $logo = $this->logo;

        $rooms = $this->roomRepo->findMany($request->input('room_id'));


        $date = [
            'check-in' => $request->input('check-in'),
            'check-out' => $request->input('check-out'),

        ];
        $number_of_adults = 0;
        $number_of_childrens = 0;
        foreach ($rooms as $key => $room) { // as we have dynamic name for our form we are using foreach to put value dynamically
             $room_name = strtolower(str_replace(' ', '_', $room->type));
            $number_of_adults += $request->input("{$room_name}_adult");
            $number_of_childrens += $request->input("{$room_name}_child");
             $data[] = [
               "{$room_name}_adult" => $request->input("{$room_name}_adult"),
               "{$room_name}_child" => $request->input("{$room_name}_child"),
               "{$room_name}_children" => $request->input("{$room_name}_children"),
               "{$room_name}_room" => $request->input("{$room_name}")
             ];
           }
           $datainfo = ['packages' => $request->input('package'),
                        'number_of_adults'=>$number_of_adults,
                        'number_of_childrens'=>$number_of_childrens
           ];




           $this->storedata($data);
           $this->storedatainfo($datainfo);







          
        return view('chhaimale.room_booking')->with(compact('rooms','data','date','logo'));
        //redirecting to booking 
    }

    public function room_detail(request $request){

            if(!empty($request->input('room_id_standard'))){
                $room_id = $request->input('room_id_standard');
                $discount = 0;
            }elseif (!empty($request->input('room_id_member'))){
                $room_id = $request->input('room_id_member');
                $discount = 15;
            }else{
                $room_id = $request->input('room_id_advance');
                $discount = 25;
            }

            $start =  Carbon::parse(session('date')[0]['check-in']);
            $end =  Carbon::parse(session('date')[0]['check-out']);
            $days = $end->diffInDays($start) + 1 ; //calc number of days from date
            //package id
            $package_id = session('datainfo')[0]['packages'];

            //number of adults
            $adult_num = session('datainfo')[0]['number_of_adults'];
            //number of childs
            $child_num = session('datainfo')[0]['number_of_childrens'];


            $package = packages::findOrFail($package_id);
            $package_price = $package->price;

            
           $title = $this->title;
            $img_path = $this->img_path;
             $logo = $this->logo;
           $rooms = $this->roomRepo->findMany($room_id);

           $room_cost = 0; // intialising room cost for use in loop
           $i = 0;

           foreach($rooms as $room){
               $room_name = strtolower(str_replace(' ', '_', $room->type));
//               $room_cost+= $room->last_price;
               $room_number_id = session('data')[0][$i][$room_name.'_room']; // getting the room_number from session
               //count number of rooms

               $room_number = count($room_number_id);

               $per_room_cost[$i] =
                   [
                       $room_name =>    $room->last_price * $room_number  + $package_price * session('data')[0][$i][$room_name.'_adult'] + ($package_price/2) * session('data')[0][$i][$room_name.'_child']
                   ]; // calc the per room cost

               $room_cost += $per_room_cost[$i][$room_name];

               //calc total cost
                   $i++;
           }
            
            $totaldiscount  = ($discount/100) * $room_cost;
           $service_charge = (10/100)*$room_cost; //calc service charge
           $vat = (13/100)*($room_cost + $service_charge); //calc vat
            $calcdiscount = ($room_cost + $service_charge + $vat);
           $total = $room_cost + $service_charge + $vat -$totaldiscount;
            $room_data = [
                'room_cost' => $room_cost,
                'service_charge' => $service_charge,
                'discount'=> $totaldiscount,
                'vat' => $vat,
                'total' => $total,
                'id' => $rooms,
                'per_room_cost' => $per_room_cost
            ];
            //creating var for room data to be put in session

           session()->forget('room_data'); //forgetting the var
           session()->push('room_data',$room_data); //pushing the var in session to be used in future
          return view('chhaimale.room_booking_detail', compact('rooms','logo','title','img_path','room_cost','service_charge','vat','total','per_room_cost','totaldiscount'));

    }
    public function add()
    {

        $title = $this->title;
        $facility = DB::table('facilities')
            ->get();
        return view('admin.room.add', compact('title','facility'));

    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'room_type' => 'required|min:5',
            'room_facilities' => 'required',
            'room_image'=>'image|required',
            'room_description' => 'required',
            'room_last_price' => 'required',
            'room_starting_price' => 'required',
            'total_rooms'=>'required',
            'room_sales_price'=>'required',
            'room_availability'=>'required',
            'room_status'=>'required',
            'room_criteria'=>'required',

        ]);
        $data=[
            'type'=>$request->input('room_type'),
            'facilities'=>json_encode(($request->input('room_facilities'))),
            'description'=>$request->input('room_description'),
            'last_price'=>$request->input('room_last_price'),
            'starting_price'=>$request->input('room_starting_price'),
            'total_rooms'=>$request->input('total_rooms'),
            'sales_price'=>$request->input('room_sales_price'),
            'availability'=>$request->input('room_availability'),
            'status'=>$request->input('room_status'),
            'criteria'=>$request->input('room_criteria'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        if($request->hasFile('room_image'))
        {
            $extension = $request->file('room_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('room_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->roomRepo->create($data);
        return redirect()->route('admin.rooms');
    }

    public function edit($id)
    {
        if (!$room = $this->roomRepo->find($id)) {
            return view('errors.503');
        }
        $facility = DB::table('facilities')
            ->get();
            $img_path = $this->img_path;
        return view('admin.room.update')->with(compact('room','facility','img_path'));
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'room_type' => 'required|min:5',
            'room_image'=>'image',
            'room_facilities' => 'required',
            'room_description' => 'required',
            'room_last_price' => 'required',
            'room_starting_price' => 'required',
            'total_rooms'=>'required',
            'room_sales_price'=>'required',
            'room_availability'=>'required',
            'room_status'=>'required',
            'room_criteria'=>'required',

        ]);
        $id = $request->input('id');
        $data=[
            'type'=>$request->input('room_type'),
            'facilities'=>json_encode(($request->input('room_facilities'))),
            'description'=>$request->input('room_description'),
            'last_price'=>$request->input('room_last_price'),
            'starting_price'=>$request->input('room_starting_price'),
            'total_rooms'=>$request->input('total_rooms'),
            'sales_price'=>$request->input('room_sales_price'),
            'availability'=>$request->input('room_availability'),
            'status'=>$request->input('room_status'),
            'criteria'=>$request->input('room_criteria'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];
        if($request->hasFile('room_image'))
        {
            $extension = $request->file('room_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('room_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }

        $this->roomRepo->update($id,$data);
        return redirect()->route('admin.rooms');

    }

    public function delete($id)
    {
        if (!$room = $this->roomRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->roomRepo->delete($id);
        }
        return redirect()->route('admin.rooms');
    }
}
