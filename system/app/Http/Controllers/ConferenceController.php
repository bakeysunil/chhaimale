<?php

namespace App\Http\Controllers;

use App\libraries\storage\ConferenceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class ConferenceController extends Controller
{
    public  $conference, $img_path;
    public $title = 'Conference';

    public function __construct(ConferenceRepository $conference)
    {
        $this->conference = $conference;
    }

    public function index()
    {
        $title = $this->title;

        $conference_bookings = $this->conference->all();

        return view('admin.conference.index', compact('conference_bookings', 'title'));
    }
    public function View($id)
    {
        $title = $this->title;

        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
       $end = Carbon::parse($conference->enddate);
        $start = Carbon::parse($conference->startdate);
        $length = $end->diffInDays($start);
        return view('admin.conference.single',compact('conference', 'title','length'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.conference.add', compact('title'));
    }

    public function save(Request $request)
    {
        $data = [
            'title' =>(empty($request->input('title'))? 'NA':$request->input('title')),
            'first_name' =>(empty($request->input('firstname'))? 'NA':$request->input('firstname')),
            'last_name' => (empty($request->input('lastname'))? 'NA':$request->input('lastname')),
            'address1' => (empty($request->input('addressline1'))? 'NA':$request->input('addressline1')),
            'address2' => (empty($request->input('addressline2'))? 'NA':$request->input('addressline2')),
             'city'=>(empty($request->input('city'))? 'NA':$request->input('city')),
            'zip'=>(empty($request->input('postalcode'))? 'NA':$request->input('postalcode')),
            'country'=>(empty($request->input('country'))? 'NA':$request->input('country')),
            'phone'=>(empty($request->input('phone'))? 'NA':$request->input('phone')),
            'email'=>(empty($request->input('email'))? 'NA':$request->input('email')),
            'org'=>(empty($request->input('organizationname'))? 'NA':$request->input('organizationname')),
            'cfname'=>(empty($request->input('conferencename'))? 'NA':$request->input('conferencename')),
            'attendance'=>(empty($request->input('attendence'))? 'NA':$request->input('attendence')),
            'startdate'=>(empty($request->input('conference_start'))? 'NA':$request->input('conference_start')),
            'starttime'=>json_encode($request->input('conferencestart')),
            'endtime'=>json_encode($request->input('conferenceend')),
            'enddate'=>(empty($request->input('conference_end'))? 'NA':$request->input('conference_end')),
            'style'=>json_encode($request->input('layout')),
            'setup_suggestion'=>(empty($request->input('layout_suggestion'))? 'NA':$request->input('layout_suggestion')),
            'breakfasttime' =>json_encode($request->input('breakfasttime')),
            'lunchtime' =>json_encode($request->input('lunchtime')),
            'dinnertime' =>json_encode($request->input('dinnertime')),
        'standard_room_occupant'=>(empty($request->input('standard_room_occupant'))? 'NA':$request->input('standard_room_occupant')),
        'standard_room_quantity'=>(empty($request->input('standard_room_quantity'))? 'NA':$request->input('standard_room_quantity')),
            'wooden_room_occupant'=>(empty($request->input('wooden_room_occupant'))? 'NA':$request->input('wooden_room_occupant')),
            'wooden_room_quantity'=>(empty($request->input('wooden_room_quantity'))? 'NA':$request->input('wooden_room_quantity')),
            'tented_room_occupant'=>(empty($request->input('tented_room_occupant'))? 'NA':$request->input('tented_room_occupant')),
            'tented_room_quantity'=>(empty($request->input('tented_room_quantity'))? 'NA':$request->input('tented_room_quantity')),
            'group_room_occupant'=>(empty($request->input('group_room_occupant'))? 'NA':$request->input('group_room_occupant')),
            'group_room_quantity'=>(empty($request->input('group_room_quantity'))? 'NA':$request->input('group_room_quantity')),
            'meal_suggestion'=>(empty($request->input('food_suggestion'))? 'NA':$request->input('food_suggestion')),
            'room_suggestion'=>(empty($request->input('room_suggestion'))? 'NA':$request->input('room_suggestion')),
            'paymentmethod'=>(empty($request->input('paymentmethod'))? 'NA':$request->input('paymentmethod')),
            'card_type'=>(empty($request->input('card_type'))? 'NA':$request->input('card_type')),
            'card_number'=>(empty($request->input('card_number'))? 'NA':$request->input('card_number')),
            'card_month'=>(empty($request->input('card_month'))? 'NA':$request->input('card_month')),
            'card_year'=>(empty($request->input('card_year'))? 'NA':$request->input('card_year')),
            'added_by'=>0,
            'edited_by'=>0
        ];
        // dd($data);
        // $data=[
        //     'title'=>$request->input('title'),
        //     'first_name'=>$request->input('first_name'),
        //     'last_name'=>$request->input('last_name'),
        //     'address1'=>$request->input('address1'),
        //     'address2'=>$request->input('address2'),
        //     'city'=>$request->input('city'),
        //     'zip'=>$request->input('zip'),
        //     'country'=>$request->input('country'),
        //     'phone'=>$request->input('phone'),
        //     'email'=>$request->input('email'),
        //     'org'=>$request->input('org'),
        //     'cfname'=>$request->input('cfname'),
        //     'attendance'=>$request->input('attendance'),
        //     'startdate'=>$request->input('startdate'),
        //     'starttime'=>$request->input('starttime'),
        //     'endtime'=>$request->input('endtime'),
        //     'enddate'=>$request->input('enddate'),
        //     'style'=>$request->input('style'),
        //     'setup_suggestion'=>$request->input('setup_suggestion'),
        //     'meal'=>json_encode($request->input('meal')),
        //     'meal_suggestion'=>$request->input('meal_suggestion'),
        //     'room_detail'=>json_encode($request->input('room_detail')),
        //     'room_suggestion'=>$request->input('room_suggestion'),
        //     'credit_card'=>$request->input('credit_card'),
        //     'added_by'=>Auth::user()->id,
        //     'edited_by'=>Auth::user()->id
        // ];

        $this->conference->create($data);
        return redirect()->back();
        // return redirect()->route('admin.conference');
    }

    public function edit($id)
    {
        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;
        return view('admin.conference.update')->with(compact('conference','img_path'));
    }

    public function update($id,Request $request)
    {

        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        $data=[
            'title' =>(empty($request->input('title'))? 'NA':$request->input('title')),
            'first_name' =>(empty($request->input('firstname'))? 'NA':$request->input('firstname')),
            'last_name' => (empty($request->input('lastname'))? 'NA':$request->input('lastname')),
            'address1' => (empty($request->input('addressline1'))? 'NA':$request->input('addressline1')),
            'address2' => (empty($request->input('addressline2'))? 'NA':$request->input('addressline2')),
            'city'=>(empty($request->input('city'))? 'NA':$request->input('city')),
            'zip'=>(empty($request->input('postalcode'))? 'NA':$request->input('postalcode')),
            'country'=>(empty($request->input('country'))? 'NA':$request->input('country')),
            'phone'=>(empty($request->input('phone'))? 'NA':$request->input('phone')),
            'email'=>(empty($request->input('email'))? 'NA':$request->input('email')),
            'org'=>(empty($request->input('organizationname'))? 'NA':$request->input('organizationname')),
            'cfname'=>(empty($request->input('conferencename'))? 'NA':$request->input('conferencename')),
            'attendance'=>(empty($request->input('attendence'))? 'NA':$request->input('attendence')),
            'paymentmethod'=>(empty($request->input('paymentmethod'))? 'NA':$request->input('paymentmethod')),
            'card_type'=>(empty($request->input('card_type'))? 'NA':$request->input('card_type')),
            'card_number'=>(empty($request->input('card_number'))? 'NA':$request->input('card_number')),
            'card_month'=>(empty($request->input('card_month'))? 'NA':$request->input('card_month')),
            'card_year'=>(empty($request->input('card_year'))? 'NA':$request->input('card_year')),
            'edited_by'=>Auth::user()->id
        ];

        $this->conference->update($id,$data);
        return redirect()->route('admin.conference')->with('flash_notice','Update Successfully');

    }

    public function changedate($id)
    {
        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        $end = Carbon::parse($conference->enddate);
        $start = Carbon::parse($conference->startdate);
        $length = $end->diffInDays($start);
        return view('admin.conference.changedate')->with(compact('conference','length'));
    }

    public function datechanged($id,Request $request)
    {

        $data = [
            'startdate'=>(empty($request->input('conference_start'))? 'NA':$request->input('conference_start')),
            'starttime'=>json_encode($request->input('conferencestart')),
            'endtime'=>json_encode($request->input('conferenceend')),
            'enddate'=>(empty($request->input('conference_end'))? 'NA':$request->input('conference_end')),
            'style'=>json_encode($request->input('layout')),
            'setup_suggestion'=>(empty($request->input('layout_suggestion'))? 'NA':$request->input('layout_suggestion')),
            'breakfasttime' =>json_encode($request->input('breakfasttime')),
            'lunchtime' =>json_encode($request->input('lunchtime')),
            'dinnertime' =>json_encode($request->input('dinnertime')),
            'standard_room_occupant'=>(empty($request->input('standard_room_occupant'))? 'NA':$request->input('standard_room_occupant')),
            'standard_room_quantity'=>(empty($request->input('standard_room_quantity'))? 'NA':$request->input('standard_room_quantity')),
            'wooden_room_occupant'=>(empty($request->input('wooden_room_occupant'))? 'NA':$request->input('wooden_room_occupant')),
            'wooden_room_quantity'=>(empty($request->input('wooden_room_quantity'))? 'NA':$request->input('wooden_room_quantity')),
            'tented_room_occupant'=>(empty($request->input('tented_room_occupant'))? 'NA':$request->input('tented_room_occupant')),
            'tented_room_quantity'=>(empty($request->input('tented_room_quantity'))? 'NA':$request->input('tented_room_quantity')),
            'group_room_occupant'=>(empty($request->input('group_room_occupant'))? 'NA':$request->input('group_room_occupant')),
            'group_room_quantity'=>(empty($request->input('group_room_quantity'))? 'NA':$request->input('group_room_quantity')),
            'meal_suggestion'=>(empty($request->input('food_suggestion'))? 'NA':$request->input('food_suggestion')),
            'room_suggestion'=>(empty($request->input('room_suggestion'))? 'NA':$request->input('room_suggestion')),
            'edited_by'=>Auth::user()->id,
        ];
      $this->conference->update($id,$data);
      return redirect()->route('admin.conference')->with('flash_notice','Update successfully');
    }

    public function delete($id)
    {
        if (!$conference = $this->conference->find($id)) {
            return view('errors.503');
        }
        $this->conference->delete($id);
        return redirect()->route('admin.conference');
    }
}
