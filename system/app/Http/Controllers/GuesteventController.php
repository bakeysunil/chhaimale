<?php

namespace App\Http\Controllers;

use App\guestevent;
use App\libraries\storage\GuestEventRepository;
use Illuminate\Http\Request;

class GuesteventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     *
     */
    public $guestevent;
    public $title = 'Guest Event';
    public function __construct(GuestEventRepository $guestevent)
    {
        $this->guesteventRepo = $guestevent;
    }

    public function index()
    {
        $title = $this->title;

        $events = $this->guesteventRepo->all();
        return view('admin.guestevent.index', compact('events', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $title = $this->title;

        return view('admin.guestevent.add', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {

        $data = [
            'title' =>(empty($request->input('title'))? 'NA':$request->input('title')),
            'first_name' =>(empty($request->input('firstname'))? 'NA':$request->input('firstname')),
            'last_name' => (empty($request->input('lastname'))? 'NA':$request->input('lastname')),
            'address1' => (empty($request->input('addressline1'))? 'NA':$request->input('addressline1')),
            'address2' => (empty($request->input('addressline2'))? 'NA':$request->input('addressline2')),
            'city'=>(empty($request->input('city'))? 'NA':$request->input('city')),
            'postalcode'=>(empty($request->input('postalcode'))? 'NA':$request->input('postalcode')),
            'country'=>(empty($request->input('country'))? 'NA':$request->input('country')),
            'phone'=>(empty($request->input('phone'))? 'NA':$request->input('phone')),
            'email'=>(empty($request->input('email'))? 'NA':$request->input('email')),
            'org'=>(empty($request->input('organizationname'))? 'NA':$request->input('organizationname')),
            'event_type'=>(empty($request->input('event_type'))? 'NA':$request->input('event_type')),
            'attendance'=>(empty($request->input('attendence'))? 'NA':$request->input('attendence')),
            'start_date'=>(empty($request->input('event_start'))? 'NA':$request->input('event_start')),
            'end_date'=>(empty($request->input('event_end'))? 'NA':$request->input('event_end')),
            'standard_room_occupant'=>(empty($request->input('standard_room_occupant'))? 'NA':$request->input('standard_room_occupant')),
            'standard_room_quantity'=>(empty($request->input('standard_room_quantity'))? 'NA':$request->input('standard_room_quantity')),
            'wooden_room_occupant'=>(empty($request->input('wooden_room_occupant'))? 'NA':$request->input('wooden_room_occupant')),
            'wooden_room_quantity'=>(empty($request->input('wooden_room_quantity'))? 'NA':$request->input('wooden_room_quantity')),
            'tented_room_occupant'=>(empty($request->input('tented_room_occupant'))? 'NA':$request->input('tented_room_occupant')),
            'tented_room_quantity'=>(empty($request->input('tented_room_quantity'))? 'NA':$request->input('tented_room_quantity')),
            'group_room_occupant'=>(empty($request->input('group_room_occupant'))? 'NA':$request->input('group_room_occupant')),
            'group_room_quantity'=>(empty($request->input('group_room_quantity'))? 'NA':$request->input('group_room_quantity')),
            'room_suggestion'=>(empty($request->input('room_suggestion'))? 'NA':$request->input('room_suggestion')),
            'paymentmethod'=>(empty($request->input('paymentmethod'))? 'NA':$request->input('paymentmethod')),
            'cardtype'=>(empty($request->input('card_type'))? 'NA':$request->input('card_type')),
            'cardnumber'=>(empty($request->input('card_number'))? 'NA':$request->input('card_number')),
            'card_month'=>(empty($request->input('card_month'))? 'NA':$request->input('card_month')),
            'card_year'=>(empty($request->input('card_year'))? 'NA':$request->input('card_year')),
            'package_name'=>(empty($request->input('package'))? 'NA':$request->input('package')),
            'package_suggestion'=>(empty($request->input('package_suggestion'))? 'NA':$request->input('package_suggestion')),
            'adult_number'=>$request->input('adult_number'),
            'adult_more'=>$request->input('adult_more'),
            'child_number'=>$request->input('child_number'),
            'child_more'=>$request->input('child_more'),
            'children_number'=>$request->input('children_number'),
            'children_more'=>$request->input('children_more'),
            'added_by'=>0,
            'edited_by'=>0
        ];
       
        $this->guesteventRepo->create($data);
        return redirect()->route('admin.guestevent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\guestevent  $guestevent
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $title = $this->title;

        if (!$event = $this->guesteventRepo->find($id)) {
            return view('errors.503');
        }

        return view('admin.guestevent.single',compact('event', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\guestevent  $guestevent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$event = $this->guesteventRepo->find($id)) {
            return view('errors.503');
        }

        return view('admin.guestevent.update')->with(compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\guestevent  $guestevent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $data = [
            'title' =>(empty($request->input('title'))? 'NA':$request->input('title')),
            'first_name' =>(empty($request->input('firstname'))? 'NA':$request->input('firstname')),
            'last_name' => (empty($request->input('lastname'))? 'NA':$request->input('lastname')),
            'address1' => (empty($request->input('addressline1'))? 'NA':$request->input('addressline1')),
            'address2' => (empty($request->input('addressline2'))? 'NA':$request->input('addressline2')),
            'city'=>(empty($request->input('city'))? 'NA':$request->input('city')),
            'postalcode'=>(empty($request->input('postalcode'))? 'NA':$request->input('postalcode')),
            'country'=>(empty($request->input('country'))? 'NA':$request->input('country')),
            'phone'=>(empty($request->input('phone'))? 'NA':$request->input('phone')),
            'email'=>(empty($request->input('email'))? 'NA':$request->input('email')),
            'org'=>(empty($request->input('organizationname'))? 'NA':$request->input('organizationname')),
            'event_type'=>(empty($request->input('event_type'))? 'NA':$request->input('event_type')),
            'attendance'=>(empty($request->input('attendence'))? 'NA':$request->input('attendence')),
            'start_date'=>(empty($request->input('event_start'))? 'NA':$request->input('event_start')),
            'end_date'=>(empty($request->input('event_end'))? 'NA':$request->input('event_end')),
            'standard_room_occupant'=>(empty($request->input('standard_room_occupant'))? 'NA':$request->input('standard_room_occupant')),
            'standard_room_quantity'=>(empty($request->input('standard_room_quantity'))? 'NA':$request->input('standard_room_quantity')),
            'wooden_room_occupant'=>(empty($request->input('wooden_room_occupant'))? 'NA':$request->input('wooden_room_occupant')),
            'wooden_room_quantity'=>(empty($request->input('wooden_room_quantity'))? 'NA':$request->input('wooden_room_quantity')),
            'tented_room_occupant'=>(empty($request->input('tented_room_occupant'))? 'NA':$request->input('tented_room_occupant')),
            'tented_room_quantity'=>(empty($request->input('tented_room_quantity'))? 'NA':$request->input('tented_room_quantity')),
            'group_room_occupant'=>(empty($request->input('group_room_occupant'))? 'NA':$request->input('group_room_occupant')),
            'group_room_quantity'=>(empty($request->input('group_room_quantity'))? 'NA':$request->input('group_room_quantity')),
            'room_suggestion'=>(empty($request->input('room_suggestion'))? 'NA':$request->input('room_suggestion')),
            'paymentmethod'=>(empty($request->input('paymentmethod'))? 'NA':$request->input('paymentmethod')),
            'cardtype'=>(empty($request->input('card_type'))? 'NA':$request->input('card_type')),
            'cardnumber'=>(empty($request->input('card_number'))? 'NA':$request->input('card_number')),
            'card_month'=>(empty($request->input('card_month'))? 'NA':$request->input('card_month')),
            'card_year'=>(empty($request->input('card_year'))? 'NA':$request->input('card_year')),
            'package_name'=>(empty($request->input('package'))? 'NA':$request->input('package')),
            'package_suggestion'=>(empty($request->input('package_suggestion'))? 'NA':$request->input('package_suggestion')),
            'adult_number'=>$request->input('adult_number'),
            'adult_more'=>$request->input('adult_more'),
            'child_number'=>$request->input('child_number'),
            'child_more'=>$request->input('child_more'),
            'children_number'=>$request->input('children_number'),
            'children_more'=>$request->input('children_more'),
            'added_by'=>0,
            'edited_by'=>0
        ];
        $this->guesteventRepo->update($id,$data);
        return redirect()->route('admin.guestevent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\guestevent  $guestevent
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!$event = $this->guesteventRepo->find($id)) {
            return view('errors.503');
        }
        $this->guesteventRepo->delete($id);
        return redirect()->route('admin.guestevent.index');
    }
}
