<?php

namespace App\Http\Controllers;

use App\libraries\storage\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;


class UserController extends Controller
{
    public  $userRepo,  $img_path;
    public $title = 'User';

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
        $this->img_path = 'assets/uploads/users/';
    }

    public function index()
    {
        $title = $this->title;
        $img_path = $this->img_path;
        $users = $this->userRepo->all();
        return view('admin.user.index',compact('users', 'title','img_path'));
    }

    public function view()
    {

    }

    public function profile($id)
    {
        $title = $this->title;
        $img_path = $this->img_path;

        if (!$user = $this->userRepo->find($id)) {
            return view('errors.503');
        }

        return view('admin.user.profile',compact('user', 'title','img_path'));
    }

    public function admin_credential_rules(array $data)
    {
        $messages = [
            'confirm_password.required' => 'Please enter password',
        ];

        $validator = Validator::make($data, [
            'user_password' => 'required',
            'confirm_password' => 'required|same:user_password',
        ], $messages);


        return $validator;
    }
    public function update(Request $request)
    {
       $data = [
           'name' => $request->input('user_name'),
           'email' => $request->input('user_email'),
       ];
       $id = $request->input('id');
       $this->userRepo->update($id,$data);
       if($request->input('user_password')!= null){
           $request_data = $request->All();

           $validator = $this->admin_credential_rules($request_data);
           if ($validator->fails()) {
               return redirect()->back()->with('unmatch','Passwords didnot matched');
           }else{
               $user_id = Auth::User()->id;
               $obj_user = User::find($user_id);
               $obj_user->password = Hash::make($request_data['user_password']);;
               $obj_user->save();
               return redirect()->route('admin.user.profile')->with('message', 'Password Updated');;
           }
       }else{
           return redirect()->back()->with('message','Updated Successfully');
       }
    }

    public function delete($id)
    {

        if (!$testimonial = $this->userRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->userRepo->delete($id);
        }
        return redirect()->route('admin.users');
    }
}
