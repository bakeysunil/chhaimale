<?php

namespace App\Http\Controllers;

use App\model\bookedroom;
use App\libraries\storage\BookedRoomRepository;
use App\model\room_number;
use App\model\rooms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;


class BookedroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public  $bookedroomRepo;
    public $title = 'Booked Rooms';
    public function __construct(BookedRoomRepository $bookedroom)
    {
    $this->bookedroomRepo = $bookedroom;

    }
    public function index()
    {
        $title = $this->title;
        $data = $this->bookedroomRepo->all();

        return view('admin.roombook.index', compact('data', 'title'));
    }

    public function view($id)
    {
        $title = $this->title;

        if (!$data = $this->bookedroomRepo->find($id)) {
            return view('errors.503');
        }

//        $end = Carbon::parse($conference->enddate);
//        $start = Carbon::parse($conference->startdate);
//        $length = $end->diffInDays($start);
        return view('admin.roombook.single',compact('data', 'title'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        //checking the room availability from room numbers according to the date provided
        $time_from = date('Y-m-d',strtotime($request->input('check_in'))); //date format
        $time_to = date('Y-m-d',strtotime($request->input('check_out')));
        $date = [
            'check-in' => $request->input('check_in'),
            'check-out' => $request->input('check_out'),

        ];

        // storing the date
        $this->storedate($date);

        if ($request->isMethod('POST')) {

            //Method 1 to get room available using using DB ::select
            //$data = DB::select('SELECT * FROM `room_numbers` WHERE NOT EXISTS(select room_id from `bookings` where date(`time_from`) >= ? AND date(`time_to`) <=? AND room_id = room_numbers.id)',[$time_from,$time_to]);

            //Method 2 Using Eloquent Laravel
            $data = room_number::whereDoesntHave('bookings', function ($q) use ($time_from, $time_to) {
                $q->where(function ($q2) use ($time_from, $time_to) {
                    $q2->whereDate('time_from', '>=', $time_from)
                        ->WhereDate('time_to', '<=', $time_to);
                });
            })->get();
        } else {
            $data = null;
        }

        // getting the rooms that are available according to date
        $room_types = DB::table('rooms')
            ->get(); // get rooms types

        foreach($room_types as $room_type){
            $room_name = strtolower(str_replace(' ', '_', $room_type->type));
            $room_count = 1;
            $room_type_count[$room_name] = 0;
            $room_number_select[$room_name] = array();
            foreach ($data as $d){

                $room_id = $d->room_type_id;
                if($room_type->id == $room_id){
                    $room_type_count[$room_name] =$room_count++;
                    array_push($room_number_select[$room_name],$d);


                }

            }
        } // getting number of room available according to the types


        $rooms = DB::table('rooms')
            ->get();

        return view('admin.roombook.add')->with(compact('rooms','room_type_count','room_number_select'));
    }
    /// functions needed for search and store in session of booked rooms
    ///
    ///
    ///


    public function storedate($date)
    {
        session()->forget('date'); // cleaning the date variable in session
        session::push('date',$date); //inserting the date var for fuether use
    }

    public  function storedata($data)
    {
        session()->forget('data'); //forgetitng the data var. so there will be no mismatch
        // session()->forget('date'); // pushing the data in session

        session::push('data',$data);
    }
    public function rooms($request)
    {
        $rooms = rooms::findMany($request->input("room_id"));
        foreach ($rooms as $key => $room) { // as we have dynamic name for our form we are using foreach to put value dynamically
            $room_name = strtolower(str_replace(' ', '_', $room->type));

            $data[] = [
                "{$room_name}_adult" => $request->input("{$room_name}_adult"),
                "{$room_name}_child" => $request->input("{$room_name}_child"),
                "{$room_name}_children" => $request->input("{$room_name}_children"),
                "{$room_name}_room" => $request->input("{$room_name}")
            ];
        }
        $this->storedata($data);
        return $rooms;

    }

    public function cost($rooms)
    {
        $start =  Carbon::parse(session('date')[0]['check-in']);
        $end =  Carbon::parse(session('date')[0]['check-out']);
        $days = $end->diffInDays($start) + 1 ; //calc number of days from date
        $room_cost = 0; // intialising room cost for use in loop
        $i = 0;

        foreach($rooms as $room){
            $room_name = strtolower(str_replace(' ', '_', $room->type));
//               $room_cost+= $room->last_price;
            $room_number_id = session('data')[0][$i][$room_name.'_room']; // getting the room_number from session

            $room_number = count($room_number_id);

            $per_room_cost[$i] =
                [
                    $room_name =>    $room->last_price * $room_number * $days
                ]; // calc the per room cost
            $room_cost += $per_room_cost[$i][$room_name];
            //calc total cost
            $i++;
        }
        $service_charge = (10/100)*$room_cost; //calc service charge
        $vat = (13/100)*($room_cost + $service_charge); //calc vat
        $total = $room_cost + $service_charge + $vat;
        $room_data = [
            'room_cost' => $room_cost,
            'service_charge' => $service_charge,
            'vat' => $vat,
            'total' => $total,
            'id' => $rooms,
            'per_room_cost' => $per_room_cost
        ];
        $this->storeroomdata($room_data);
        //creating var for room data to be put in session
        return true;

    }

    public function storeroomdata($room_data)
    {
        session()->forget('room_data'); //forgetting the var
        session()->push('room_data',$room_data); //pushing the var in session to be used in future
    }
    /// end of functions
    public function form(Request $request)
    {


        $rooms = $this->rooms($request);

        if($this->cost($rooms))
        return view('admin.roombook.form');

    }
    public function add()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $rooms = session('room_data')[0]['id']; //get the rooms data from session


        $i= 0;
        foreach ($rooms as $room){
            $room_name = strtolower(str_replace(' ', '_', $room->type));
            $room_id[$i] = $room['id'];
            $booked_room[$i][$room_name] = session('data')[0][$i][$room_name.'_room'];
            $occupancy[$i][$room_name.'_adult'] = session('data')[0][$i][$room_name.'_adult'];
            $occupancy[$i][$room_name.'_child'] = session('data')[0][$i][$room_name.'_child'];
            $occupancy[$i][$room_name.'_children'] = session('data')[0][$i][$room_name.'_children'];
            $booked_room_number[$i] = array();
            array_push($booked_room_number[$i],$booked_room[$i][$room_name]);

            $i++;
            //getting all the needed data from session and putting it in var
        }
        $brn = array();

        foreach($booked_room_number as $br){
            array_push($brn,$br[0]);

        }


        $sub_total = session('room_data')[0]['room_cost'];
        $service_charge = session('room_data')[0]['service_charge'];
        $vat = session('room_data')[0]['vat'];
        $total = session('room_data')[0]['total'];
        $check_in = session('date')[0]['check-in'];
        $check_out = session('date')[0]['check-out']; //getting all the needed data from session and putting it in var
         $start =  Carbon::parse(session('date')[0]['check-in']);
            $end =  Carbon::parse(session('date')[0]['check-out']);


        $data = [
            'title' =>(empty($request->input('title'))? 'NA':$request->input('title')),
            'first_name' =>(empty($request->input('firstname'))? 'NA':$request->input('firstname')),
            'last_name' => (empty($request->input('lastname'))? 'NA':$request->input('lastname')),
            'address1' => (empty($request->input('addressline1'))? 'NA':$request->input('addressline1')),
            'address2' => (empty($request->input('addressline2'))? 'NA':$request->input('addressline2')),
            'city'=>(empty($request->input('city'))? 'NA':$request->input('city')),
            'zip'=>(empty($request->input('postalcode'))? 'NA':$request->input('postalcode')),
            'country'=>(empty($request->input('country'))? 'NA':$request->input('country')),
            'phone'=>(empty($request->input('phone'))? 'NA':$request->input('phone')),
            'email'=>(empty($request->input('email'))? 'NA':$request->input('email')),
            'org'=>(empty($request->input('org'))? 'NA':$request->input('org')),
            'payment_method'=>(empty($request->input('paymentmethod'))? 'NA':$request->input('paymentmethod')),
            'card_type'=>(empty($request->input('card_type'))? 'NA':$request->input('card_type')),
            'card_number'=>(empty($request->input('card_number'))? 'NA':$request->input('card_number')),
            'card_month'=>(empty($request->input('card_month'))? 'NA':$request->input('card_month')),
            'card_year'=>(empty($request->input('card_year'))? 'NA':$request->input('card_year')),
            'room_id' => json_encode($room_id),
            'rooms_type_number' =>json_encode($booked_room),
            'sub_total' => $sub_total,
            'service_charge' => $service_charge,
            'vat' => $vat,
            'total' => $total,
            'check_in' =>$check_in,
            'check_out' =>$check_out,
            'occupancy'=>json_encode($occupancy)
        ];


        $this->insertbookroom($brn,$check_in,$check_out);
        $this->bookedroomRepo->create($data);

        if(Auth::guest())
        {
            return redirect()->route('index');
        }else{
            return redirect()->route('admin.bookedroom')->with('flash_notice','Booked Successfully');
        }

    }

    public function insertbookroom($brn,$check_in,$check_out)
    {
        foreach ($brn as $booked_room) {
            foreach ($booked_room as $booked_room_id){
                $values = [
                    'time_from' =>  Carbon::parse($check_in) ,
                    'time_to'=> Carbon::parse($check_out),
                    'room_id'=>$booked_room_id
                ];

                DB::table('bookings')
                    ->insert($values);
            }
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\bookedroom  $bookedroom
     * @return \Illuminate\Http\Response
     */
    public function show(bookedroom $bookedroom)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bookedroom  $bookedroom
     * @return \Illuminate\Http\Response
     */
    public function edit(bookedroom $bookedroom, $id)
    {
         $title = $this->title;

        if (!$data = $this->bookedroomRepo->find($id)) {
            return view('errors.503');
        }
        return view('admin.roombook.update',compact('data', 'title'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bookedroom  $bookedroom
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {


        $data = [
            'title' =>(empty($request->input('title'))? 'NA':$request->input('title')),
            'first_name' =>(empty($request->input('firstname'))? 'NA':$request->input('firstname')),
            'last_name' => (empty($request->input('lastname'))? 'NA':$request->input('lastname')),
            'address1' => (empty($request->input('addressline1'))? 'NA':$request->input('addressline1')),
            'address2' => (empty($request->input('addressline2'))? 'NA':$request->input('addressline2')),
            'city'=>(empty($request->input('city'))? 'NA':$request->input('city')),
            'zip'=>(empty($request->input('postalcode'))? 'NA':$request->input('postalcode')),
            'country'=>(empty($request->input('country'))? 'NA':$request->input('country')),
            'phone'=>(empty($request->input('phone'))? 'NA':$request->input('phone')),
            'email'=>(empty($request->input('email'))? 'NA':$request->input('email')),
            'org'=>(empty($request->input('org'))? 'NA':$request->input('org')),
            'payment_method'=>(empty($request->input('paymentmethod'))? 'NA':$request->input('paymentmethod')),
            'card_type'=>(empty($request->input('card_type'))? 'NA':$request->input('card_type')),
            'card_number'=>(empty($request->input('card_number'))? 'NA':$request->input('card_number')),
            'card_month'=>(empty($request->input('card_month'))? 'NA':$request->input('card_month')),
            'card_year'=>(empty($request->input('card_year'))? 'NA':$request->input('card_year')),
        ];
       $this->bookedroomRepo->update($id,$data);
       return redirect()->route('admin.bookedroom')->with('flash_notice','Updated Successfully');
    }

    public function roomDate($id)
    {
        $title = $this->title;

        if (!$data = $this->bookedroomRepo->find($id)) {
            return view('errors.503');
        }
        return view('admin.roombook.changedate',compact('data','title'));
    }

    public function updateroom($id,Request $request)
    {

        $rooms = rooms::all();

        $time_from = date('Y-m-d',strtotime($request->input('check_in'))); //date format
        $time_to = date('Y-m-d',strtotime($request->input('check_out')));
        $from = Carbon::Parse($time_from);
        $to = Carbon::Parse($time_to); //change date string to carbon date for comparison

        if (!$olddata = $this->bookedroomRepo->find($id)) {
            return view('errors.503');
        } // get olddata to compare date
        $check_in = Carbon::Parse($olddata->check_in);
        $check_out = Carbon::Parse($olddata->check_out);
        if($check_in ->eq($from) && $check_out->eq($to)){
            if ($request->isMethod('POST')) {

                //Method 1 to get room available using using DB ::select
                //$data = DB::select('SELECT * FROM `room_numbers` WHERE NOT EXISTS(select room_id from `bookings` where date(`time_from`) >= ? AND date(`time_to`) <=? AND room_id = room_numbers.id)',[$time_from,$time_to]);

                //Method 2 Using Eloquent Laravel
                $data = room_number::whereDoesntHave('bookings', function ($q) use ($time_from, $time_to) {
                    $q->where(function ($q2) use ($time_from, $time_to) {
                        $q2->whereDate('time_from', '>=', $time_from)
                            ->WhereDate('time_to', '<=', $time_to);
                    });
                })->get();
            } else {
                $data = null;
            }
            $room_types = DB::table('rooms')
                ->get(); // get rooms types

            foreach($room_types as $room_type){
                $room_name = strtolower(str_replace(' ', '_', $room_type->type));
                $room_count = 1;
                $room_type_count[$room_name] = 0;
                $room_number_select[$room_name] = array();
                foreach ($data as $d){

                    $room_id = $d->room_type_id;
                    if($room_type->id == $room_id){
                        $room_type_count[$room_name] =$room_count++;
                        array_push($room_number_select[$room_name],$d);


                    }

                }
            } // getting number of room available according to the types
            $oldrooms = rooms::findMany(json_decode($olddata->room_id));


            return view('admin.roombook.changeroom')->with(compact('olddata','data','title','rooms','room_type_count','oldrooms','room_number_select','olddata'));
        } // if date is same as before.
        else{
            $date = [
                'check-in' => $request->input('check_in'),
                'check-out' => $request->input('check_out'),

            ];
            $rooms_number = json_decode($olddata->rooms_type_number,true);
            $this->deletebookings($rooms_number,$check_in,$check_out);// deleting the previous bookings
            // storing the date
             $this->storedate($date);
            if ($request->isMethod('POST')) {

                //Method 1 to get room available using using DB ::select
                //$data = DB::select('SELECT * FROM `room_numbers` WHERE NOT EXISTS(select room_id from `bookings` where date(`time_from`) >= ? AND date(`time_to`) <=? AND room_id = room_numbers.id)',[$time_from,$time_to]);

                //Method 2 Using Eloquent Laravel
                $data = room_number::whereDoesntHave('bookings', function ($q) use ($time_from, $time_to) {
                    $q->where(function ($q2) use ($time_from, $time_to) {
                        $q2->whereDate('time_from', '>=', $time_from)
                            ->WhereDate('time_to', '<=', $time_to);
                    });
                })->get();
            } else {
                $data = null;
            }
            $room_types = DB::table('rooms')
                ->get(); // get rooms types
            foreach($room_types as $room_type){
                $room_name = strtolower(str_replace(' ', '_', $room_type->type));
                $room_count = 1;
                $room_type_count[$room_name] = 0;
                $room_number_select[$room_name] = array();
                foreach ($data as $d){

                    $room_id = $d->room_type_id;
                    if($room_type->id == $room_id){
                        $room_type_count[$room_name] =$room_count++;
                        array_push($room_number_select[$room_name],$d);


                    }

                }
            } // getting number of room available according to the types
            return view('admin.roombook.datechanged')->with(compact('room_type_count','room_types','rooms','room_number_select','olddata'));
        }


    }

    public function deletebookings($rooms_number,$prevdatefrom,$prevdateto)
    {
        foreach ($rooms_number as $room_number){
            foreach ($room_number as $roomn){
                foreach ($roomn as $rn){

                    $delete = DB::table('bookings')
                        ->where('room_id','=',$rn)
                        ->whereDate('time_from','=',$prevdatefrom)
                        ->whereDate('time_to','=',$prevdateto)
                        ->delete();
                }

            }

        }
    }

    public function changeroom($id,Request $request)
    {

        if (!$olddata = $this->bookedroomRepo->find($id)) {
            return view('errors.503');
        }
        $rooms = $this->rooms($request);
        $start =  Carbon::parse($request->input('check_in'));
        $end =  Carbon::parse($request->input('check_out'));
        $time_from = date('Y-m-d',strtotime($request->input('check_in'))); //date format
        $time_to = date('Y-m-d',strtotime($request->input('check_out')));
        $days = $end->diffInDays($start) + 1 ; //calc number of days from date
        // session::push('date',$date);
        $this->cost($rooms);
        $rooms_number = json_decode($olddata->rooms_type_number,true);
        $prevdatefrom = Carbon::Parse($olddata->check_in);
        $prevdateto = Carbon::Parse($olddata->check_out);
        $this->deletebookings($rooms_number,$prevdatefrom,$prevdateto);
        $i= 0;
        foreach ($rooms as $room){
            $room_name = strtolower(str_replace(' ', '_', $room->type));
            $room_id[$i] = $room['id'];
            $booked_room[$i][$room_name] = session('data')[0][$i][$room_name.'_room'];
            $occupancy[$i][$room_name.'_adult'] = session('data')[0][$i][$room_name.'_adult'];
            $occupancy[$i][$room_name.'_child'] = session('data')[0][$i][$room_name.'_child'];
            $occupancy[$i][$room_name.'_children'] = session('data')[0][$i][$room_name.'_children'];
            $booked_room_number[$i] = array();
            array_push($booked_room_number[$i],$booked_room[$i][$room_name]);

            $i++;
            //getting all the needed data from session and putting it in var
        }
        $brn = array();

        foreach($booked_room_number as $br){
            array_push($brn,$br[0]);

        }


        $sub_total = session('room_data')[0]['room_cost'];
        $service_charge = session('room_data')[0]['service_charge'];
        $vat = session('room_data')[0]['vat'];
        $total = session('room_data')[0]['total'];
             //getting all the needed data from session and putting it in var
        $dataupdate = [
            'room_id' => json_encode($room_id),
            'rooms_type_number' =>json_encode($booked_room),
            'sub_total' => $sub_total,
            'service_charge' => $service_charge,
            'vat' => $vat,
            'total' => $total,
            'check_in' =>$time_from,
            'check_out' =>$time_to,
            'occupancy'=>json_encode($occupancy)
        ];
        $this->bookedroomRepo->update($id,$dataupdate);
        $this->insertbookroom($brn,$time_from,$time_to);
        return redirect()->route('admin.bookedroom')->with('flash_notce','Room Changed Successfully');
    }

    public function changedDate($id,Request $request)
    {

        if (!$olddata = $this->bookedroomRepo->find($id)) {
            return view('errors.503');
        } // get olddata to compare date
        $rooms =  $this->rooms($request);
        $prevdatefrom = Carbon::Parse($olddata->check_in);
        $prevdateto = Carbon::Parse($olddata->check_out);
        $start =  Carbon::parse(session('date')[0]['check-in']);
        $end =  Carbon::parse(session('date')[0]['check-out']);
        $time_from = date('Y-m-d',strtotime($request->input('check_in'))); //date format
        $time_to = date('Y-m-d',strtotime($request->input('check_out')));
        $days = $end->diffInDays($start) + 1 ; //calc number of days from date
        // session::push('date',$date);
        $rooms_number = json_decode($olddata->rooms_type_number,true);
        $this->deletebookings($rooms_number,$prevdatefrom,$prevdateto);
        $this->cost($rooms);


        $i= 0;
        foreach ($rooms as $room){
            $room_name = strtolower(str_replace(' ', '_', $room->type));
            $room_id[$i] = $room['id'];
            $booked_room[$i][$room_name] = session('data')[0][$i][$room_name.'_room'];
            $occupancy[$i][$room_name.'_adult'] = session('data')[0][$i][$room_name.'_adult'];
            $occupancy[$i][$room_name.'_child'] = session('data')[0][$i][$room_name.'_child'];
            $occupancy[$i][$room_name.'_children'] = session('data')[0][$i][$room_name.'_children'];
            $booked_room_number[$i] = array();
            array_push($booked_room_number[$i],$booked_room[$i][$room_name]);

            $i++;
            //getting all the needed data from session and putting it in var
        }
        $brn = array();

        foreach($booked_room_number as $br){
            array_push($brn,$br[0]);

        }


        $sub_total = session('room_data')[0]['room_cost'];
        $service_charge = session('room_data')[0]['service_charge'];
        $vat = session('room_data')[0]['vat'];
        $total = session('room_data')[0]['total'];
        //getting all the needed data from session and putting it in var
        $dataupdate = [
            'room_id' => json_encode($room_id),
            'rooms_type_number' =>json_encode($booked_room),
            'sub_total' => $sub_total,
            'service_charge' => $service_charge,
            'vat' => $vat,
            'total' => $total,
            'check_in' =>$time_from,
            'check_out' =>$time_to,
            'occupancy'=>json_encode($occupancy)
        ];
        $this->bookedroomRepo->update($id,$dataupdate);
        $this->insertbookroom($brn,$time_from,$time_to);
        return redirect()->route('admin.bookedroom')->with('flash_notice','Updated Successfully');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bookedroom  $bookedroom
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (!$olddata = $this->bookedroomRepo->find($id)) {
            return view('errors.503');
        }
        $prevdatefrom = Carbon::Parse($olddata->check_in);
        $prevdateto = Carbon::Parse($olddata->check_out);
        $rooms_number = json_decode($olddata->rooms_type_number,true);

        $this->deletebookings($rooms_number,$prevdatefrom,$prevdateto);
        $this->bookedroomRepo->delete($id);
        return redirect()->back()->with('flash_notice','Deleted Successfully');
    }
}
