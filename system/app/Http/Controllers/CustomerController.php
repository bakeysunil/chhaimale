<?php

namespace App\Http\Controllers;

use App\libraries\storage\CustomerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    public  $customerRepo,  $img_path;
    public $title = 'Customer';

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }

    public function index()
    {
        $title = $this->title;

        $allcustomer = $this->customerRepo->all();

        return view('admin.customer.index', compact('allcustomer', 'title'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.customer.add', compact('title'));
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'website' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'vat_no'=>'required',
            'remarks'=>'required'

        ]);
        $data=[
            'name'=>$request->input('name'),
            'address'=>$request->input('address'),
            'website'=>$request->input('website'),
            'email'=>$request->input('email'),
            'phone'=>$request->input('phone'),
            'vat_no'=>$request->input('vat_no'),
            'remarks'=>$request->input('remarks'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        $this->customerRepo->create($data);
        return redirect()->route('admin.customer');
    }

    public function edit($id)
    {
        if (!$customer = $this->customerRepo->find($id)) {
            return view('errors.503');
        }

        return view('admin.customer.update')->with(compact('customer'));
    }

    public function update(Request $request)
    {
        $id=$request->input('id');
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'website' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'vat_no'=>'required',
            'remarks'=>'required'

        ]);
        $data=[
            'name'=>$request->input('name'),
            'address'=>$request->input('address'),
            'website'=>$request->input('website'),
            'email'=>$request->input('email'),
            'phone'=>$request->input('phone'),
            'vat_no'=>$request->input('vat_no'),
            'remarks'=>$request->input('remarks'),
            'edited_by'=>Auth::user()->id
        ];

        $this->customerRepo->update($id,$data);
        return redirect()->route('admin.customer');
    }

    public function delete($id)
    {
        if (!$customer = $this->customerRepo->find($id)) {
            return view('errors.503');
        }else{
            $this->customerRepo->delete($id);
        }
        return redirect()->route('admin.customer');
    }
}
