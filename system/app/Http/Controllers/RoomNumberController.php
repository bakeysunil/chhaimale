<?php

namespace App\Http\Controllers;

use App\libraries\storage\RoomNumberRepository;
use App\model\room_number;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoomNumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $roomNumRepo;

    public function __construct(RoomNumberRepository $room_number)
    {
        $this->roomNumRepo = $room_number;
    }
    public function index()
    {
        $data = $this->roomNumRepo->all();
        $room_type = DB::table('rooms')
                    ->get();


        return view('admin.room_number.index', compact('data', 'room_type'));
    }




    public function add()
    {
        $room_types = DB::table('rooms')
                ->get();
        return view('admin.room_number.add',compact('room_types'));
    }


    public function save(Request $request)
    {
        $this->validate($request,[
          'room_name'=>'required',
          'room_type'=>'required',
        ]);
        $data = [
            'name' => $request->input('room_name'),
            'room_type_id' => $request->input('room_type'),
        ];
        $this->roomNumRepo->create($data);
        return redirect()->route('admin.rooms.number');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\model\room_number  $room_number
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $time_from = date('Y-m-d',strtotime($request->input('booked_from')));
        $time_to = date('Y-m-d',strtotime($request->input('booked_to')));

        //

        if ($request->isMethod('POST')) {

            //Method 1 to get room available using using DB ::select
            //$data = DB::select('SELECT * FROM `room_numbers` WHERE NOT EXISTS(select room_id from `bookings` where date(`time_from`) >= ? AND date(`time_to`) <=? AND room_id = room_numbers.id)',[$time_from,$time_to]);

            //Method 2 Using Eloquent Laravel
            $data = room_number::whereDoesntHave('bookings', function ($q) use ($time_from, $time_to) {
                $q->where(function ($q2) use ($time_from, $time_to) {
                    $q2->whereDate('time_from', '>=', $time_from)
                        ->WhereDate('time_to', '<=', $time_to);
                });
            })->get();
        } else {
            $data = null;
        }

//        $b = DB::table('room_numbers')
//            ->leftJoin('bookings','bookings.room_id','=','room_numbers.id')
//            ->where('bookings.time_from','>=','2018-09-05')
//            ->Where('bookings.time_to','<=','2018-09-07')
//            ->orWhereNotExists(function($query)
//            {
//                $query->select(DB::raw(1))
//                    ->from('room_numbers')
//                    ->whereRaw('bookings.room_id = room_numbers.id');
//            })
//            ->get();

        return view('admin.room_number.check')->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\model\room_number  $room_number
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$data = $this->roomNumRepo->find($id)) {
            return view('errors.503');
        }
        $room_types = DB::table('rooms')
            ->get();
        return view('admin.room_number.update')->with(compact('data','room_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\model\room_number  $room_number
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $this->validate($request,[
            'room_name'=>'required',
            'room_type'=>'required',
        ]);
        $data = [
            'name' => $request->input('room_name'),
            'room_type_id' => $request->input('room_type'),
        ];
        $this->roomNumRepo->update($id,$data);
        return redirect()->route('admin.rooms.number');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\model\room_number  $room_number
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
    }
}
