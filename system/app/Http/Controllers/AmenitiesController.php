<?php

namespace App\Http\Controllers;

use App\libraries\storage\AmenitiesRepository;
use App\model\amenity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AmenitiesController extends Controller
{
    public  $amenityRepo,  $img_path;
    public $title = 'Amenities';

    public function __construct(AmenitiesRepository $amenityRepo)
    {
        $this->amenityRepo = $amenityRepo;
        $this->img_path = 'assets/uploads/amenity/';
    }

    public function index()
    {
        $img_path = $this->img_path;
        $title = $this->title;

        $allAmenity = $this->amenityRepo->all();

        return view('admin.amenities.index', compact('allAmenity', 'title','img_path'));
    }

    public function add()
    {
        $title = $this->title;

        return view('admin.amenities.add', compact('title'));
    }

    public function save(Request $request)
    {
        $data=[
            'name'=>$request->input('amenity_name'),
            'added_by'=>Auth::user()->id,
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('amenity_image'))
        {
            $extension = $request->file('amenity_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('amenity_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }
        $this->amenityRepo->create($data);
        return redirect()->route('admin.amenities');
    }

    public function edit($id)
    {
        if (!$amenity = $this->amenityRepo->find($id)) {
            return view('errors.503');
        }
        $img_path = $this->img_path;

        return view('admin.amenities.update')->with(compact('amenity','img_path'));
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        if (!$amenity = $this->amenityRepo->find($id)) {
            return view('errors.503');
        }
        $data=[
            'name'=>$request->input('amenity_name'),
            'edited_by'=>Auth::user()->id
        ];

        if($request->hasFile('amenity_image'))
        {
            $extension = $request->file('amenity_image')->getClientOriginalExtension();
            $filename  = str_random(8).'.'.$extension;
            $request->file('amenity_image')->move($this->img_path,$filename);
            $data['image']=$filename;
        }

        $this->amenityRepo->update($id,$data);
        return redirect()->route('admin.amenities');

    }

    public function delete($id)
    {
        if (!$amenity = $this->amenityRepo->find($id)) {
            return view('errors.503');
        }
        $this->amenityRepo->delete($id);
        return redirect();
    }
}
