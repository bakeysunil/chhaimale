<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        return $this->from('info@chhaimaleresort.com')
            ->subject($request->input('contact_subject'))
       -> view('admin.contact.mail',['msg'=>$request->input('contact_message'),'sub'=>$request->input('contact_subject'),'name'=>$request->input('contact_name')])->to($request->input('contact_email'));

    }
}
