<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','IndexController@index')->name('index');
Route::get('/login',function (){
    return view('admin.login');
})->name('login');
Route::get('/conferencehall','IndexController@conferencehall')->name('conferencehall');
Route::POST('/conferencehall/booking','ConferenceController@save')->name('admin.conferencehall.booking');
Route::get('/contact','IndexController@contact')->name('contact');
Route::get('/event','IndexController@event')->name('event');
Route::get('/conference','IndexController@conference')->name('conference');
Route::get('/eventbook','IndexController@eventbook')->name('eventbook');
Route::get('/form','IndexController@form')->name('form');
Route::get('/gallery','IndexController@gallery')->name('gallery');
Route::get('/roombook','IndexController@roombook')->name('roombook');
Route::get('/roomdetail','IndexController@roomdetail')->name('roomdetail');
Route::get('/room','IndexController@room')->name('room');
Route::get('/room/details/{id}','IndexController@roomDetailView')->name('room.detail.view');
Route::get('/search','IndexController@search')->name('search');
Route::POST('/room/search','RoomController@search')->name('room_search');
Route::POST('/room/check','RoomController@check')->name('check_rooms');
Route::POST('/room/detail','RoomController@room_detail')->name('room_detail');
Route::POST('/room/book','BookedroomController@save')->name('room_book');
Route::get('/resturant','IndexController@resturant')->name('resturant');
Route::post('/conference/book','ConferenceController@save')->name('conference.save');
Route::post('/events/booking','GuesteventController@save')->name('guestadmin.save');

define('PARENT_PREFIX', 'admin');
    Route::group(array('prefix' => PARENT_PREFIX), function () {

        #Authentication
        Route::get('/', 'AuthController@showLogin')->name('admin.login');
        Route::get('login', 'AuthController@showLogin')->name('admin.login');
        Route::post('login-attempt', 'AuthController@postLogin')->name('admin.post.login');
        Route::get('logout', 'AuthController@showLogout')->name('admin.logout');

        Route::POST('/contact/save', 'ContactController@save')->name('admin.contact.save');


        Route::group(['middleware' => ['auth']], function () {

            Route::get('/dashboard','DashboardController@index')->name('dashboard');
            //routes for Events booked by Guest
            Route::get('/Guest/Event', 'GuesteventController@index')->name('admin.guestevent.index');
            Route::get('/Guest/Event/view/{id}', 'GuesteventController@view')->name('admin.guestevent.view');
            Route::get('/Guest/Event/add', 'GuesteventController@add')->name('admin.guestevent.add');
            Route::post('/Guest/Event/save', 'GuesteventController@save')->name('admin.guestevent.save');
            Route::get('/Guest/Event/edit/{id}', 'GuesteventController@edit')->name('admin.guestevent.edit');
            Route::post('/Guest/Event/update', 'GuesteventController@update')->name('admin.guestevent.update');
            Route::get('/Guest/Event/delete/{id}', 'GuesteventController@delete')->name('admin.guestevent.delete');


            //    routes for activities
            Route::get('/activities', 'ActivitiesController@index')->name('admin.activities');
            Route::get('/activities/add', 'ActivitiesController@add')->name('admin.activities.add');
            Route::post('/activities/save', 'ActivitiesController@save')->name('admin.activities.save');
            Route::get('/activities/edit/{id}', 'ActivitiesController@edit')->name('admin.activities.edit');
            Route::post('/activities/update', 'ActivitiesController@update')->name('admin.activities.update');
            Route::get('/activities/delete/{id}', 'ActivitiesController@delete')->name('admin.activities.delete');

            //routes for amenities
            Route::get('/amenities', 'AmenitiesController@index')->name('admin.amenities');
            Route::get('/amenities/add', 'AmenitiesController@add')->name('admin.amenities.add');
            Route::post('/amenities/save', 'AmenitiesController@save')->name('admin.amenities.save');
            Route::get('/amenities/edit/{id}', 'AmenitiesController@edit')->name('admin.amenities.edit');
            Route::post('/amenities/update', 'AmenitiesController@update')->name('admin.amenities.update');
            Route::get('/amenities/delete/{id}', 'AmenitiesController@delete')->name('admin.amenities.delete');

            //routes for company
            Route::get('/company', 'CompanyController@index')->name('admin.company');
            Route::get('/company/edit/{id}', 'CompanyController@edit')->name('admin.company.edit');
            Route::post('/company/update', 'CompanyController@update')->name('admin.company.update');

            //Routes for conference
            Route::get('/conference','ConferenceController@index')->name('admin.conference');
            Route::get('/conference/view/{id}','ConferenceController@view')->name('admin.conference.view');
            Route::get('/conference/add', 'ConferenceController@add')->name('admin.conference.add');
            Route::post('/conference/save', 'ConferenceController@save')->name('admin.conference.save');
            Route::get('/conference/edit/{id}', 'ConferenceController@edit')->name('admin.conference.edit');
            Route::get('/conference/edit/date/{id}', 'ConferenceController@changedate')->name('admin.conference.changedate');
            Route::POST('/conference/edit/date/changed/{id}', 'ConferenceController@datechanged')->name('admin.conference.datechanged');
            Route::post('/conference/update/{id}', 'ConferenceController@update')->name('admin.conference.update');
            Route::get('/conference/delete/{id}', 'ConferenceController@delete')->name('admin.conference.delete');

            //    routes for contact
            Route::get('/contact', 'ContactController@index')->name('admin.contact');
            Route::get('/contact/add', 'ContactController@add')->name('admin.contact.add');
            Route::get('/contact/view/{id}', 'ContactController@view')->name('admin.contact.view');
            Route::get('/contact/reply/{id}', 'ContactController@reply')->name('admin.contact.reply');
            Route::POST('/contact/send', 'ContactController@send')->name('admin.contact.send');
            Route::POST('/contact/update', 'ContactController@update')->name('admin.contact.update');
            Route::get('/contact/delete/{id}', 'ContactController@delete')->name('admin.contact.delete');

//    routes for customer
            Route::get('/admin/customer', 'CustomerController@index')->name('admin.customer');
            Route::get('/admin/customer/add', 'CustomerController@add')->name('admin.customer.add');
            Route::POST('/admin/customer/save', 'CustomerController@save')->name('admin.customer.save');
            Route::get('/admin/customer/edit/{id}', 'CustomerController@edit')->name('admin.customer.edit');
            Route::POST('/admin/customer/update', 'CustomerController@update')->name('admin.customer.update');
            Route::get('/admin/customer/delete/{id}', 'CustomerController@delete')->name('admin.customer.delete');

//    routes for event
            Route::get('/admin/event', 'EventController@index')->name('admin.event');
            Route::get('/event/view/{id}','EventController@view')->name('admin.event.view');
            Route::get('/admin/event/add', 'EventController@add')->name('admin.event.add');
            Route::POST('/admin/event/save', 'EventController@save')->name('admin.event.save');
            Route::get('/admin/event/edit/{id}', 'EventController@edit')->name('admin.event.edit');
            Route::POST('/admin/event/update', 'EventController@update')->name('admin.event.update');
            Route::get('/admin/event/delete/{id}', 'EventController@delete')->name('admin.event.delete');

//    routes for food
            Route::get('/admin/foods', 'FoodController@index')->name('admin.foods');
            Route::get('/admin/foods/add', 'FoodController@add')->name('admin.foods.add');
            Route::POST('/admin/foods/save', 'FoodController@save')->name('admin.foods.save');
            Route::get('/admin/foods/edit/{id}', 'FoodController@edit')->name('admin.foods.edit');
            Route::POST('/admin/foods/update', 'FoodController@update')->name('admin.foods.update');
            Route::get('/admin/foods/delete/{id}', 'FoodController@delete')->name('admin.foods.delete');

//    routes for gallery
            Route::get('/admin/gallery', 'GalleryController@index')->name('admin.gallery');
            Route::get('/admin/gallery/add', 'GalleryController@add')->name('admin.gallery.add');
            Route::POST('/admin/gallery/save', 'GalleryController@save')->name('admin.gallery.save');
            Route::get('/admin/gallery/edit/{id}', 'GalleryController@edit')->name('admin.gallery.edit');
            Route::POST('/admin/gallery/update', 'GalleryController@update')->name('admin.gallery.update');
            Route::get('/admin/gallery/delete/{id}', 'GalleryController@delete')->name('admin.gallery.delete');

//    routes for logs
            Route::get('/admin/logs', 'LogController@index')->name('admin.logs');
            Route::get('/admin/logs/add', 'LogController@add')->name('admin.logs.add');
            Route::POST('/admin/logs/save', 'LogController@save')->name('admin.logs.save');
            Route::get('/admin/logs/edit/{id}', 'LogController@edit')->name('admin.logs.edit');
            Route::POST('/admin/logs/update', 'LogController@update')->name('admin.logs.update');
            Route::get('/admin/logs/delete/{id}', 'LogController@delete')->name('admin.logs.delete');

//    routes for resturant
            Route::get('/admin/restaurant', 'ResturantController@index')->name('admin.restaurant');
            Route::get('/admin/restaurant/view/{id}', 'ResturantController@view')->name('admin.restaurant.view');
//            Route::get('/admin/resturant/add', 'ResturantController@add')->name('admin.resturant.add');
//            Route::get('/admin/resturant/save', 'ResturantController@save')->name('resturant.save');
            Route::get('/admin/restaurant/edit/{id}', 'ResturantController@edit')->name('admin.restaurant.edit');
            Route::POST('/admin/restaurant/update', 'ResturantController@update')->name('admin.restaurant.update');
            Route::get('/admin/restaurant/delete/{id}', 'ResturantController@delete')->name('admin.restaurant.delete');


//    routes for rooms
            Route::get('/admin/rooms', 'RoomController@index')->name('admin.rooms');
            Route::get('/admin/rooms/view/{id}', 'RoomController@view')->name('admin.rooms.view');
            Route::get('/admin/rooms/add', 'RoomController@add')->name('admin.rooms.add');
            Route::POST('/admin/rooms/save', 'RoomController@save')->name('admin.rooms.save');
            Route::get('/admin/rooms/edit/{id}', 'RoomController@edit')->name('admin.rooms.edit');
            Route::POST('/admin/rooms/update', 'RoomController@update')->name('admin.rooms.update');
            Route::get('/admin/rooms/delete/{id}', 'RoomController@delete')->name('admin.rooms.delete');

// routes for rooms number
            Route::get('/admin/rooms/number', 'RoomNumberController@index')->name('admin.rooms.number');
            Route::get('/admin/rooms/number/view/{id}', 'RoomNumberController@view')->name('admin.rooms.number.view');
            Route::POST('/admin/rooms/number/search', 'RoomNumberController@search')->name('admin.rooms.number.search');
            Route::get('/admin/rooms/number/add', 'RoomNumberController@add')->name('admin.rooms.number.add');
            Route::POST('/admin/rooms/number/save', 'RoomNumberController@save')->name('admin.rooms.number.save');
            Route::get('/admin/rooms/number/edit/{id}', 'RoomNumberController@edit')->name('admin.rooms.number.edit');
            Route::POST('/admin/rooms/number/update', 'RoomNumberController@update')->name('admin.rooms.number.update');
            Route::get('/admin/rooms/number/delete/{id}', 'RoomNumberController@delete')->name('admin.rooms.number.delete');

//    routes for setting
            Route::get('/admin/setting', 'SettingController@index')->name('admin.setting');
//            Route::get('/admin/setting/add', 'SettingController@add')->name('setting.add');
//            Route::get('/admin/setting/save', 'SettingController@save')->name('setting.save');
            Route::get('/admin/setting/edit', 'SettingController@edit')->name('admin.setting.edit');
            Route::POST('/admin/setting/update', 'SettingController@update')->name('admin.setting.update');
//            Route::get('/admin/setting/delete/{id}', 'SettingController@delete')->name('setting.delete');



//    routes for subs
            Route::get('/admin/subscription', 'SubsController@index')->name('subscription');
            Route::get('/admin/subscription/add', 'SubsController@add')->name('subscription.add');
            Route::get('/admin/subscription/save', 'SubsController@save')->name('subscription.save');
            Route::get('/admin/subscription/edit/{id}', 'SubsController@edit')->name('subscription.edit');
            Route::get('/admin/subscription/update', 'SubsController@update')->name('subscription.update');
            Route::get('/admin/subscription/delete/{id}', 'SubsController@delete')->name('subscription.delete');

//    routes for packages
            Route::get('/packages', 'PackagesController@index')->name('admin.packages');
            Route::get('/packages/add', 'PackagesController@add')->name('admin.packages.add');
            Route::POST('/packages/save', 'PackagesController@save')->name('admin.packages.save');
            Route::get('/packages/edit/{id}', 'PackagesController@edit')->name('admin.packages.edit');
            Route::POST('/packages/update/{id}', 'PackagesController@update')->name('admin.packages.update');
            Route::get('/packages/delete/{id}', 'PackagesController@delete')->name('admin.packages.delete');


// routes for slider
            Route::get('/admin/Slider', 'SliderController@index')->name('admin.slider');
            Route::get('/admin/Slider/add', 'SliderController@add')->name('admin.slider.add');
            Route::POST('/admin/Slider/save', 'SliderController@save')->name('admin.slider.save');
            Route::get('/admin/Slider/edit/{id}', 'SliderController@edit')->name('admin.slider.edit');
            Route::POST('/admin/Slider/update', 'SliderController@update')->name('admin.slider.update');
            Route::get('/admin/Slider/delete/{id}', 'SliderController@delete')->name('admin.slider.delete');


//    routes for testimonials
            Route::get('/admin/testimonials', 'TestimonialController@index')->name('admin.testimonials');
            Route::get('/testimonial/view/{id}','TestimonialController@view')->name('admin.testimonial.view');
            Route::get('/admin/testimonials/add', 'TestimonialController@add')->name('admin.testimonials.add');
            Route::POST('/admin/testimonials/save', 'TestimonialController@save')->name('admin.testimonials.save');
            Route::get('/admin/testimonials/edit/{id}', 'TestimonialController@edit')->name('admin.testimonials.edit');
            Route::POST('/admin/testimonials/update', 'TestimonialController@update')->name('admin.testimonials.update');
            Route::get('/admin/testimonials/delete/{id}', 'TestimonialController@delete')->name('admin.testimonials.delete');

//    routes for upcomingevents
            Route::get('/admin/upcomingevents', 'UpcomingEventController@index')->name('admin.upcomingevents');
            Route::get('/admin/upcomingevents/view/{id}', 'UpcomingEventController@view')->name('admin.upcomingevents.view');
            Route::get('/admin/upcomingevents/add', 'UpcomingEventController@add')->name('admin.upcomingevents.add');
            Route::POST('/admin/upcomingevents/save', 'UpcomingEventController@save')->name('admin.upcomingevents.save');
            Route::get('/admin/upcomingevents/edit/{id}', 'UpcomingEventController@edit')->name('admin.upcomingevents.edit');
            Route::POST('/admin/upcomingevents/update', 'UpcomingEventController@update')->name('admin.upcomingevents.update');
            Route::get('/admin/upcomingevents/delete/{id}', 'UpcomingEventController@delete')->name('admin.upcomingevents.delete');


            //Routes for user profile,and others
            Route::get('/admin/users', 'UserController@index')->name('admin.users');
            Route::get('/admin/user/profile/{id}', 'UserController@profile')->name('admin.user.profile');
            Route::POST('/admin/user/update', 'UserController@update')->name('admin.user.update');
            Route::POST('/admin/user/edit', 'UserController@edit')->name('admin.user.edit');
            Route::POST('/admin/user/delete/{id}', 'UserController@delete')->name('admin.user.delete');

            //ROutes for room booking through backend
            Route::get('/booked/room','BookedroomController@index')->name('admin.bookedroom');
            Route::POST('/booked/room/search','BookedroomController@search')->name('admin.bookedroom.search');
            Route::POST('/booked/room/form','BookedroomController@form')->name('admin.bookedroom.form');
            Route::get('/booked/room/view/{id}','BookedroomController@view')->name('admin.bookedroom.view');
            Route::get('/booked/room/add', 'BookedroomController@add')->name('admin.bookedroom.add');
            Route::post('/booked/room/save', 'BookedroomController@save')->name('admin.bookedroom.save');
            Route::get('/booked/room/edit/{id}', 'BookedroomController@edit')->name('admin.bookedroom.edit');
            Route::get('/booked/room/date/edit/{id}', 'BookedroomController@roomDate')->name('admin.bookedroom.date.edit');
            Route::post('/booked/room/update/{id}', 'BookedroomController@update')->name('admin.bookedroom.update');
            Route::post('/booked/room/edit/roomdetails/{id}', 'BookedroomController@updateroom')->name('admin.bookedroom.update.room');
            Route::post('/booked/room/edit/roomdetail/{id}', 'BookedroomController@changedDate')->name('admin.bookedroom.update.date');
            Route::post('/booked/room/update/roomdetails/{id}', 'BookedroomController@changeroom')->name('admin.bookedroom.changed.room');
            Route::get('/booked/room/delete/{id}', 'BookedroomController@delete')->name('admin.bookedroom.delete');

        });
    });

Route::POST('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::POST('password/reset','Auth\ResetPasswordController@reset');
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
