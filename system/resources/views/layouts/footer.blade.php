<div id="subscribe">
            <div class="container background">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text">
                                    <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <form>
                                    <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                    <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="followus text-center">
                            <div class="title">
                                <h5>Follow Us</h5>
                            </div>
                            <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<footer class="footer-content container">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="text-wrapper">
                             <div class="title">
                                City Office
                            </div>

                            <ul class="description">
                                <li><span><i class="fa fa-map-marker"></i></span>Newa: Nuga: Complex, Om Bahal, Kathmandu</li>
                                <li><span><i class="fa fa-phone"></i></span>01-4268121 // 9851181409</li>
                                <li><span><i class="fa fa-envelope-o"></i></i></span>chhaimale.resort@gmail.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                            <div class="text-wrapper">
                             <div class="title">
                               Resort Address
                            </div>

                            <ul class="description">
                                <li><span><i class="fa fa-map-marker"></i></span>Ramche Bhanjyang, Chhaimale - 13,Dakschinkali, Kathmandu</li>
                                <li><span><i class="fa fa-phone"></i></span>01-6924909 // 9860676495</li>
                                <li><span><i class="fa fa-envelope-o"></i></span>info@chhaimaleresort.com.np</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- Copy Right Content -->
        </footer>