<!DOCTYPE html>
<html lang="en">

<head>
    <title>Chhaimale Resort |The Pear Garden</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/png" href="{{asset('assets\images\chhaimale.png')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\preloader.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\animate.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Bayon' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\nav.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\tabnav.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{'assets\css\slick.css'}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\slick-theme.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets\css\hover-box.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\3dhover.css')}}">
    <link href="{{asset('assets\css\jquery.multiselect.css')}}" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="{{asset('assets\css\google-places.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets\css\main.css')}}">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets\css\style.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel='stylesheet' href='{{asset('assets\css\unite-gallery.css')}}' type='text/css' />
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css" />
    <link rel="stylesheet" href="{{asset('assets\css\smartphoto.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\jquery.fancybox.css')}}">

    <!-- Font Awesome  -->
    <link href="https://fonts.googleapis.com/css?family=Philosopher|Spirax" rel="stylesheet">
    <!-- Font Awesome Ends -->
    <style type="text/css">
        body {
            counter-reset: my-sec-counter;
        }

        .layout1 {

            background-image: url("{{asset('assets/images/conferencehall/u-shape.jpg')}}");
            border: 1px solid black;
        }

        .layout2 {
            background-image: url("{{asset('assets/images/conferencehall/conference.jpg')}}");
            border: 1px solid black;
        }

        .layout3 {
            background-image: url("{{asset('assets/images/conferencehall/hollow-square.jpg')}}");
            border: 1px solid black;
        }

        .layout4 {
            background-image: url("{{asset('assets/images/conferencehall/square.jpg')}}");
            border: 1px solid black;
        }

        .layout5 {
            background-image: url({{asset('assets/images/conferencehall/theater.jpg')}});
            border: 1px solid black;
        }

        .layout6 {
            background-image: url({{asset('assets/images/conferencehall/u-shape.jpg')}});
            border: 1px solid black;
        }

        .step-steps a::before {
            counter-increment: my-sec-counter 1;
            content: counter(my-sec-counter) ". ";
        }
    </style>
</head>
<body>
@include('layouts.header')

@yield('content')

@include('layouts.footer')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5ba0b474c666d426648adc1a/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<script type="text/javascript" src="{{asset('assets/js/baguetteBox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets\js\main.js')}}"></script>
<script type="text/javascript">
    function show1() {
        document.getElementById('card_detail').style.display = 'none';
    }

    function show2() {
        document.getElementById('card_detail').style.display = 'block';
    }
</script>
<script type="text/javascript" src="{{asset('assets\js\google-place.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\min-height.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\ug-theme-tiles.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\unitegallery.min.js')}}"></script>
<script src="{{asset('assets\js\owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\jquery.bpopup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets\js\jquery.hover3d.min.js')}}"></script>
<script src="{{asset('assets\js\wow.min.js')}}"></script>
<script src="{{asset('assets\js\jquery.fancybox.pack.js')}}"></script>
<script>
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: false, // default
        live: true // default
    })
    wow.init();

    // Gallery Javascript
    jQuery(document).ready(function(){
        jQuery("#gallery").unitegallery({
            tiles_type:"justified",
        });
    });

    // Image Preview in Room Page

    $(document).ready(function() {
        $(".fancybox").fancybox();
    });

    // Home Page Parallex Effect

      $(document).ready(function() {
        var onScroll = function() {
            var scrollTop = $(this).scrollTop();
            $('.banner-slider').each(function(index, elem) {
                var $elem = $(elem);
                $elem.find('img').css('top', scrollTop - $elem.offset().top);
            });
        };
        onScroll.apply(window);
        $(window).on('scroll', onScroll);
    });

      // Preloader
        $(document).ready(function() {
        setTimeout(function() {
            $('body').addClass('loaded');
        }, 2000);

    });

        // Popup

    $(function() {
        $('#popup_this').bPopup({
            easing: 'easeOutBack', //uses jQuery easing plugin
            speed: 650,
            transition: 'slideDown'
        });
    });


     // match height
var height={
        byRow:true,
        property:'min-height'
    }
    if($("rooms-section").length > 0){
        $("rooms-section .autoplay .room-description").matchHeight(height);
        $("rooms-section .autoplay .room-description .info h4").matchHeight(height);
    }

</script>

<script type="text/javascript">
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 4,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
         responsiveClass:true,
        responsive:{
        0:{
            items:4,
            nav:true
        },
        640:{
            items:2,
            nav:false
        },
        992 : {
        item : 2,
        nav : false,
        }
      
    }
    });
    $('.play').on('click', function() {
        owl.trigger('play.owl.autoplay', [1000])
    })
    $('.stop').on('click', function() {
        owl.trigger('stop.owl.autoplay')
    })
</script>
<script type="text/javascript">
    $('.autoplay').slick({
        slidesToShow:4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
         responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll:2,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll:2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]
    });

</script>

<script>
    window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
            t._e.push(f);
        };

        return t;
    }(document, "script", "twitter-wjs"));
</script>
<script>
    $(document).ready(function() {
        $(".activite").hover3d({
            selector: ".activite_card"
        });

        $(".movie").hover3d({
            selector: ".movie__card",
            shine: true,
            sensitivity: 20,
        });
    });
</script>
<script type="text/javascript">
    $("#check_out").datepicker({
        minDate: new Date()
    });

    $("#check_in").datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $(this).datepicker("getDate");
            $("#check_out").datepicker("option", "minDate", selectedDate);
        }
    });

    $(function() {
        $("#check_in").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });

    $(function() {
        $("#check_out").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "1");
    });
</script>
<script>
    new WOW().init();
</script>

<script type="text/javascript">
    $(".hover").mouseleave(
        function () {
            $(this).removeClass("hover");
        }
    );
</script>

<script type="text/javascript" src="http://twitter.github.io/bootstrap/assets/js/bootstrap-collapse.js"></script>
<script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js'></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyB8itk5NaiLw5hPvRJW9qCf1oquSVRx9qk &signed_in=true&libraries=places"></script>


<script>
    var eventInfo = $('#eventInfo');
    var eventInfoValidator = eventInfo.validate();

    var roomInfo = $('#roomInfo');
    var roomInfoValidator = roomInfo.validate()

    var package = $('#package');
    var packageValidator = package.validate();

    var mealTime = $('#mealTime');
    var mealTimeValidator = mealTime.validate();

    var usrInfo = $('#usrInfo');
    var usrInfoValidator = usrInfo.validate();

    $('#demo').steps({
        onChange: function(currentIndex, newIndex, stepDirection) {
            console.log('onChange', currentIndex, newIndex, stepDirection);
            // tab1
            if (currentIndex === 0) {
                if (stepDirection === 'forward') {
                    var valid = eventInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    eventInfoValidator.resetForm();
                }
            }

            // tab2
            if (currentIndex === 1) {
                if (stepDirection === 'forward') {
                    var valid = roomInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    roomInfoValidator.resetForm();
                }
            }

            // tab3
            if (currentIndex === 2) {
                if (stepDirection === 'forward') {
                    var valid = package.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    packageValidator.resetForm();
                }
            }


            if (currentIndex === 3) {
                if (stepDirection === 'forward') {
                    var valid = usrInfo.valid();
                    return valid;
                }
                if (stepDirection === 'backward') {
                    usrInfoValidator.resetForm();
                }
            }

            return true;

        },
        onFinish: function() {
            alert('Thank You for Booking');
            window.location.replace("index.html");
        }
    });

    // Tabs 3 Check Box Appers
    $(function() {
        $("#breakfast").click(function() {
            if ($(this).is(":checked")) {
                $("#breakfasttime").show();
            } else {
                $("#breakfasttime").hide();
            }
        });
    });

    $(function() {
        $("#lunch").click(function() {
            if ($(this).is(":checked")) {
                $("#lunchtime").show();
            } else {
                $("#lunchtime").hide();
            }
        });
    });

    $(function() {
        $("#dinner").click(function() {
            if ($(this).is(":checked")) {
                $("#dinnertime").show();
            } else {
                $("#dinnertime").hide();
            }
        });
    });

    $(function() {
        $("#standard_room").click(function() {
            if ($(this).is(":checked")) {
                $("#standard_occupant").show();
                $("#standard_quantity").show();
            } else {
                $("#standard_occupant").hide();
                $("#standard_quantity").hide();
            }
        });
    });


    $(function() {
        $("#wooden_room").click(function() {
            if ($(this).is(":checked")) {
                $("#wooden_quantity").show();
                $("#wooden_occupant").show();
            } else {
                $("#wooden_quantity").hide();
                $("#wooden_occupant").hide();
            }
        });
    });


    $(function() {
        $("#tented_room").click(function() {
            if ($(this).is(":checked")) {
                $("#tented_quantity").show();
                $("#tented_occupant").show();
            } else {
                $("#tented_quantity").hide();
                $("#tented_occupant").hide();
            }
        });
    });


    $(function() {
        $("#group_room").click(function() {
            if ($(this).is(":checked")) {
                $("#group_quantity").show();
                $("#group_occupant").show();
            } else {
                $("#group_quantity").hide();
                $("#group_occupant").hide();
            }
        });
    });

    function show1() {
        document.getElementById('card_detail').style.display = 'none';
    }

    function show2() {
        document.getElementById('card_detail').style.display = 'block';
    }



    $(function() {
        $("#add_rooms").click(function() {
            if ($(this).is(":checked")) {
                $("#sleeping_rooms").show();

            } else {
                $("#sleeping_rooms").hide();
            }
        });
    });



    $(function() {
        $("#add_rooms").click(function() {
            if ($(this).is(":checked")) {
                $(".rooms_content").show();
            } else {
                $(".rooms_content").hide();
            }
        });
    });



    $('#adult').on('change', function() {

        if (this.value == "other") {
            $('#adult_more').show();
        } else {

            $('#adult_more').hide();

        }
    });


    $('#child').on('change', function() {

        if (this.value == "other") {
            $('#child_more').show();
        } else {

            $('#child_more').hide();

        }
    });

    $('#children').on('change', function() {

        if (this.value == "other") {
            $('#children_more').show();
        } else {

            $('#children_more').hide();

        }
    });
</script>

<script type="text/javascript">
    $("#check-out").datepicker({
        minDate: new Date()
    });

    $("#check-in").datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $(this).datepicker("getDate");
            $("#check-out").datepicker("option", "minDate", selectedDate);
        }
    });

    $(function() {
        $("#check-in").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });

    $(function() {
        $("#check-out").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "1");
    });

//Google Review 
jQuery(document).ready(function( $ ) {
    $("#google-reviews").googlePlaces({
      placeId: 'ChIJJx66yJQ96zkRN73OEtai_90' //Find placeID @: https://developers.google.com/places/place-id
      , render: ['reviews']
      , min_rating:1
      , max_rows:50
      });
    });
$(window).on("load",function(){
  $("#map-plug").remove();
   $("#google-reviews, .customer-review").slick({
      infinite:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
       arrows: false,
    })
})

$(function() { 
        var height={
        byRow:true,
        property:'min-height'
    }
        if($("#rooms-section").length > 0){
        $("#rooms-section .room-description").matchHeight(height);
        $("#rooms-section .room-description .info h4").matchHeight(height);
    }
});


</script>

<!-- 
<script type="text/javascript">

    //if user has already selected a date while reloading the page show the previously selected date
    // we have already set date in session for future purpose lets use that
    // check if session has var date if has date change the datepicker setdate accordingly
        @if(session()->has('date'))
    {
        <?php
        $check_in = session('date')[0]['check-in'];
        $check_out = session('date')[0]['check-out'];
        ?>
        $(function() {

            $("#check-out").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate","{{$check_out}}");
        })
        $(function() {
            $("#check-in").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "{{$check_in}}");
        });
    }
    @else
    $( "#check-out" ).datepicker({
        minDate: new Date()
    });
    $(function() {
        $("#check-out").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    })
    $(function() {
        $("#check-out").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    })
    @endif
    $( "#check-in" ).datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $( this ).datepicker( "getDate" );
            console.log(selectedDate);
            $( "#check-out" ).datepicker( "option", "minDate", selectedDate );
        }
    });
</script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/js/mdb.min.js"></script>
<script>
    $( document ).ready(function() {
        $('#slider .carousel-inner .carousel-item:first-child').addClass('active');
    });
    $( document ).ready(function() {
        $('#myCarousel .carousel-inner .carousel-item:first-child').addClass('active');
    });
</script>
<script src="{{asset('assets/js/jquery.multiselect.js')}}"></script>
@yield('script')
</body>
</html>