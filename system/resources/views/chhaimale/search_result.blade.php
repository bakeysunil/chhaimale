@extends('layouts.app')
@section('content')

    <style>
        #room_book ul li{
            display: inline-block;
        }

        .note{
            margin-top: 20px;
            padding-left:10%;
            margin-bottom: 20px;

        }

        .note h2{
            font-weight:bold;
        }

        .note li{
            margin-right: 20px;
        }
        .right{
            font-size: 18px;
            float: right;
            padding: 5px;
        }

        .ms-options-wrap > .ms-options {
            max-height: auto !important;
            min-height: auto !important;
            width:20% !important;

        }
        div.product-chooser{


        }

        div.product-chooser.disabled div.product-chooser-item
        {
            zoom: 1;
            filter: alpha(opacity=60);
            opacity: 0.6;
            cursor: default;
        }

        div.product-chooser div.product-chooser-item{
            padding: 11px;
            border-radius: 6px;
            cursor: pointer;
            position: relative;
            border: 1px solid #4bc35a;
            margin-bottom: 10px;
            margin-left: 10px;
            margin-right: 10x;
        }

        div.product-chooser div.product-chooser-item.selected{
            border: 4px solid #428bca;
            background: #efefef;
            padding: 8px;
            filter: alpha(opacity=100);
            opacity: 1;
        }

        div.product-chooser div.product-chooser-item img{
            padding: 0;
        }

        div.product-chooser div.product-chooser-item span.title{
            display: block;
            margin: 10px 0 5px 0;
            font-weight: bold;
            font-size: 12px;
        }

        div.product-chooser div.product-chooser-item span.description{
            font-size: 12px;
        }

        div.product-chooser div.product-chooser-item input{
            position: absolute;
            left: 0;
            top: 0;
            visibility:hidden;
        }

    </style>

    <link rel="stylesheet" href="css/bootstrap-multiselect/bootstrap-multiselect.css" type="text/css">
    <script type="text/javascript" src="js/bootstrap-multiselect/bootstrap-multiselect.js"></script>
    <div class="content container" style="padding-top:200px;">
        <!-- Navigation Ends -->
        <div class="row">
            <div class="col-md-12">
                <div class="search_rooms">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#" >Room Booking</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('conferencehall')}}">Conference Hall Booking</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('eventbook')}}">Event Booking</a>
                        </li>
                    </ul>
                    <div class="room_picker">

                        <form action="{{route('check_rooms')}}" class="form style" method="POST">
                            {{csrf_field()}}
                            <div class="row" style="margin-bottom: 10px;">
                                <div class="col-md-2">
                                    <label for="" class="right">Check-In</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="check_in" id="check-in" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <label for="" class="right">Check-Out</label>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="check_out" id="check-out" class="form-control">
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-success" type="submit">Check Availability</button>
                                </div>
                            </div>
                        </form>
                        <form action="{{route('room_search')}}" method="POST" class="style" id="bookroom">
                            {{csrf_field()}}
                            <table class="table" style="">
                                <th>Check</th>
                                <th>Room Type</th>
                                <th>Available Rooms</th>
                                <th>Occupancy</th>
                                <th>No. of adults</th>
                                <th>No. of child(10-6)</th>
                                <th>No. of child(0-5)</th>
                                @foreach($rooms as $room)
                                    <tr>
                                        <?php
                                        $room_name = strtolower(str_replace(' ', '_', $room->type));
                                        ?>
                                        <td id=""><input id="{{$room_name}}" value="{{$room->id}}" name="room_id[]" type="checkbox"></td>
                                        <td>{{$room->type}}</td>
                                        <td class="{{$room_name.'_hidden'}}" style="visibility:hidden;">

                                            @if($room_type_count[$room_name]!=null)

                                                <select class="multi-select" multiple="multiple" id="{{$room_name.'_selected_room'}}" onchange="roomSelected(event,this.id,'{{$room_name}}')" name="{{$room_name}}[]">
                                                @foreach($room_number_select[$room_name] as $room_select)
                                                    <!--  <input type="checkbox" value="{{$room_select->id}}" name="{{$room_name}}[]"> -->
                                                        <option value="{{$room_select->id}}" >{{$room_select->name}}</option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <input type="text" value="All booked" disabled readonly class="form-control">
                                            @endif
                                        </td>
                                        <td id="{{$room_name.'_selected_occupancy'}}">{{$room->availability}}</td>
                                        <td><input class="form-control  {{$room_name.'_inputs'}}" id="{{$room_name}}_adult_input" onchange="checkTotal(event,this.id,'{{$room_name}}')"  onkeyup="checkTotal(event,this.id,'{{$room_name}}')" value="1" min="0" type="number" style=" display:none; width: 50px; height:30px;" name="{{$room_name}}_adult" ></td>
                                        <td><input class="form-control  {{$room_name.'_inputs'}}"  id="{{$room_name}}_child_input" onchange="checkTotal(event,this.id,'{{$room_name}}')"   onkeyup="checkTotal(event,this.id,'{{$room_name}}')" value="0" type="number" min="0" style="display:none; width: 50px; height:30px;" name="{{$room_name}}_child"></td>
                                        <td><input class="form-control {{$room_name.'_inputs'}}" value="0" min="0" type="number" style=" display:none; width: 50px; height:30px;" name="{{$room_name}}_children"></td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="packages">
                                <div class="package-header">
                                </div>
                                <div class="package-body">
                                    <h5>Select package you want</h5>
                                    <div class="row form-group product-chooser">
                                        @foreach($packages as $package)
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                            <div class="product-chooser-item">
                                                <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                                    <span class="title">{{$package->name}}</span>
                                                    <span class="price">Rs 2000</span>
                                                    <span class="description">{{$package->description}}</span>
                                                    <input type="radio" name="package" value="{{$package->id}}">
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                            </div>
                            <button class="btn btn-success" type="submit" style=" margin: 10px;">Continue</button>
                        </form>
                    </div>

                    <div class="note">
                        <h2>Note*</h2>
                        <ul>
                            <li type="square">50% Off for Child From 6-10.</li>
                            <li type="square">Free for Child Below 5.</li>
                        </ul>
                    </div>
                </div>
                <!--Room Picker Ends -->
            </div>
            <!-- Search Rooms Ends -->

            <!-- Col-md-6 Ends -->
        </div>
        <!-- Container Ends -->

        <!-- Content Ends -->
    </div>


    <div id="subscribe">
        <div class="container background">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text">
                                <p>Subscribe to our  breif newsletter to get exclusive discounts and new events right in your inbox</p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <form>
                                <p><input class="form-control" name="email" required="" type="email" placeholder="Enter Your Email Address" style="outline:0;"></p>
                                <p><input class="btn btn-success" type="submit" value="Subscribe"></p>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="followus text-center">
                        <div class="title">
                            <h5>Follow Us</h5>
                        </div>
                        <a href="https://www.facebook.com/chhaimaleresort/" class="btn btn-fb"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="btn btn-tweet"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="btn btn-googleplus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="btn btn-insta"><i class="fa fa-instagram"></i></a>

                    </div>

                </div>
            </div>
        </div>



        <footer class="footer-content container">
            <div class="container">
                <div class="row">

                    <div class="col-md-3">
                        <div class="footer-logo">
                            <img src="{{asset('assets\images\footer-logo.png')}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-map-marker"></i></td>
                                <td> <p> Ramche Bhanjyang-6,<br>Kathmandu</p></td>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-phone"></i></td>
                                <td><p> +977-016924909<br>+977-9851181409</p></td>

                                </tbody>
                            </table>
                        </div>


                    </div>

                    <div class="col-md-3">
                        <div class="text-wrapper">
                            <table>
                                <tbody>
                                <td> <i class="fa fa-envelope icon"></i></td>
                                <td> <p>info@chhaimaleresort.com.np<br>
                                        chhaimale.resort@gmail.com
                                    </p></td>
                                </tbody>
                            </table>
                        </div>
                    </div>





                    <div class="col-md-4">
                        <div class="copyright text-center">
                            <p>Copyright © <span class="name">Chhaimale Resort</span>  <script>document.write(new Date().getFullYear());</script> &nbsp;All rights reserved. </p>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="policy">
                            <ul>
                                <li><a href="#">Privacy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Help & Support</a></li>
                            </ul>
                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="Developedby text-center">
                            <p>Developed with <img src="{{asset('assets\images\icon\love.gif')}}" width="20px" height="20px;"> <a href="https://prabidhilabs.com/">Prabidhi Labs</a></p>
                        </div>

                    </div>
                </div>





            </div>
            <!-- Copy Right Content -->


        </footer>
        @endsection
        @section('script')
            <script>
                @foreach($rooms as $room)
                <?php
                $room_name = strtolower(str_replace(' ', '_', $room->type));
                ?>

                $('#{{$room_name}}').on('click',function () {

                    if ($('#{{$room_name}}').is(':checked')){
                        $('.{{$room_name}}').show();
                        $('.{{$room_name.'_hidden'}}').css('visibility','visible');

                        $(".{{$room_name}}").prop('required',true);

                        $("#{{$room_name.'_selected_room'}}").prop('required',true);

                    }else{
                        $('.{{$room_name}}').hide();
                        $('.{{$room_name.'_inputs'}}').hide();
                        $(".{{$room_name}}").prop('required',false);
                        $('.{{$room_name.'_hidden'}}').css('visibility','hidden');
                        $("#{{$room_name.'_selected_room'}}").prop('required',false);

                    }
                });
                $("#{{$room_name.'_selected_room'}}").on('change',function(){
                    var Selectedroom = $('#' + '{{$room_name}}' + '_selected_room :selected').length;
                    var occupancy = $('#' + '{{$room_name}}' + '_selected_occupancy').html();
                    var totalOccupancy = parseInt(occupancy) * Selectedroom;
                    if(totalOccupancy>=1){
                        $('#{{$room_name}}').show();
                    }else{

                    }

                });
                @if($room_type_count[$room_name]==0)
                $("#{{$room_name}}").css('display','none');
                $(".{{$room_name.'_select'}}").css('display','block');
                @endif
                @endforeach

                //test function
            </script>
            <script type="text/javascript">


                $(document).ready(function() {
                    $('.multi-select').multiselect();
                    $('.product-chooser .product-chooser-item:first').addClass('selected');
                    var x = $('.product-chooser-item:first').find('input[type="radio"]').prop("checked", true);
                });

                function checkTotal(event,idName,roomName){

                    var adultNum = $('#' + roomName  + '_adult_input').val();
                    var childNum = $('#' + roomName  + '_child_input').val();
                    var Selectedroom = $('#' + roomName + '_selected_room :selected').length;

                    var occupancy = $('#' + roomName + '_selected_occupancy').html();
                    var totalOccupancy = parseInt(occupancy) * Selectedroom;

                    var userTotal = parseInt(adultNum) + parseInt(childNum);
                    if(userTotal>totalOccupancy){
                        alert('Persons exceeded');
                        $('#' + idName).val(0);
                    }

                };
                function roomSelected(event,idName,roomName) {

                    $('.' + roomName + '_inputs').show();
                    $(".{{$room_name}}").prop('required',true);

                    var userroom = $('#' + roomName + '_selected_room :selected').length;
                    if(userroom<=0){
                        $('.' + roomName + '_inputs').hide();
                        $('.' + roomName).prop('required',false);
                    }

                }
            </script>
            <script>
                $(function(){
                    $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function(){
                        $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
                        $(this).addClass('selected');
                        $(this).find('input[type="radio"]').prop("checked", true);
                    });
                });

                $("#bookroom").submit(function(e) {
                    if(!$('input[type=checkbox]:checked').length) {
                        alert("Please start with Selecting Room Type");

                        //stop the form from submitting
                        return false;
                    }
                    return true;
                });
            </script>
@endsection
