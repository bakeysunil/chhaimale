@extends('layouts.app')
@section('content')
    <div class="content container" style="padding-bottom: 50px;">
        <section class="container">
            <div class="gallery-banner">
                <img src="{{asset('assets\images\banner3.jpg')}}">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <h1 class="animated zoomIn"><strong> Chhaimale Gallery</strong></h1><br>
                </div>
            </div>
        </section>


        <div class="container">

            <div id="gallery" class="animated fadeInUp";">

            @foreach($data as $d)
                <a><img src="{{asset('assets/uploads/gallery/'.$d->image)}}" alt="{{$d->title}}" data-image="{{asset('assets/uploads/gallery/'.$d->image)}}"></a>
            @endforeach
            {{--<a><img alt="" src="{{asset('assets\images\boating.jpg')}}" data-image="images/boating.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\bar.jpg')}}" data-image="images/bar.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\conference-hall.jpg')}}" data-image="images/conference-hall.jpg" ></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\drink.jpg')}}" data-image="images/drink.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\deluxe-room.jpg')}}" data-image="images/deluxe-room.jpg" ></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\family-room.jpg')}}" data-image="images/family-room.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\homestay.jpg')}}" data-image="images/homestay.jpg" ></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\banner1.jpg')}}" data-image="images/banner1.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\wedding.jpg')}}" data-image="images/wedding.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\deluxe-room.jpg')}}" data-image="images/deluxe-room.jpg" ></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\family-room.jpg')}}" data-image="images/family-room.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\thakali.jpg')}}" data-image="images/thakali.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\boating.jpg')}}" data-image="images/boating.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\bar.jpg')}}" data-image="images/bar.jpg"></a>--}}
            {{--<a><img alt="" src="{{asset('assets\images\conference-hall.jpg')}}" data-image="images/conference-hall.jpg"></a>--}}



        </div>

    </div>
 </div>



@endsection