@extends('layouts.app')
@section('content')
    <div class="content container">
        <!-- Navigation Ends -->
        <section class="container">
            <div class="contactus-banner">
                <img src="{{asset('assets\images\banner3.jpg')}}">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <h1 class="animated zoomIn" style=" animation-delay: 1s;"><strong>Contact Now</strong></h1>
                    <br>
                    <h5>We are at your Service</h5>
                </div>
            </div>
        </section>
        <!-- Contact Us -->
        <section id="contactus">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-area">
                            <div class="heading text-uppercase">
                                <h3><span class="color">Get in </span>touch</h3>
                                <hr align="left" width="10%">
                            </div>
                            <form role="form" action="{{route('admin.contact.save')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="{{ old('mobile') }}">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="{{ old('subject') }}">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" type="textarea" id="message" placeholder="Message" maxlength="140" rows="7" name="message">{{ old('message') }}</textarea>
                                </div>
                                <button type="submit" id="submit"  class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-info">
                            <div class="text-center text-uppercase">
                                <h4>Contact info</h4>
                                <hr align="center" width="10%">
                            </div>
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td><b>Address</b></td>
                                    <td>
                                        {{$data[0]->address}}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Resort Contact</b></td>
                                    <td>{{$data[0]->resort_contact}}</td>
                                </tr>
                                <tr>
                                <tr>
                                    <td><b>Office Contact</b></td>
                                    <td>{{$data[0]->office_contact}}</td>
                                </tr>
                                <td><b>Email</b></td>
                                <td>{{$data[0]->email}}
                                    <br> chhaimaleresort@gmail.com
                                </td>
                                </tr>
                                <tr>
                                    <td><b>Online</b></td>
                                    <td><a href="{{route('search')}}" style="color:#8B6B43">Booking now</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Us ends -->
        <div class="container border">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8294.982963030816!2d85.25012550007948!3d27.587183216266165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb3d94c8ba1e27%3A0xddffa2d612cebd37!2sChhaimale+Resort!5e1!3m2!1sen!2snp!4v1523293085986" width="100%" height="450" frameborder="0" style="" allowfullscreen></iframe>
        </div>
        <!-- Content Ends -->
    </div>

@endsection