@extends('layouts.app')
@section('content')
    <style>
        #room_book ul li{
            display: inline-block;
        }
     .margin{
        padding: 5px;
        margin-bottom: 20px;
        font-family: 'Roboto', sans-serif;

     }

     .note{
        margin-top: 20px;
        padding-left:10%;
        margin-bottom: 20px;

     }

     .note h2{
        font-weight:bold;
     }


     #demo .carousel-item img{
        width: 100%;
        min-height: 380px;
     }

    </style>
    <div class="content container" style="padding-top:200px;">
        <!-- Navigation Ends -->
        <div class="row">
            <div class="col-md-6">
                <div class="search_rooms">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Room Booking</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('conferencehall')}}">Conference Hall Booking</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('eventbook')}}">Event Booking</a>
                        </li>
                    </ul>
                    <div class="room_picker">

                        <form action="{{route('check_rooms')}}" class="form" method="POST">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-4 text-center margin">
                                    <label for="" style="font-size: 18px;">Check-In</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="check_in" id="check-in" class="form-control">
                                </div>
                                <div class="col-md-4 text-center margin" >
                                    <label for="" style="font-size: 18px;">Check-out</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="check_out" id="check-out" class="form-control">
                                </div>
                                
                                <div class="col-md-4">
                                     
                                </div>
                                <div class="col-md-3" style="float: right;">
                                    <button class="btn btn-success" type="submit">Check Availability</button>
                                </div>
                            </div>
                        </form>

                        <div class="note">
                                            <h2>Note*</h2>
                                            <ul>
                                                <li>50% Off for Child From 6-10.</li>
                                                <li>Free for Child Below 5.</li>
                                            </ul>
                         </div>


                    </div>
                   
                </div>
                <!--Room Picker Ends -->
            </div>
            <!-- Search Rooms Ends -->

            <!-- Col-md-6 Ends -->

            <!-- Rooms Slider-->
            <div class="col-md-6">
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- <ul class="carousel-indicators">
                  <li data-target="#demo" data-slide-to="0" class="active"></li>
                  <li data-target="#demo" data-slide-to="1"></li>
                  <li data-target="#demo" data-slide-to="2"></li>
                </ul>
     -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{asset('assets\images\banner2.jpg')}}" alt="" >
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('assets\images\homestay.jpg')}}" alt="">
                        </div>

                        <div class="carousel-item">
                            <img src="{{asset('assets\images\wooden-house.jpg')}}" alt="">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
                <!-- Rooms Slider Begains -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->

        <!-- Content Ends -->
    </div>


@endsection
