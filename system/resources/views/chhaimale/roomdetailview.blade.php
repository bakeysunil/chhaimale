@extends('layouts.app')
@section('content')

<div class="content container">
    <div class="rooms-banner" id="room-detail">
        <div class="banner-image">
            <img src="{{asset('assets/uploads/rooms/'.$room->image)}}" alt="Standard Room ">
        </div>
        <div class="text-wrapper">
            <div class="container">
                <div class="row">
                    <div class=" col-xs-6 col-md-8">
                        <div class="heading">
                        <h3>{{$room->type}}</h3>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="rate-info">
                            <div class="rate-label text-right">
                            Rs {{$room->starting_price}} /NIGHT
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Room Banner -->
	<div class="rooms-page-description">
		<div class="title">Room Description</div>
    <p>{{$room->description}}</p>

		<div class="title">Some Key Feature :</div>
		<ul class="room-feature">
                <?php
                $facility = json_decode($room->facilities);
                ?>
                @foreach($facility as $fac)
                <li>{{\App\model\rooms::findFacility($fac)}}</li>
                @endforeach                                        
		</ul>
		 <a href="{{route('search')}}" class="button">Book Now</a>
	</div>
    <!-- Content Ends -->
</div>

@endsection