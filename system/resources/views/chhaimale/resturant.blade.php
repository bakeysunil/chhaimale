@extends('layouts.app')
@section('content')

    <div class="content container">
        <!-- banner-section -->
        <section class="container">
            <div class="restuarant-banner">
                <img src="{{asset('assets\images\restaurant-banner.jpg')}}">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">

                    <h1 class="animated zoomIn"><strong>{{$data[0]->page_header}}</strong></h1><br>
                    <h5>{{$data[0]->sub_header}}</h5>
                </div>
            </div>
        </section>
        <!-- Banner- Section -->
        <section id="about-restuarent">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 restauarant">
                        <div class="heading text-center">
                            <h3>{{$data[0]->chef_title}}</h3><br>
                        </div>
                        <?php
                        $name = explode(' ',trim($data[0]->chef_name));
                        ?>
                        <div class="text-wrapper text-center">
                            <p>{{$data[0]->chef_message}}</p>
                            <p>Yours truly, {{$name[0]}}</p>
                        </div>

                        <div class="cook_image text-center">
                            <img src="{{asset('assets/uploads/restaurant/'.$data[0]->chef_photo)}}">
                            <p>
                                <strong>{{$data[0]->chef_name}}</strong><br>
                                Head Chef
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="bar" class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="wrapper">
                        <img src="{{asset('assets/uploads/restaurant/'.$data[0]->featured_image)}}" width="100%" height="600px;">

                        <div class="about-bar text-justify wow fadeInRight">
                            <h4>{{$data[0]->featured_title}}</h4><br>
                            <p>{{$data[0]->featured_message}}</p>
                        </div>

                    </div>
                </div>

            </div>
        </section>



        <section id="food-item">

            <div class="food-menu">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="heading text-center wow zoomIn">
                                <h1>Finest Food house</h1>
                            </div>
                        </div>

                        @foreach($food as $f)
                        <div class="col-md-3 padding">
                            <figure class="snip1482 wow fadeIn" style="animation-delay: 0.2s">
                                <figcaption>
                                    <h2>{{$f->name}}<br>
                                        <small>Rs {{$f->price}}</small></h2>
                                </figcaption>
                                <a href="#"></a> <img src="{{asset('assets/uploads/foods/'.$f->image)}}">
                            </figure>
                        </div>
                        @endforeach
                    </div>

                </div>

            </div>

        </section>



        <!-- last Banner -->
        <section>
            <div class="last-banner">
                <img src="{{asset('assets\images\restaurant.jpg')}}" height="500px;" width="100%">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <strong><h1>HAVE A ROOM? YOU HAVE A SEAT!</h1></strong>
                    <p>You can book your room online, and if your room supports it, automatically reserve a seat for dinner in our 5-star gourme restaurant. So do not wait, book your room now!</p>
                    <a href="{{route('search')}}" class="btn btn-outline">Book Now</a>
                </div>
            </div>
        </section>


    </div>



@endsection

