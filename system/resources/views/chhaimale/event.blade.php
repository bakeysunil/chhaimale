@extends('layouts.app')
@section('content')
    <div class="content container">
        <!-- banner-section -->
        <section class="container">
            <div class="event-banner">
                <img src="{{asset('assets\images\banner3.jpg')}}" height="400px;" width="100%">
                <div class="text-wrapper col-md-8 offset-md-2 text-center">
                    <h1 class="animated zoomIn"><strong>Event</strong></h1><br>
                    <h5>Host meetings, conferences or a celebration that inspires your guests</h5>
                </div>
            </div>
        </section>

        <div id="wrapper">

            <div class="container">
                <div class="tab-wrapper">
                    <ul class="tabs">
                        <li class="tab-link active" data-tab="1"> Our Event</li>
                        <li class="tab-link" data-tab="2">Event</li>
                        <li class="tab-link" data-tab="3">Chhaimale Weather</li>
                    </ul>
                </div>

                <div class="content-wrapper">

                    <div id="tab-1" class="tab-content active">
                        <div class="container">
                            @foreach($upcomingevents as $upcomingevent)
                            <div class="event">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="image">
                                            <img src="{{asset('assets/uploads/upcomingevent/'.$upcomingevent->image)}}">
                                        </div>

                                    </div>
                                        <?php
                                    $timestamp = strtotime($upcomingevent->date);
                                    $day = date('d',$timestamp);
                                    $month = date('m',$timestamp);
                                    $dateObj   = DateTime::createFromFormat('!m',$month);
                                    $monthName = $dateObj->format('F');
                                    ?>
                                    <div class="col-md-6">
                                        <div class="event-details">
                                            <div class="event-header">
                                                <div class="event-time">
                                                    <p class="day">{{$day}}</p>
                                                    <p class="month">{{$monthName}}</p>
                                                </div>

                                                <div class="event-info">
                                                    <h2>{{$upcomingevent->title}}</h2>


                                                </div>
                                            </div>

                                            <div class="event-detail text-justify">
                                                <p>{{$upcomingevent->description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>


                    </div>


                    <div id="tab-2" class="tab-content">
                        <div  id="event-description">
                            <div class="container">
                                <div class="row">
                                    @foreach($events as $event)
                                    <div class="col-md-12">
                                        <div class="event">
                                            <div class="row align-items-center">
                                                <div class="col-md-6">
                                                    <div class="image">
                                                        <img src="{{asset('assets/uploads/events/'.$event->image)}}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6 col-sm-12">
                                                    <div class="title text-center">
                                                        <h3>{{$event->title}}</h3>
                                                        <p>{{$event->sub_title}}</p>
                                                    </div>

                                                    <div class="details">
                                                        <?php $desc = json_decode($event->description)?>
                                                        <ul>
                                                            <?php
                                                            $description = json_decode($event->description);
                                                            ?>

                                                            @foreach($description as $desc)
                                                                <li>{{\App\model\events::findDescription($desc)}}</li>
                                                                @endforeach
                                                        </ul>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                    @endforeach

                                    <!-- Event Details Ends -->
                                </div>
                            </div>

                        </div>

                    </div>

                    <div id="tab-3" class="tab-content">
                        <a class="weatherwidget-io" href="https://forecast7.com/en/27d5985d26/chhaimale/" data-label_1="CHHAIMALE" data-label_2="WEATHER" data-font="Roboto" data-days="5" data-theme="pure" >CHHAIMALE WEATHER</a>
                        <script>
                            !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                        </script>
                    </div>


                </div>

            </div>

        </div>


        <!-- Content Ends -->
    </div>
@endsection

