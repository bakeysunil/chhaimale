@extends('layouts.app')
@section('content')
<!-- Pre Loader -->
<div id="loader-wrapper">
    <div class="loader-content">
        <div id="loader">
        </div>
        <div id="image1">
            <img src="{{asset('assets\images\logo.png')}}">
        </div>
        <div class="loader-section section-left">
        </div>
        <div class="loader-section section-right">
        </div>
    </div>
</div>
<!-- Pre Loader Ends -->
<!-- Navigation Begains -->
<div id="popup_this" style="display: none;">
    <span class="button b-close">
        <span>X</span>
    </span>
    <img src="{{asset('assets\images\ad2.jpg')}}" height="500px" alt="Chhaimale Event">
</div>
<!-- Navigation Ends -->
<!-- Banner Slider -->
<div class="content container">
    <section class="banner-slider">
        <div id="slider" class="carousel carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                @foreach($banner as $slider)
                <div class="carousel-item">
                    <img src="{{asset('assets/uploads/slider/'.$slider->image)}}" alt="{{$slider->link_text}}">
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#slider" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#slider" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
        <!-- Book Now in Banner -->
        <div class="booknow">
            <form action="{{route('check_rooms')}}" method="POST">
                {{csrf_field()}}
                <div class="col-md-10 offset-md-1">
                    <div class="Wrapper">
                        <div class="row">
                            <!-- date  -->
                            <div class="col-md-4 padding">
                                <div class="form-group" style="padding-left:30px;">
                                    <label>Check  In</label>
                                    <input type="text" id="check_in" name="check_in" class="form">
                                </div>
                            </div>
                            <div class="col-md-4 padding">
                                <div class="form-group">
                                    <label>Check Out</label>
                                    <input type="text" id="check_out" name="check_out" class="form">
                                </div>
                            </div>
                            <!-- Date Ends -->
                            <!-- check availablity button -->
                            <div class="col-md-4">
                                <button type="submit" class="btn">Check Availability</button>
                            </div>
                            <!-- Check Availablity Button Ends -->
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- About Us  -->
    <section id="aboutus-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-uppercase text-center wow zoomIn">
                        <h4>welcome to</h4>
                        <h2>Chhaimale Resort</h2>
                        <div class="outline text-center">
                            <img src="{{asset('assets\images\border.png')}}">
                        </div>
                    </div>
                    <div class="info">
                        <div class="col-md-10 offset-md-1 text-center">
                            <p>Chhaimaile Resort is located in Chhaimale, Dakshinkaali, just 22 kilometers away from Kathmandu. Located in a peaceful environmental, we deliver a natural environment with quality services, comfortable rooms, excellent and hygienic foods at relatively cheap price.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="amenities">
            <div class="row align-items-center">
                <div class="col-md-3">
                    <div class="info">
                        <h2>Amenities</h2>
                        <p> Chhaimale Resort privately owns residence with distinct features, and no two properties are exactly alike. </p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="owl-carousel">
                        @foreach($amenities as $amt)
                        <div class="amenities-cat-item">
                            <img src="{{asset('/assets/uploads/amenity/'.$amt->image)}}">
                            <h4>{{$amt->name}}</h4>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <!-- About Us Ends -->
        <!-- Rooms Description-->
        <section id="rooms-section" class="container">
            <div class="container">
                <div class="col-lg-12">
                    <div class="heading text-center">
                        <h2 class="wow zoomIn">Rooms and Suite</h2>
                        <p>With a privileged location next to the Daksinkali Temple, the recently refurbished Chhaimale Resort is the ideal resort for your leisure or work trip. Get to know our exclusive The Level service, an experience that will make your stay unforgettable.</p>
                    </div>
                </div>
                <div class="autoplay">
                    @foreach($room as $rooms)
                    <div class="room-description padding">
                        <div class="image">
                            <img src="{{asset('assets/uploads/rooms/'.$rooms->image)}}">
                        </div>
                        <div class="info">
                            <h4>{{$rooms->type}}</h4>
                            <div class="price">
                                <p>{{$rooms->last_price}} Per Night</p> 
                            </div>
                            <span class="icon">
                                <i class="fa fa-wifi" data-toggle="tooltip" title="Free WIFI!"></i>
                                <i class="fa fa-bath" data-toggle="tooltip" title="Attached Bathroom!"></i>
                                <i class="fa fa-tv" data-toggle="tooltip" title="TV "></i>
                                <i class="fa fa-tty" data-toggle="tooltip" title="Phone"></i>
                            </span>
                            <br>
                            <br>
                            <a href="{{route('search')}}" class="btn btn-outline"> Book Now</a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <!-- Container Ends -->
        </section>
        <section id="activites" class="container">
            <div class="heading text-center wow zoomIn">
                <h2>Activities</h2>
                <div class="outline text-center">
                    <img src="{{asset('assets\images\border.png')}}">
                </div>
            </div>
            <div class="site-content">
                <div class="container">
                    <div class="activite-list">
                        <div class="row">
                            @foreach($activities as $activity)
                            <div class="col-md-4 col-xs-3   ">
                                <div class="activite">
                                    <div class="activite_card wow fadeIn" style="animation-delay: 0.1s">
                                        <a href="{{asset('/assets/uploads/activity/'.$activity->image)}}" class="activite_image fancybox"><img src="{{asset('/assets/uploads/activity/'.$activity->image)}}" alt="">
                                            <div class="activite_detail">
                                                <h2 class="activite_title">{{$activity->name}}</h2>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
            <!-- Client Review -->
            <section id="testimonial" class="container">
                <div class="heading text-center wow zoomIn">
                    <h4>Testimonial</h4>
                    <h2>Customer Satisfaction</h2>
                    <div class="outline text-center" style="margin-bottom: 20px;">
                        <img src="{{asset('assets\images\border.png')}}">
                    </div>
                </div>
                <!-- Slider Starts -->
                <div class="row">
                    <!-- Google Review -->
                    <div class="col-lg-6">
                        <div id="google-reviews"></div>
                    </div>
                    <!-- Custom Website Review -->
                    <div class="col-lg-6">
                        <div class="customer-review">
                             @foreach($testimonial as $test)
                             <div class="slide-item">
                                  <p class="overview"><span>{{$test->name}}</span>{{$test->position}} at <a href="#">{{$test->company}}</a></p>
                                   <div class="star-rating">
                                        <ul class="list-inline">
                                            @for($i=0;$i<=$test->rating;$i++)
                                            <li class="list-inline-item"><i class="fa fa-star"></i></li>
                                            @endfor
                                        </ul>
                                    </div>
                                    <p class="testimonial">{{$test->comment}}</p>
                                   
                              </div>
                               @endforeach
                        </div>
                        <!-- .col-lg-6 -->
                    </div>
                </div>
            </section>
            <!-- Client Reviews Ends -->
            <!-- Book Now Bottom Fixed -->
            <div id="book_botton">
                <a href="{{route('search')}}" class="btn btn-success animated rotateInDownRight">
                    <span class="text"> B </span>
                    <span class="text"> O </span>
                    <span class="text"> O </span>
                    <span class="text"> K </span>
                    <span class="text"> N</span>
                    <span class="text"> O </span>
                    <span class="text">W </span>
                </a>
            </div>
            <!-- Book Now Bottom Ends -->
            <!-- Content Ends -->
        </div>
        @endsection