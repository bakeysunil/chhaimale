@extends('layouts.app')
@section('content')
<style>
    .clearfix{
        clear: both;
    }
</style>

    <!-- Navigation Ends -->
    <div class="content container" style="padding-top: 150px;">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-10">
                <div class="search_result">
                    <div class="heading">
                        <a href="{{route('search')}}"> Back </a>
                    </div>
                    <div class="header_content text-center">
                        <div class="content">
                            <div class="content__container">

                                <ul class="content__container__list">
                                    <li class="content__container__list__item">Free Internet Avaiable</li>
                                    <li class="content__container__list__item">Conference Hall </li>
                                    <li class="content__container__list__item">Special Chhaimale Food </li>
                                    <li class="content__container__list__item">Acoustic Friday Night</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="offer_room">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="image">
                                    <img src="{{asset('assets\images\standard-room.jpg')}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="room_description">
                                    <h4>Standard Room</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, recusandae, dolorum eveniet rerum ex quidem nam. Reprehenderit tenetur, soluta rerum nobis iusto aspernatur. Consectetur quis soluta cumque laboriosam tempora quasi.</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="price_details">
                                    <div class="card">
                                        <div class="card-header">
                                            Lowest Avaiable Rate
                                        </div>
                                        <div class="card-body text-center">
                                            <p class="card-text">Price Per Night
                                                <br> Rs 2,000
                                            </p>
                                            <a href="{{route('roomdetail')}}" class="btn">Book Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Offer Rooms Ends -->
                    <!-- Rooms Search List -->
                    <form action="{{route('room_detail')}}" method="POST" id="bookroom">
                        {{csrf_field()}}
                        <input type="text" hidden="" value="{{$date['check-in']}}" name="check-in">
                        <input type="text" hidden="" value="{{$date['check-out']}}" name="check-out">
                    <div class="rooms_rate_list">
                        <div class="rooms_search">
                            <div class="date">
                                {{$date['check-in']}} - {{$date['check-out']}}
                            </div>
                            <div class="card" style="padding: 5px 10px;">

                                <!-- Card Heading One -->
                                <div class="card-header " role="tab" id="parentheadingOne">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseOne" aria-expanded="true" aria-controls="collapseCloseOne">
                                        Standard Rate
                                    </a>

                                </div>
                                <div id="collapseCloseOne" class="card-collapse collapse" role="tabpanel" aria-labelledby="parentheadingOne">
                                    <div class="card-body ">
                                        <ul class="list-group list-group-flush">

                                            <!-- Child Heading One -->
                                            <?php $cnt = 1;
                                             $count=1;  

                                            ?>
                                             @foreach($rooms as $room)
                                             <?Php
                                              $room_name = strtolower(str_replace(' ', '_', $room->type));  

                                              ?>
                                              <li class="list-group-item" role="tab" id="childheadingOne">                                            
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#{{$room_name}}" aria-expanded="true" aria-controls="collapseCloseTwo">
                                                           {{$room->type}}
                                                        </a>
                                                        <p class="policy_text"><i class="fa fa-check"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="strike-rate">
                                                                <strike> Rs {{number_format($room->starting_price,2)}}</strike>
                                                            </div>
                                                            <div class="rate">
                                                                <p> Rs {{number_format($room->last_price,2)}}</p>
                                                            </div>

                                                            <div class="booknow" id="{{$room_name.'_standard_btn'}}">
                                                                <input  type="checkbox" name="room_id_standard[]" value="{{$room->id}}"  data-type="room<?php echo $cnt; ?>" id="roomId<?php echo $count; ?>"> 
                                                                <label for="roomId<?php echo $count;?>" class="button" >Check In</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="{{$room_name}}" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>{{$room->type}}</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <?php
                                                        $des = json_decode($room->facilities);
                                                        ?>
                                                        @foreach($des as $d)
                                                        <p>{{$d}}</p>
                                                        @endforeach
                                                    </div>

                                                </div>
                                            </li>
                                            <?php $cnt++; 
                                              $count ++;

                                            ?>
                                             @endforeach


                                            <!-- Child Heading Two Ends -->


                                        </ul>

                                        <!-- Card Headingone Ends -->
                                    </div>

                                    <!-- Card Body Ends -->
                                </div>
                                

                                <!-- Card One Ends -->

                                <!-- Card Two Begain -->

                                <div class="card-header " role="tab" id="parentheadingTwo">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseTwo" aria-expanded="true" aria-controls="collapseCloseTwo">
                                        Privileged Member Rate, Save 15% <span class="text"> Average Nightly Rate starting from 11,641NPR</span>
                                    </a>
                                </div>

                                <div id="collapseCloseTwo" class="card-collapse collapse" role="tabpanel" aria-labelledby="parentheadingTwo">
                                    <div class="card-body ">
                                         <ul class="list-group list-group-flush">
                                            <!-- Child Heading One -->
                                            <?php $cnt = 1;
                                             $count=101;

                                            ?>
                                             @foreach($rooms as $room)
                                             <?Php
                                              $room_name = strtolower(str_replace(' ', '_', $room->type));                                              
                                              ?>                                    
                                              
                                            <li class="list-group-item" role="tab" id="childheadingOne">                                            
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#{{$room_name}}" aria-expanded="true" aria-controls="collapseCloseTwo">
                                                           {{$room->type}}
                                                        </a>
                                                        <p class="policy_text"><i class="fa fa-check"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="strike-rate">
                                                                <strike> Rs {{number_format($room->starting_price,2)}}</strike>
                                                            </div>
                                                            <div class="rate">
                                                                <p> Rs {{number_format($room->last_price,2)}}</p>
                                                            </div>

                                                            <div class="booknow" id="{{$room_name.'_standard_btn'}}">
                                                               
                                                                    <input  type="checkbox" name="room_id_standard[]" value="{{$room->id}}"  data-type="room<?php echo $cnt; ?>" id="roomid<?php echo $count; ?>"> 
                                                                <label for="roomid<?php echo $count; ?>" class="button" >Check In</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="{{$room_name}}" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>{{$room->type}}</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <?php
                                                        $des = json_decode($room->facilities);
                                                        ?>
                                                        @foreach($des as $d)
                                                        <p>{{$d}}</p>
                                                        @endforeach
                                                    </div>

                                                </div>
                                            </li>
                                            <?php $cnt++; 
                                            $count ++;

                                            ?>
                                             @endforeach


                                            <!-- Child Heading Two Ends -->


                                        </ul>

                                        <!-- Card Headingone Ends -->
                                    </div>

                                    <!-- Card Body Ends -->
                                </div>

                                <!-- Card Two Ends -->


                                <!-- Card Three Begain -->

                                <div class="card-header " role="tab" id="parentheadingTwo">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#collapseCloseThree" aria-expanded="true" aria-controls="collapseCloseThree">
                                        Advance Purchase, Save 15% <span class="text"> Average Nightly Rate starting from 11,641 NPR</span>
                                    </a>
                                </div>

                                <div id="collapseCloseThree" class="card-collapse collapse" role="tabpanel" aria-labelledby="parentheadingTwo">
                                    <div class="card-body ">
                                         <ul class="list-group list-group-flush">

                                            <!-- Child Heading One -->
                                            <?php $cnt = 1;
                                             $count=201;
                                            ?>
                                             @foreach($rooms as $room)
                                             <?Php
                                              $room_name = strtolower(str_replace(' ', '_', $room->type));
                                              
                                              ?>
                                             
                                              
                                            <li class="list-group-item" role="tab" id="childheadingOne">                                            
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionGroupClosed" href="#{{$room_name}}" aria-expanded="true" aria-controls="collapseCloseTwo">
                                                           {{$room->type}}
                                                        </a>
                                                        <p class="policy_text"><i class="fa fa-check"></i> Free Cancellation</p>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="price_details">
                                                            <div class="strike-rate">
                                                                <strike> Rs {{number_format($room->starting_price,2)}}</strike>
                                                            </div>
                                                            <div class="rate">
                                                                <p> Rs {{number_format($room->last_price,2)}}</p>
                                                            </div>

                                                            <div class="booknow" id="{{$room_name.'_standard_btn'}}">
                                                               
                                                                    <input  type="checkbox" name="room_id_standard[]" value="{{$room->id}}"  data-type="room<?php echo $cnt; ?>" id="roomid<?php echo $count; ?>"> 
                                                                <label  for="roomid<?php echo $count; ?>" class="button">Check In</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="{{$room_name}}" class="child-collapse collapse" role="tabpanel" aria-labelledby="childheadingTwo">
                                                    <div class="title">
                                                        <h2>{{$room->type}}</h2>
                                                    </div>

                                                    <div class="room_details">
                                                        <h4>Rooms Description</h4>
                                                        <?php
                                                        $des = json_decode($room->facilities);
                                                        ?>
                                                        @foreach($des as $d)
                                                        <p>{{$d}}</p>
                                                        @endforeach
                                                    </div>

                                                </div>
                                            </li>
                                            <?php $cnt++;
                                                $count ++; 

                                            ?>
                                             @endforeach


                                            <!-- Child Heading Two Ends -->


                                        </ul>

                                        <!-- Card Headingone Ends -->
                                    </div>

                                    <!-- Card Body Ends -->
                                </div>
                                <!-- Card Three Ends -->

                            </div>
                            <button class="btn btn-success pull-right" type="submit" style="margin-right: 10px; margin-bottom: 10px;">Continue</button>

                            <div class="clearfix"></div>
                        </div>
                  </div>
                  
                    <!-- Rooms Search ends -->
                </div>
                <!-- Search Result Ends -->

                <div class="terms">
                    <p>* Average Nightly Rate - Additional taxes and surcharges may apply. Total estimated cost is only available in hotel currency.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora totam nihil maxime dolore reiciendis. Amet, repellat, minima. Sint ipsam ratione libero facilis illum voluptatibus aut beatae laboriosam quae dignissimos, sed?</p>
                </div>

            </div>
            <!-- Col-md-10 Ends -->
        </div>
        <!-- Row Ends -->

        <!-- Content Ends -->
    </div>
</form>

@endsection
@section('script')
<script>


    $("#bookroom").submit(function(e) {
        if(!$('input[type=checkbox]:checked').length) {
            alert("Please Select one to continue");

            //stop the form from submitting
            return false;
        }

        return true;
    });


$(document).ready(function(){
        $(document).on("change",".list-group  input[type='checkbox']",function(){
            var _target=$(this).data('type');
            console.log(_target);
            $(".list-group").removeClass('current');
            $(this).parents('.list-group').addClass("current");
            $(document).find('.list-group')
                        .not('.current')
                        .find('.booknow>input[data-type="'+_target+'"]')
                        .prop('checked',false)
                        .next('label')
                        .html('Check In');
            if($(this).parents('.list-group').length == 1){
                if($(this).is(":checked")){
                    $(this).next('label').html('Checked');
                       $(document).find('.list-group')
                        .not('.current')
                        .find('input[data-type="'+_target+'"]')
                        .attr('checked',false);                              
                }else{

                    $(this).next('label').html('Check In');
                    
            }
        }
    });
});

</script>

@endsection