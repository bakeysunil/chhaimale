@extends('layouts.app')
@section('content')
<div class="content container">
    <!-- banner-section -->
    <section class="container">
        <div class="event-banner">
            <img src="{{asset('assets\images\banner3.jpg')}}" height="400px;" width="100%">
            <div class="text-wrapper col-md-8 offset-md-2 text-center">
                <h1 class="animated zoomIn"><strong>Conference</strong></h1><br>
                <h5>Host meetings, conferences that inspires your guests</h5>
            </div>
        </div>
    </section>
    <div id="conference-detail">
        <div class="container">
            <div class="tab-wrapper">
                <ul class="tabs">
                    <li class="tab-link active" data-tab="2">Conference Hall</li>
                    <li class="tab-link" data-tab="3">Chhaimale Weather</li>
                </ul>
            </div>
            <div id="tab-2" class="tab-content active">
                <div  class="conference-description">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div id="conference-slider" class="carousel carousel-fade" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="{{asset('assets\images\conference-hall.jpg')}}" alt="" >
                                    </div>
                                    <div class="carousel-item">
                                        <img src="{{asset('assets\images\homestay.jpg')}}" alt="">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="{{asset('assets\images\conference-hall.jpg')}}" alt="">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#conference-slider" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#conference-slider" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                            </div>
                            <!-- conference slider Slider Begains -->
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="title text-center">
                                <h3>Conference Hall</h3>
                                <p>State-of-the-art facilities</p>
                            </div>
                            <div class="conference-detail text-center">
                                <ul>
                                    <li>Projector Facalities</li>
                                    <li>Free High-Speed Internet Access</li>
                                    <li>Free Bright Side Breakfast</li>
                                    <li>100 Guest Capacity</li>
                                    <li>Projector Facalities</li>
                                    <li>Free High-Speed Internet Access</li>
                                    <li>Free Bright Side Breakfast</li>
                                    <li>100 Guest Capacity</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-3" class="tab-content">
                <a class="weatherwidget-io" href="https://forecast7.com/en/27d5985d26/chhaimale/" data-label_1="CHHAIMALE" data-label_2="WEATHER" data-font="Roboto" data-days="5" data-theme="pure" >CHHAIMALE WEATHER</a>
                <script>
                !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                </script>
            </div>
        </div>
    </div>
</div>
<!-- Content Ends -->
@endsection