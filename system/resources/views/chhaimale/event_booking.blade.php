@extends('layouts.app')
@section('content')
    <style>
        #eventbooking{
    margin-top: 56px;
    height: auto;
    padding: 10px 0px;
    margin-bottom: 50px;
    box-shadow: 0px 5px 7px #8e8989;
    background-color: white;
    border: solid 5px white;
    border-bottom-color:white;
    border-left-color:white;
    border-radius: 2px;
    border-right-color:white;
    border-top-color: #ddd;
    box-sizing: border-box;
        }
        #eventbooking section{
            padding: 10px;
        }


.date .day{
    background-color: #544e4e;
    color: white;
    padding: 10px 17px;
    margin-right: 10px;
}

.title h2{
    font-size: 24px;
    color: #4d4d4d;
}

.img-section img{
    max-height: 400px;
    width: 100%;
    box-shadow:5px 5px 5px #8e8989;
}


    </style>
    <div class="content container" style="padding-top:150px; padding-bottom: 50px;">
        <div class="title"><h2 class="text-center">Event Booking</h2><hr align="center" width="5%"></div>
        <div class="col-md-12">
            <form id="eventbooking" action="{{route('guestadmin.save')}}" method="POST">
                {{csrf_field()}}
              <div>
        <h3>Event Info <br><span class="text">when and how many</span></h3>
        <section>
          <div class="row">
                            <div class="col-md-6">
                                <h5>Please Enter Your Event Information</h5>
                                <p>Tell us what Event you are preparing for, when it will be held and how many attendees will attend.</p>
                                    <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="event">Event Type<span class="asterisk">*</span></label>
                                                    <select id="sel" class="form-control" name="event_type">
                                                        <option>Picnic</option>
                                                        <option>Weeding</option>
                                                        <option>Birthday</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="attendence">Number of Attendence <span class="asterisk">*</span></label>
                                                    <input type="number" class="form-control" min="1" name="attendence" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="event_start">Event Start Date</label>
                                                    <input type="text" class="form-control" name="event_start" id="event_start">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="event_end">Event End Date</label>
                                                    <input type="text" class="form-control" name="event_end" id="event_end">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="checkbox" name="" id="eventaddrooms">i need sleeping rooms.
                                                </div>
                                            </div>
                                        </div>
                            </div>

                              <div class="col-md-6 img-section">
                                <img src="{{asset('assets\images\conference-hall.jpg')}}">
                            </div>
                        </div>
        </section>
        <h3>Sleeping Room <br><span class="text">Need Sleeping Rooms</span></h3>
        <section>
            <h5>Please Add Your Sleeping Rooms</h5>
                                <p>Tell us what sleeping rooms you need and their quantity.</p>
                                <h6>Please select the rooms you want</h6>
                                            <!--Standard Room   -->
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>Room Type</label>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="standard_room" id="standard_room" name="standard_room">Standard Room</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label> Occupant</label>
                                                        <select id="standard_occupant" style="display: none;" class="form-control">
                                                            <option value="single">Single </option>
                                                            <option value="double">Double</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label> Quantity</label>
                                                        <input type="number" name="" id="standard_quantity" style="display: none" class="form-control" min="1">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Standard Room Ends -->
                                            <!-- Wooden Room  -->
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="wooden_room" id="wooden_room" name="wooden_room">wooden Room</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select id="wooden_occupant" style="display: none;" class="form-control">
                                                            <option value="single">Single </option>
                                                            <option value="double">Double</option>
                                                            <option value="tripple">Tripple</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" name="wooden_quantity" id="wooden_quantity" style="display: none" class="form-control" min="1">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Wooden Room Ends-->
                                            <!-- Tented Room  -->
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="tented_room" id="tented_room" name="tented_room">Tented Room</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select id="tented_occupant" style="display: none;" class="form-control">
                                                            <option value="single">Single </option>
                                                            <option value="double">Double</option>
                                                            <option value="tripple">Tripple</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3" class="form-control">
                                                        <input type="number" name="tented_quantity" id="tented_quantity" style="display: none" class="form-control" min="1">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Tented Rooms Ends -->
                                            <!-- Group Family Tradition Rooms Start -->
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" value="group_room" id="group_room" name="group_room">Group  Traditional Room</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select  id="group_occupant" style="display: none;" class="form-control">
                                                            <option value="single">Single </option>
                                                            <option value="double">Double</option>
                                                            <option value="tripple">Tripple</option>
                                                            <option value="more">More</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="number" name="group_quantity" id="group_quantity" style="display: none" class="form-control" min="1">
                                                    </div>
                                                </div>
                                            </div>

                                              <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="comment">Any Suggestion</label>
                                        <textarea class="form-control" rows="4" id="comment"></textarea>
                                    </div>
                                </div>
        </section>
   <h3>Packages<br> <span class="text">Add packages you want</span></h3>
        <section>
            <h5>Please Add Your Packages</h5>
            <h6>Please select packages you want</h6><br>
            <div class="col-md-12">
                                <form id="package">
                                    <label>
                                        <input type="checkbox" class="package-list" value="Rooms Only" id="" name="package">Rooms Only
                                    </label>
                                    <label>
                                        <input name="package" type="checkbox" class="package-list" value="Rooms With Breakfast" id="tent_stay">Breakfast Included
                                    </label>
                                    <label>
                                        <input type="checkbox" name="package"   class="package-list" value="Rooms with Breakfast,Lunch" id="tent_stay">Breakfast and Lunch Included
                                    </label>
                                    <label>
                                        <input type="checkbox" class="package-list" name="package" value="Rooms with Breakfast,Lunch & Dinner" id="tent_stay">Breakfast Lunch and Dinner
                                    </label>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>No of Adult</label>
                                            <select class="form-control" id="adult" name="adult_number">
                                                <option disabled selected>Select Here</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="other">10+</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2" id="adult_more" style="display: none">
                                            <label>Enter the Number</label>
                                            <input type="number" name="adult_more" min="1" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Children(0-5)</label>
                                            <select class="form-control" id="child" name="child_number">
                                                <option disabled selected>Select Here</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="other">10+</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2" id="child_more" style="display: none">
                                            <label>Enter the Number</label>
                                            <input type="number" name="child_more" min="1" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Children(6-10)</label>
                                            <select class="form-control" id="children" name="children_number">
                                                <option disabled selected>Select Here</option>
                                                <option value="0">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="other">10+</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2" id="children_more" style="display: none">
                                            <label>Enter the Number</label>
                                            <input type="number" name="children_more" min="1" class="form-control">
                                        </div>
                                    </div>
                                    <!--Row Ends -->
                                </form>
                                <!-- Lunch Dinner and Serving Time Ends -->
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="comment">Any Suggestion</label>
                                        <textarea class="form-control" rows="4" id="comment" name="package_suggestion"></textarea>
                                    </div>
                                </div>
                            </div>
        </section>
        <h3>Send<br><span class="text">Confirm & Send</span></h3>
        <section>
          <h5>Please Enter Information</h5>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="firstname">Title<span class="asterisk">*</span></label>
                                        <select name="title" class="form-control" required="">
                                            <option value="mr">Mr</option>
                                            <option value="ms">Ms</option>
                                            <option value="mrs">Mrs</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="firstname">First Name <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="firstname" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lastname">Last Name<span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="lastname" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="addressline1">Address Line 1 <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="addressline1" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="addressline2">Address Line 2</label>
                                        <input type="text" class="form-control" name="addressline2">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="city">City <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="city" class="required">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="postalcode">Zip/Postal Code</label>
                                        <input type="text" class="form-control" name="postalcode">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <select class="form-control" name="country">
                                            <option value="NP">Nepal</option>
                                            <option value="AF">Afghanistan</option>
                                            <option value="AX">Åland Islands</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antarctica</option>
                                            <option value="AG">Antigua and Barbuda</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia, Plurinational State of</option>
                                            <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                            <option value="BA">Bosnia and Herzegovina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei Darussalam</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, the Democratic Republic of the</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Côte d'Ivoire</option>
                                            <option value="HR">Croatia</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CW">Curaçao</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Southern Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GG">Guernsey</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard Island and McDonald Islands</option>
                                            <option value="VA">Holy See (Vatican City State)</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran, Islamic Republic of</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IM">Isle of Man</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JE">Jersey</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KP">Korea, Democratic People's Republic of</option>
                                            <option value="KR">Korea, Republic of</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao People's Democratic Republic</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macao</option>
                                            <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia, Federated States of</option>
                                            <option value="MD">Moldova, Republic of</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="ME">Montenegro</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanmar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PS">Palestinian Territory, Occupied</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Réunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="BL">Saint Barthélemy</option>
                                            <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint Lucia</option>
                                            <option value="MF">Saint Martin (French part)</option>
                                            <option value="PM">Saint Pierre and Miquelon</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="RS">Serbia</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SX">Sint Maarten (Dutch part)</option>
                                            <option value="SK">Slovakia</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                                            <option value="SS">South Sudan</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syrian Arab Republic</option>
                                            <option value="TW">Taiwan, Province of China</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania, United Republic of</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TL">Timor-Leste</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="US">United States</option>
                                            <option value="UM">United States Minor Outlying Islands</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela, Bolivarian Republic of</option>
                                            <option value="VN">Viet Nam</option>
                                            <option value="VG">Virgin Islands, British</option>
                                            <option value="VI">Virgin Islands, U.S.</option>
                                            <option value="WF">Wallis and Futuna</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone">Phone Number <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="phone" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="email">Email <span class="asterisk">*</span></label>
                                        <input type="email" class="form-control" name="email" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="organizationname">Organization Name(if any)</label>
                                        <input type="text" class="form-control" name="organizationname">
                                    </div>
                                </div>
                            </div>
                            <!--Rows Ends -->
                        <div class="card">
                            <div class="card-header">
                                Guarantee this Reservation
                            </div>
                            <div class="card-body">
                                <div class="col-md-6">
                                    <input type="radio" name="paymentmethod" value="cashpayment" onclick="show1();" checked=""> Cash Payment
                                    <input type="radio" name="paymentmethod" value="credit-card" id="card" onclick="show2();" style="margin-left: 50px;" disabled=""> Credit Card
                                </div>
                                <div id="card_detail" style="display: none;">
                                    <table cellpadding="10" cellspacing="10">
                                        <tbody>
                                            <tr>
                                                <td>*</td>
                                                <td>Card Type</td>
                                                <td>
                                                    <input type="number" name="card_type" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>*</td>
                                                <td>Card Number</td>
                                                <td>
                                                    <input type="number" name="card_number" class="form-control">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>*</td>
                                                <td>Expire Date</td>
                                                <td>
                                                    <select class="expire_date" name="card_month">
                                                        <option value="">Month</option>
                                                        <option value="Jan">Jan</option>
                                                        <option value="Feb">Feb</option>
                                                        <option value="Mar">Mar</option>
                                                        <option value="Apr">Apr</option>
                                                        <option value="May">May</option>
                                                        <option value="Jun">Jun</option>
                                                        <option value="July">July</option>
                                                        <option value="Aug">Aug</option>
                                                        <option value="Sep">Sep</option>
                                                        <option value="Oct">Oct</option>
                                                        <option value="Nov">Nov</option>
                                                        <option value="Dec">Dec</option>
                                                    </select>
                                                    <select class="expire_date" name="card_year">
                                                        <option value="">Year</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2021">2021</option>
                                                        <option value="2022">2022</option>
                                                        <option value="2023">2023</option>
                                                        <option value="2024">2024</option>
                                                        <option value="2025">2025</option>
                                                        <option value="2026">2026</option>
                                                        <option value="2027">2027</option>
                                                        <option value="2028">2028</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Card-->
                        <div class="terms">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <input type="checkbox" name="">I have read and agree to the <a data-toggle="collapse" href="#terms" role="button" aria-expanded="false" aria-controls="terms">terms of sales </a>
                                        <div class="collapse" id="terms">
                                            <h3>Terms of Sale</h3> Total estimated cost for stay includes the room rate, estimated taxes, and estimated fees. Total estimated cost for stay does not include any additional applicable service charges or fees that may be charged by the hotel. Estimated taxes and estimated fees includes applicable local taxes, governmental fees, and resort fees as estimated by the hotel. Actual taxes and fees may vary. Currency conversions are estimates and are provided for comparison purposes only. Final cost for stay is charged in hotel’s local currency. Guests are required to present upon check-in valid photo identification and a credit card for any incidental charges. We reserve the right to modify or cancel a reservation if it appears, in our sole discretion, that a guest has engaged in fraudulent, illegal or other inappropriate activity or the reservation contains or results from fraud, mistake or error. Our privacy policy applies to all data collected.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <input type="checkbox" name="">I have read and agree that my personal data will be processed by the Radisson Hotel Group in accordance with the <a> privacy policy. </a>*
                                    </div>
                                </div>
                            </div>
                        </div>
        </section>
    </div>
</form>
          </div> 
</div>



@endsection
@section('script')
<script src="{{asset('assets/js/eventsteps.js')}}"></script>
<script>
   // Date Picker for conference 
    $("#event_end").datepicker({
        minDate: new Date()
    });

    $("#event_start").datepicker({
        minDate: new Date(),
        onSelect: function(dateText, inst) {
            var selectedDate = $(this).datepicker("getDate");
            $("#event_end").datepicker("option", "minDate", selectedDate);
        }
    });

    // Display today date for conference start
    $(function() {
        $("#event_start").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });


    // Display today date conference end
    $(function() {
        $("#event_end").datepicker({
            dateFormat: "yy-mm-dd"
        }).datepicker("setDate", "0");
    });

     $('#adult').on('change', function() {

        if (this.value == "other") {
            $('#adult_more').show();
        } else {

            $('#adult_more').hide();

        }
    });


    $('#child').on('change', function() {

        if (this.value == "other") {
            $('#child_more').show();
        } else {

            $('#child_more').hide();

        }
    });

    $('#children').on('change', function() {

        if (this.value == "other") {
            $('#children_more').show();
        } else {

            $('#children_more').hide();

        }
    });

       $('.package-list').on('change', function() {
       $('.package-list').not(this).prop('checked', false);  
    });
</script>
@endsection