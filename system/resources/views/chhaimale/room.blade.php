@extends('layouts.app')
@section('content')
<div class="content container">
    <div class="rooms-banner">
        <div class="banner-image">
            <img src="{{asset('assets\images\deluxe-room.jpg')}}" alt="Standard Room ">
        </div>
        <div class="text-wrapper">
            <div class="container">
                <div class="row">
                    <div class=" col-xs-6 col-md-8">
                        <div class="heading">
                            <h3>Chhaimale Resort The Pear Garden</h3>
                            <p><i class="fa fa-map-marker"></i>Ramche Bhanjyang-6,Kathmandu</p>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <div class="rate-info">
                            <div class="rate-label">
                                Rate From
                                <br>Rs 2000/NIGHT
                                <a href="#wrapper" class="btn btn-success btn-lg">See Rooms</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Room Banner -->
    <div id="wrapper">
        <div class="container">
            <div class="tab-wrapper">
                <ul class="tabs">
                    <li class="tab-link active" data-tab="1">Rooms</li>
                    <li class="tab-link" data-tab="2">Weather Forecast</li>
                </ul>
            </div>
            <div class="content-wrapper">
                <div id="tab-1" class="tab-content active">
                    <div id="room-description">
                        <!-- //////////////////////////////////////////////////////
                        /////////////Standard Room -->
                        @foreach($rooms as $room)
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <div class="room-image ">
                                    <a href="{{asset('assets/uploads/rooms/'.$room->image)}}" class="fancybox">
                                        <img src="{{asset('assets/uploads/rooms/'.$room->image)}}" width="360"/>
                                    </a>
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div class="room-detail">
                                    <div class="heading">
                                        <h3>{{$room->type}}</h3>
                                    </div>
                                    <div class="room-feature">
                                        <ul>
                                            <?php
                                            $facility = json_decode($room->facilities);
                                            ?>
                                            @foreach($facility as $fac)
                                            <li>{{\App\model\rooms::findFacility($fac)}}</li>
                                            @endforeach                                      
                                         <a href="{{route('room.detail.view',$room->id)}}" class="viewmore-btn"> View More </a>      
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="room-price">
                                    <div class="price">
                                        <strike>Rs {{$room->starting_price}}/ Per Night</strike>
                                        <h2>Rs {{$room->last_price}} / Per Night</h2>
                                    </div>
                                    <div class="booking-btn">
                                        <a href="{{route('search')}}" class="button">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    <hr>
                        @endforeach
                    </div>
                </div>
                <div id="tab-2" class="tab-content">
                    <a class="weatherwidget-io" href="https://forecast7.com/en/27d5985d26/chhaimale/" data-label_1="CHHAIMALE" data-label_2="WEATHER" data-font="Roboto" data-days="5" data-theme="pure">CHHAIMALE WEATHER</a>
                    <script>
                    ! function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s);
                    js.id = id;
                    js.src = 'https://weatherwidget.io/js/widget.min.js';
                    fjs.parentNode.insertBefore(js, fjs); } }(document, 'script', 'weatherwidget-io-js');
                    </script>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Ends -->
</div>

<script>
$(function() {
$('.pop').on('click', function() {
$('.imagepreview').attr('src', $(this).find('img').attr('src'));
$('#imagemodal').modal('show');
});
});
</script>
</div>
@endsection