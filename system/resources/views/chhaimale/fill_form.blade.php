@extends('layouts.app')
@section('content')
    <div class="container content" style="padding-top: 150px">
        <div class="row">
          
            <div class="col-md-2">
            </div> 
            <div class="col-md-10">
                  <form action="{{route('room_book')}}" method="POST">
                      {{csrf_field()}}
                <div class="rate_details">
                    <div class="heading">
                         <a href="{{route('roombook')}}"> Back </a>
                    </div>
                    <div class="header_content text-center">
                        <div class="content">
                            <div class="content__container">

                                <ul class="content__container__list">
                                    <li class="content__container__list__item">Free Internet Avaiable</li>
                                    <li class="content__container__list__item">Conference Hall </li>
                                    <li class="content__container__list__item">Special Chhaimale Food </li>
                                    <li class="content__container__list__item">Acoustic Friday Night</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="book_room">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="room_selected">
                                    <table>
                                        <tr>
                                            <td><img src="{{asset('assets\images\standard-room.jpg')}}"></td>
                                            <td style="padding-left: 20px;">Standard Room
                                                <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime illo ipsum dolor ea quos architecto, neque, itaque doloremque delectus iure explicabo, cumque voluptatum iste repudiandae suscipit obcaecati non.
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Review Booking Ends -->
                </div>
                <!-- Card Details one  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">1.</span>Enter Your Personal Information
                    </div>
                    <div class="card-body">

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="firstname">Title<span class="asterisk">*</span></label>
                                        <select name="title" class="form-control" required="">
                                            <option value="mr">Mr.</option>
                                            <option value="ms">Ms.</option>
                                            <option value="mrs">Mrs.</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="firstname">First Name <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="firstname" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="lastname">L ast Name<span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="lastname" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="addressline1">Address Line 1 <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="addressline1" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="addressline2">Address Line 2</label>
                                        <input type="text" class="form-control" name="addressline2">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="city">City <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control" name="city">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="postalcode">Zip/Postal Code</label>
                                        <input type="text" class="form-control" name="postalcode">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <select class="form-control" name="country">
                                            <option value="NP">Nepal</option>
                                            <option value="AF">Afghanistan</option>
                                            <option value="AX">Åland Islands</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antarctica</option>
                                            <option value="AG">Antigua and Barbuda</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia, Plurinational State of</option>
                                            <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                            <option value="BA">Bosnia and Herzegovina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei Darussalam</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African Republic</option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, the Democratic Republic of the</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Côte d'Ivoire</option>
                                            <option value="HR">Croatia</option>
                                            <option value="CU">Cuba</option>
                                            <option value="CW">Curaçao</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="TF">French Southern Territories</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GG">Guernsey</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HM">Heard Island and McDonald Islands</option>
                                            <option value="VA">Holy See (Vatican City State)</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran, Islamic Republic of</option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IM">Isle of Man</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JE">Jersey</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KP">Korea, Democratic People's Republic of</option>
                                            <option value="KR">Korea, Republic of</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao People's Democratic Republic</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LY">Libya</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macao</option>
                                            <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia, Federated States of</option>
                                            <option value="MD">Moldova, Republic of</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="ME">Montenegro</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="MM">Myanmar</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PS">Palestinian Territory, Occupied</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Réunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="BL">Saint Barthélemy</option>
                                            <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint Lucia</option>
                                            <option value="MF">Saint Martin (French part)</option>
                                            <option value="PM">Saint Pierre and Miquelon</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="RS">Serbia</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SX">Sint Maarten (Dutch part)</option>
                                            <option value="SK">Slovakia</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                                            <option value="SS">South Sudan</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syrian Arab Republic</option>
                                            <option value="TW">Taiwan, Province of China</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania, United Republic of</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TL">Timor-Leste</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="US">United States</option>
                                            <option value="UM">United States Minor Outlying Islands</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela, Bolivarian Republic of</option>
                                            <option value="VN">Viet Nam</option>
                                            <option value="VG">Virgin Islands, British</option>
                                            <option value="VI">Virgin Islands, U.S.</option>
                                            <option value="WF">Wallis and Futuna</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="YE">Yemen</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="phone">Phone Number <span class="asterisk">*</span></label>
                                        <input type="number" class="form-control" name="phone" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="email">Email <span class="asterisk">*</span></label>
                                        <input type="email" class="form-control" name="email" required="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="organizationname">Organization Name(if any)</label>
                                        <input type="text" class="form-control" name="org">
                                    </div>
                                </div>
                            </div>
                            <!--Rows Ends -->
                    </div>
                </div>
                <!-- Card Details One Ends -->
                <!-- Card Detials Two  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">2.</span>Guarantee this Reservation
                    </div>
                    <div class="card-body">
                        <div class="col-md-6">
                            <input type="radio" name="paymentmethod" value="cashpayment" onclick="show1();" id="cash"> Cash Payment
                            <input type="radio" name="paymentmethod" value="credit-card" id="card" onclick="show2();" style="margin-left: 50px;" > Credit Card
                        </div>
                        <div id="card_detail" style="display: none;">
                            <table cellpadding="10" cellspacing="10">
                                <tbody>
                                <tr>
                                    <td>*</td>
                                    <td>Card Type</td>
                                    <td>
                                        <input type="number" name="card_type" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>*</td>
                                    <td>Card Number</td>
                                    <td>
                                        <input type="number" name="card_number" class="form-control">
                                    </td>
                                </tr>
                                <tr>
                                    <td>*</td>
                                    <td>Expire Date</td>
                                    <td>
                                        <select class="expire_date" name="card_month">
                                            <option value="">Month</option>
                                            <option value="Jan">Jan</option>
                                            <option value="Feb">Feb</option>
                                            <option value="Mar">Mar</option>
                                            <option value="Apr">Apr</option>
                                            <option value="May">May</option>
                                            <option value="Jun">Jun</option>
                                            <option value="July">July</option>
                                            <option value="Aug">Aug</option>
                                            <option value="Sep">Sep</option>
                                            <option value="Oct">Oct</option>
                                            <option value="Nov">Nov</option>
                                            <option value="Dec">Dec</option>
                                        </select>
                                        <select class="expire_date" name="card_year">
                                            <option value="">Year</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option value="2024">2024</option>
                                            <option value="2025">2025</option>
                                            <option value="2026">2026</option>
                                            <option value="2027">2027</option>
                                            <option value="2028">2028</option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Card Details Two Ends -->
                <!-- Card Detials Three  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">3.</span>Review Reservation Details
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="room-title">
                                    <h2>Standard Room</h2>
                                </div>
                                <hr style="border: 1px solid black">
                            </div>
                            <div class="col-md-6">
                                <div class="select_room_detail">
                                    <table class="table">
                                        <?php
                                        $room_detail = session('room_data')[0]['id'];
                                        $i = 1;
                                        $j = 0;
                                        ?>
                                        @foreach($room_detail as $room)
                                                <?php $room_name = strtolower(str_replace(' ', '_', $room->type)); ?>
                                            <tr>
                                                <td>Day {{$i}}</td>
                                                <td> {!!  count(session('data')[0][$j][$room_name.'_room']) !!} Room,
                                                    Adult {!! session('data')[0][$j][$room_name.'_adult'] !!},
                                                    Child {!! session('data')[0][$j][$room_name.'_child'] !!}</td>
                                            </tr>
                                            <?php $i++;
                                            $j++;
                                            ?>
                                            @endforeach


                                    </table>
                                    <!-- Selected Rooms Details Ends -->
                                </div>
                                <!-- Col-md-6 Ends -->
                            </div>
                            <!-- COl-md-6 Begains -->
                            <div class="col-md-6">
                                <table class=" table">
                                    <thead><span class="head_title">Rooms Cost </span></thead>
                                    <tbody>
                                    <?php
                                    $per_room = session('room_data')[0]['per_room_cost'];

                                    $i = 1;
                                    ?>
                                    @foreach($per_room as $room)
                                        @foreach($room as $r)
                                            <tr>
                                                <td>Day {{$i}}</td>
                                                <td>{{$r}}</td>
                                            </tr>
                                            @endforeach
                                        <?php $i++; ?>
                                    @endforeach
                                    <br>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td>{{session('room_data')[0]['room_cost']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Service Charge</td>
                                        <td>{{session('room_data')[0]['service_charge']}}</td>
                                    </tr>
                                    <tr>
                                        <td>13% Vat Added</td>
                                        <td>{{session('room_data')[0]['vat']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Estimated Cost</td>
                                        <td>{{session('room_data')[0]['total']}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- Col-md-6 Ends -->
                            </div>
                            <!-- Row Ends -->
                        </div>
                        <!-- Card body Ends -->
                    </div>
                    <!-- Card Details Three Ends -->
                </div>

                <!-- Card Detials Four  -->
                <div class="card">
                    <div class="card-header">
                        <span class="number">4.</span>Terms and Conditions
                    </div>
                    <div class="card-body">
                            <div class="form-check" style="padding: 0px;">
                                <label class="form-check-label" for="check1">
                                   <div class="checkbox">
                                        <input type="checkbox" name="terms" required>I have read and agree to the <a data-toggle="collapse" href="#terms" role="button" aria-expanded="false" aria-controls="terms">terms of sales </a>
                                        <div class="collapse" id="terms">
                                            <h3>Terms of Sale</h3> Total estimated cost for stay includes the room rate, estimated taxes, and estimated fees. Total estimated cost for stay does not include any additional applicable service charges or fees that may be charged by the hotel. Estimated taxes and estimated fees includes applicable local taxes, governmental fees, and r
                                        </div>
                                    </div>

                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label" for="check2" required>
                                    <input type="checkbox" class="form-check-input" id="check2"  value="something">I have read and agree that my personal data will be processed by the Chhaimale Resort accordance with the privacy policy.*
                                </label>
                            </div>

                            <div class="button">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>

                        <!-- Card body Ends -->
                    </div>
                    <!-- Card Details Four Ends -->
                </div>
              </form>

            </div>

            <!-- Col-md-10 Ends -->
        </div>

        <!-- Row Ends -->

    </div>
    <!-- Content Ends -->
@endsection