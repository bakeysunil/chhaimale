@extends('layouts.app')
@section('content')
    <div class=" content container" style="padding-top: 125px;">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-10">
                <div class="rate_details">
                    <div class="heading">
                        <a href="{{route('roombook')}}"> Back </a>
                    </div>

                    <div class="header_content text-center">
                        <div class="content">
                            <div class="content__container">

                                <ul class="content__container__list">
                                    <li class="content__container__list__item">Free Internet Avaiable</li>
                                    <li class="content__container__list__item">Conference Hall </li>
                                    <li class="content__container__list__item">Special Chhaimale Food </li>
                                    <li class="content__container__list__item">Acoustic Friday Night</li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="review_booking">
                         <?php $i=0; ?>
                         @foreach($rooms as $room)
                        <div class="book_room">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="room_selected">
                                        <table>
                                           
                                            <tr>
                                                <td><img src="{{asset('assets\images\standard-room.jpg')}}"></td>
                                                <td>{{$room->type}}<br>
                                                    <p>{{$room->description}}</p>

                                            </tr>
                                            
                                        </table>


                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="date_details" style="float: right;">
                                        <table>
                                             <?Php
                                              $room_name = strtolower(str_replace(' ', '_', $room->type));
                                              ?>
                                            <tr>
                                                <td>Check-In : {{ session('date')[0]['check-in'] }}</td>

                                            </tr>
                                            <tr>
                                                <td>Check-In : {{ session('date')[0]['check-out'] }}</td>

                                            </tr>
                                            <tr>
                                                
                                                <td>
                                                   
                                             {!! count(session('data')[0][$i][$room_name.'_room']) !!} Room,
                                             Adult {!! session('data')[0][$i][$room_name.'_adult'] !!},
                                             Child {!! session('data')[0][$i][$room_name.'_child'] !!}
                                                    
                                                </td>
                                                <?php $i++ ?>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <!-- Review Booking Ends -->
                    

                        <div class="review_content">
                         

                            <div class="row">
                                <div class="col-md-7">
                                    <div class="title">
                                        <h4>Chhaimale Resort Alert</h4>
                                    </div>

                                    <div class="notice">
                                        <p>CREDIT CARD ON THE RESERVATION WILL BE PREAUTHORIZED 24 HOURS PRIOR TO ARRIVAL DAY - UNLESS POLICY STATES EARLIER. IF CREDIT CARD IS DECLINED, RESERVATION WILL BE CANCELLED</p>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <table class=" table table-striped">
                                        <thead><span class="head_title">Rooms Cost </span></thead>
                                        <tbody>
                                      
                                        <?php $i = 0;
                                            $j = 1;?>
                                        @foreach($rooms as $room)
                                            <?php $room_name = strtolower(str_replace(' ', '_', $room->type)); ?>

                                            <tr>
                                            <td>{{$room_name}}</td>
                                             <td>{{$per_room_cost[$i][$room_name]}}</td>
                                            </tr>
                                            <br>
                                            <?php $i++;
                                            $j++?>
                                            @endforeach
                                        <br>
                                        <tr>
                                            <td>Sub Total</td>
                                            <td>{{$room_cost}}</td>
                                        </tr>
                                        <tr>
                                            <td>Discount</td>
                                            <td>{{$totaldiscount}}</td>
                                        </tr>
                                        <tr>
                                            <td>Service Charge</td>
                                            <td>{{$service_charge}}</td>
                                        </tr>
                                        <tr>
                                            <td>13% Vat Added</td>
                                            <td>{{$vat}}</td>
                                        </tr>

                                        <tr style="background-color:red; color: white">
                                            <td>Total Estimated Cost</td>
                                            <td style="font-size: 14px;">{{$total}}</td>
                                        </tr>

                                        </tbody>
                                    </table>

                                    <a href="{{route('form')}}" class="btn btn-success" style="float: right;"> Continue</a>

                                </div>
                            </div>
                        </div>



                    </div>


                    <!-- Rate End Details -->
                </div>
                <!-- Col-md-10 Ends -->
            </div>
            <!-- Row Ends -->
        </div>
        <!-- Container Ends -->
        <!-- Content Ends -->
    </div>
@endsection