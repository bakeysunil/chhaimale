@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Testimonial
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Testimonial</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Testimonial
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.customer.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{{$customer->id}}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('name') ? 'has-error' : '' }}">
                                            <label>Name</label>
                                            <input class="form-control" type="text" name="name"  value="{{ $customer->name }}">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('address') ? 'has-error' : '' }}">
                                            <label>Address</label>
                                            <input class="form-control" type="text" name="address"  value="{{ $customer->address }}">
                                            @if ($errors->has('address'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('website') ? 'has-error' : '' }}">
                                            <label>Website</label>
                                            <input class="form-control" type="text" name="website"  value="{{ $customer->website }}">
                                            @if ($errors->has('website'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('email') ? 'has-error' : '' }}">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="email"  value="{{ $customer->email }}">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('phone') ? 'has-error' : '' }}">
                                            <label>Contact Number</label>
                                            <input class="form-control" type="text" name="phone"  value="{{ $customer->phone }}">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('vat_no') ? 'has-error' : '' }}">
                                            <label>Vat Number</label>
                                            <input class="form-control" type="text" name="vat_no"  value="{{ $customer->vat_no }}">
                                            @if ($errors->has('vat_no'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('vat_no') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('remarks') ? 'has-error' : '' }}">
                                            <label>Remarks</label>
                                            <textarea class="form-control" rows="5" type="text" name="remarks"  value="">{{ $customer->remarks }}
                                            </textarea>
                                            @if ($errors->has('remarks'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('remarks') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop