@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Company Details
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Company Details</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Company Details
                            <a href="{{route('admin.company.edit',$company_details[0]->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Details</a>
                        </div>
                        <div class="panel-body">
                            @foreach($company_details as $detail)
                            <div class="col-md-2">
                                    Company Name:
                            </div>
                                <div class="col-md-4">
                                    {{$detail->name}}
                                </div>
                            <div class="col-md-2">
                                Company Address:
                            </div>
                                <div class="col-md-4">
                                    {{$detail->address}}
                                </div>
                            <hr>
                            <div class="clearfix"></div>
                                <div class="col-md-2">
                                    Resort Contact:
                                </div>
                                <div class="col-md-4">
                                    {{$detail->resort_contact}}
                                </div>
                                <div class="col-md-2">
                                    Office Contact:
                                </div>
                                <div class="col-md-4">
                                    {{$detail->office_contact}}
                                </div>
                            <hr>
                            <div class="clearfix"></div>
                                <div class="col-md-2">
                                   Email Address:
                                </div>
                                <div class="col-md-4">
                                    {{$detail->email}}
                                </div>
                            <hr>
                            <div class="clearfix"></div>
                                <div class="col-md-2">
                                   Facebook Link:
                                </div>
                                <div class="col-md-10">
                                    {{$detail->facebook_link}}
                                </div>
                            <hr>
                            <div class="clearfix"></div>
                                <div class="col-md-2">
                                    Twitter Link:
                                </div>
                                <div class="col-md-10">
                                    {{$detail->twitter_link}}
                                </div>
                                <hr>
                            <fiv class="clearfix"></fiv>
                                <div class="col-md-2">
                                    Google Link:
                                </div>
                                <div class="col-md-10">
                                    {{$detail->google_link}}
                                </div>
                                <hr>
                            <div class="clearfix"></div>
                                <div class="col-md-2">
                                    Instagram Link:
                                </div>
                                <div class="col-md-10">
                                    {{$detail->instagram_link}}
                                </div>
                                <hr>
                            <div class="clearfix"></div>
                                <div class="col-md-2">
                                    Map Link:
                                </div>
                                <div class="col-md-10">
                                    {{$detail->map_link}}
                                </div>
                                <hr>
                            <div class="clearfix"></div>
                                <div class="col-md-2">
                                    Last Edited By:
                                </div>
                                <div class="col-md-10">
                                    {{$detail->getUserName($detail->edited_by)}}
                                </div>
                                @endforeach

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop