@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Company Details
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Company Details</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Update Company Details
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.company.update')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{!! $company->id !!}">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('company_name') ? 'has-error' : '' }}">
                                        <label>Company Name</label>
                                        <input class="form-control" type="text" name="company_name"  value="{{ $company->name }}">
                                        @if ($errors->has('company_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('company_address') ? 'has-error' : '' }}">
                                        <label>Company Address</label>
                                        <input class="form-control" type="text" name="company_address"  value="{{ $company->address }}">
                                        @if ($errors->has('company_address'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_address') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('resort_contact') ? 'has-error' : '' }}">
                                        <label>Resort Contact</label>
                                        <input class="form-control" type="text" name="resort_contact"  value="{{ $company->resort_contact }}">
                                        @if ($errors->has('resort_contact'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('resort_contact') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('office_contact') ? 'has-error' : '' }}">
                                        <label>Office Contact</label>
                                        <input class="form-control" type="text" name="office_contact"  value="{{ $company->office_contact }}">
                                        @if ($errors->has('office_contact'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('office_contact') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('email') ? 'has-error' : '' }}">
                                        <label>Email Address</label>
                                        <input class="form-control" type="email" name="email"  value="{{ $company->email }}">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                    <div class="form-group {{$errors->has('facebook_link') ? 'has-error' : '' }}">
                                        <label>Facebook Link</label>
                                        <input class="form-control" type="text" name="facebook_link"  value="{{ $company->facebook_link }}">
                                        @if ($errors->has('facebook_link'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('facebook_link') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                    <div class="form-group {{$errors->has('twitter_link') ? 'has-error' : '' }}">
                                        <label>Twitter Link</label>
                                        <input class="form-control" type="text" name="twitter_link"  value="{{ $company->twitter_link }}">
                                        @if ($errors->has('twitter_link'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('twitter_link') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                    <div class="form-group {{$errors->has('google_link') ? 'has-error' : '' }}">
                                        <label>Google Link</label>
                                        <input class="form-control" type="text" name="google_link"  value="{{ $company->google_link }}">
                                        @if ($errors->has('google_link'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('google_link') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                    <div class="form-group {{$errors->has('instagram_link') ? 'has-error' : '' }}">
                                        <label>Instagram Link</label>
                                        <input class="form-control" type="text" name="instagram_link"  value="{{ $company->instagram_link }}">
                                        @if ($errors->has('instagram_link'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('instagram_link') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8">
                                    <div class="form-group {{$errors->has('map_link') ? 'has-error' : '' }}">
                                        <label>Google Map Link</label>
                                        <input class="form-control" type="text" name="map_link"  value="{{ $company->map_link }}">
                                        @if ($errors->has('map_link'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('map_link') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update Company Details</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop