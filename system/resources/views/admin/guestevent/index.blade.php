@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Bookings by Guest
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Guest Event Bookings</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Event Bookings
                            <a href="{{route('admin.guestevent.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($events) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Booking By</th>
                                        <th>Phone Number</th>
                                        <th>Email Address</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Address 1</th>
                                        <th>Address 2</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @forelse($events as $event)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{!! $event->title !!} {!! $event->first_name  !!} {!! $event->last_name !!}</td>
                                            <td>{!! $event->phone !!}</td>
                                            <td>{!! $event->email !!}</td>
                                            <td>{!! $event->start_date!!}</td>
                                            <td>{!! $event->end_date!!}</td>
                                            <td>{!! $event->address1 !!}
                                            </td>
                                            <td>{!! $event->address2 !!}

                                            <td>{!! date('d F Y',strtotime($event->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="{{route('admin.guestevent.view',$event->id)}}" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.guestevent.edit',$event->id)}}" class="btn btn-info" title="View"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('admin.guestevent.delete',$event->id)}}" class="btn btn-danger" title="View"><i class="fa fa-trash"></i></a>
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Bookings yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop