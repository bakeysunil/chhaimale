@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Guest Event Booking
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Guest Event Booking</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Book a Event
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.guestevent.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h3 class="title">Conference Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('event_type') ? 'has-error' : '' }}">
                                            <label for="event">Event Type<span class="asterisk">*</span></label>
                                            <select id="sel" class="form-control" name="event_type">
                                                <option>Picnic</option>
                                                <option>Weeding</option>
                                                <option>Birthday</option>
                                            </select>
                                            @if ($errors->has('event_type'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_type') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('attendence') ? 'has-error' : '' }}">
                                            <label>Number of attendance</label>
                                            <input class="form-control" type="text" name="attendence" required   value="{{ old('attendence') }}">
                                            @if ($errors->has('attendence'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('attendence') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('event_start') ? 'has-error' : '' }}">
                                            <label>Start Date</label>
                                            <input class="form-control" type="text" id="event_start" name="event_start" required value="{{ old('conference_start') }}">
                                            @if ($errors->has('event_start'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_start') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('event_end') ? 'has-error' : '' }}">
                                            <label>End Date</label>
                                            <input class="form-control" type="text" id="event_end" name="event_end" required  value="{{ old('event_end') }}">
                                            @if ($errors->has('event_end'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_end') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <h3 class="title">Room Info</h3>
                                <hr>
                                <div class="row">
                                    {{--standard rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room') ? 'has-error' : '' }}">
                                            <label>Room type</label>
                                            <input class="form-control" type="text"   value="Standard Room" readonly>
                                            @if ($errors->has('standard_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room_occupant') ? 'has-error' : '' }}">
                                            <label>Occupant</label>
                                            <select name="standard_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('standard_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room_quantity') ? 'has-error' : '' }}">
                                            <label>Quantity</label>
                                            <input class="form-control" type="text" name="standard_room_quantity"  value="{{ old('standard_room_quantity') }}">
                                            @if ($errors->has('standard_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--//wooden Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Wooden Room" readonly>
                                            @if ($errors->has('wooden_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room_occupant') ? 'has-error' : '' }}">

                                            <select name="wooden_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('wooden_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="wooden_room_quantity"  value="{{ old('wooden_room_quantity') }}">
                                            @if ($errors->has('wooden_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--//group Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Group Room" readonly>
                                            @if ($errors->has('group_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room_occupant') ? 'has-error' : '' }}">

                                            <select name="group_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('group_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="group_room_quantity"  value="{{ old('group_room_quantity') }}">
                                            @if ($errors->has('group_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--tented Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Tented Room" readonly>
                                            @if ($errors->has('tented_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room_occupant') ? 'has-error' : '' }}">

                                            <select name="tented_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('tented_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="tented_room_quantity"  value="{{ old('tented_room_quantity') }}">
                                            @if ($errors->has('tented_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="room_suggestion">Room Suggestion</label>
                                        <div class="form-group">
                                            <textarea class="form-control" name="room_suggestion" id="" cols="30" rows="5"></textarea>
                                            @if ($errors->has('room_suggestion'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_suggestion') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--end of row--}}
                                </div>
                                <h3 class="title">Package Info</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group {{$errors->has('package') ? 'has-error' : '' }}">
                                            <label for=""> Select  Package</label>
                                            <select name="package" id="standard_occupant" class="form-control">
                                                <option value="Rooms Only">Rooms Only</option>
                                                <option value="Rooms With Breakfast">Breakfast Included</option>
                                                <option value="Rooms with Breakfast,Lunch">Breakfast and Lunch Included</option>
                                                <option value="Rooms with Breakfast,Lunch & Dinner">Breakfast Lunch and Dinner</option>

                                            </select>
                                            @if ($errors->has('package'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('package') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group {{$errors->has('adult_more') ? 'has-error' : '' }}">
                                            <label for="">No. of adult</label>
                                            <input class="form-control" type="text" name="adult_more"  value="{{ old('adult_more') }}">
                                            @if ($errors->has('adult_more'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('adult_more') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group {{$errors->has('child_more') ? 'has-error' : '' }}">
                                            <label for="">No. of Children</label>
                                            <input class="form-control" type="text" name="child_more" placeholder="(10-6years)"  value="{{ old('child_more') }}">
                                            @if ($errors->has('child_more'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('child_more') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group {{$errors->has('children_more') ? 'has-error' : '' }}">
                                            <label for="">No. of child</label>
                                            <input class="form-control" type="text" name="children_more"  placeholder="(0-5years)" value="{{ old('children_more') }}">
                                            @if ($errors->has('children_more'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('children_more') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{$errors->has('package_suggestion') ? 'has-error' : '' }}">
                                            <label for="">Package Suggestion</label>
                                            <textarea class="form-control" name="package_suggestion" id="" cols="30" rows="5"></textarea>
                                            @if ($errors->has('package_suggestion'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('package_suggestion') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <h3 class="title">Personal Info</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="firstname">Title<span class="asterisk">*</span></label>
                                            <select name="title" class="form-control" required="">
                                                <option value="mr">Mr</option>
                                                <option value="ms">Ms</option>
                                                <option value="mrs">Mrs</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="firstname">First Name <span class="asterisk">*</span></label>
                                            <input type="text" class="form-control" name="firstname" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="lastname">Last Name<span class="asterisk">*</span></label>
                                            <input type="text" class="form-control" name="lastname" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="addressline1">Address Line 1 <span class="asterisk">*</span></label>
                                            <input type="text" class="form-control" name="addressline1" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="addressline2">Address Line 2</label>
                                            <input type="text" class="form-control" name="addressline2">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="city">City <span class="asterisk">*</span></label>
                                            <input type="text" class="form-control" name="city">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="postalcode">Zip/Postal Code</label>
                                            <input type="text" class="form-control" name="postalcode">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="country">Country</label>
                                            <select class="form-control" name="country">
                                                <option value="NP">Nepal</option>
                                                <option value="AF">Afghanistan</option>
                                                <option value="AX">Åland Islands</option>
                                                <option value="AL">Albania</option>
                                                <option value="DZ">Algeria</option>
                                                <option value="AS">American Samoa</option>
                                                <option value="AD">Andorra</option>
                                                <option value="AO">Angola</option>
                                                <option value="AI">Anguilla</option>
                                                <option value="AQ">Antarctica</option>
                                                <option value="AG">Antigua and Barbuda</option>
                                                <option value="AR">Argentina</option>
                                                <option value="AM">Armenia</option>
                                                <option value="AW">Aruba</option>
                                                <option value="AU">Australia</option>
                                                <option value="AT">Austria</option>
                                                <option value="AZ">Azerbaijan</option>
                                                <option value="BS">Bahamas</option>
                                                <option value="BH">Bahrain</option>
                                                <option value="BD">Bangladesh</option>
                                                <option value="BB">Barbados</option>
                                                <option value="BY">Belarus</option>
                                                <option value="BE">Belgium</option>
                                                <option value="BZ">Belize</option>
                                                <option value="BJ">Benin</option>
                                                <option value="BM">Bermuda</option>
                                                <option value="BT">Bhutan</option>
                                                <option value="BO">Bolivia, Plurinational State of</option>
                                                <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                                <option value="BA">Bosnia and Herzegovina</option>
                                                <option value="BW">Botswana</option>
                                                <option value="BV">Bouvet Island</option>
                                                <option value="BR">Brazil</option>
                                                <option value="IO">British Indian Ocean Territory</option>
                                                <option value="BN">Brunei Darussalam</option>
                                                <option value="BG">Bulgaria</option>
                                                <option value="BF">Burkina Faso</option>
                                                <option value="BI">Burundi</option>
                                                <option value="KH">Cambodia</option>
                                                <option value="CM">Cameroon</option>
                                                <option value="CA">Canada</option>
                                                <option value="CV">Cape Verde</option>
                                                <option value="KY">Cayman Islands</option>
                                                <option value="CF">Central African Republic</option>
                                                <option value="TD">Chad</option>
                                                <option value="CL">Chile</option>
                                                <option value="CN">China</option>
                                                <option value="CX">Christmas Island</option>
                                                <option value="CC">Cocos (Keeling) Islands</option>
                                                <option value="CO">Colombia</option>
                                                <option value="KM">Comoros</option>
                                                <option value="CG">Congo</option>
                                                <option value="CD">Congo, the Democratic Republic of the</option>
                                                <option value="CK">Cook Islands</option>
                                                <option value="CR">Costa Rica</option>
                                                <option value="CI">Côte d'Ivoire</option>
                                                <option value="HR">Croatia</option>
                                                <option value="CU">Cuba</option>
                                                <option value="CW">Curaçao</option>
                                                <option value="CY">Cyprus</option>
                                                <option value="CZ">Czech Republic</option>
                                                <option value="DK">Denmark</option>
                                                <option value="DJ">Djibouti</option>
                                                <option value="DM">Dominica</option>
                                                <option value="DO">Dominican Republic</option>
                                                <option value="EC">Ecuador</option>
                                                <option value="EG">Egypt</option>
                                                <option value="SV">El Salvador</option>
                                                <option value="GQ">Equatorial Guinea</option>
                                                <option value="ER">Eritrea</option>
                                                <option value="EE">Estonia</option>
                                                <option value="ET">Ethiopia</option>
                                                <option value="FK">Falkland Islands (Malvinas)</option>
                                                <option value="FO">Faroe Islands</option>
                                                <option value="FJ">Fiji</option>
                                                <option value="FI">Finland</option>
                                                <option value="FR">France</option>
                                                <option value="GF">French Guiana</option>
                                                <option value="PF">French Polynesia</option>
                                                <option value="TF">French Southern Territories</option>
                                                <option value="GA">Gabon</option>
                                                <option value="GM">Gambia</option>
                                                <option value="GE">Georgia</option>
                                                <option value="DE">Germany</option>
                                                <option value="GH">Ghana</option>
                                                <option value="GI">Gibraltar</option>
                                                <option value="GR">Greece</option>
                                                <option value="GL">Greenland</option>
                                                <option value="GD">Grenada</option>
                                                <option value="GP">Guadeloupe</option>
                                                <option value="GU">Guam</option>
                                                <option value="GT">Guatemala</option>
                                                <option value="GG">Guernsey</option>
                                                <option value="GN">Guinea</option>
                                                <option value="GW">Guinea-Bissau</option>
                                                <option value="GY">Guyana</option>
                                                <option value="HT">Haiti</option>
                                                <option value="HM">Heard Island and McDonald Islands</option>
                                                <option value="VA">Holy See (Vatican City State)</option>
                                                <option value="HN">Honduras</option>
                                                <option value="HK">Hong Kong</option>
                                                <option value="HU">Hungary</option>
                                                <option value="IS">Iceland</option>
                                                <option value="IN">India</option>
                                                <option value="ID">Indonesia</option>
                                                <option value="IR">Iran, Islamic Republic of</option>
                                                <option value="IQ">Iraq</option>
                                                <option value="IE">Ireland</option>
                                                <option value="IM">Isle of Man</option>
                                                <option value="IL">Israel</option>
                                                <option value="IT">Italy</option>
                                                <option value="JM">Jamaica</option>
                                                <option value="JP">Japan</option>
                                                <option value="JE">Jersey</option>
                                                <option value="JO">Jordan</option>
                                                <option value="KZ">Kazakhstan</option>
                                                <option value="KE">Kenya</option>
                                                <option value="KI">Kiribati</option>
                                                <option value="KP">Korea, Democratic People's Republic of</option>
                                                <option value="KR">Korea, Republic of</option>
                                                <option value="KW">Kuwait</option>
                                                <option value="KG">Kyrgyzstan</option>
                                                <option value="LA">Lao People's Democratic Republic</option>
                                                <option value="LV">Latvia</option>
                                                <option value="LB">Lebanon</option>
                                                <option value="LS">Lesotho</option>
                                                <option value="LR">Liberia</option>
                                                <option value="LY">Libya</option>
                                                <option value="LI">Liechtenstein</option>
                                                <option value="LT">Lithuania</option>
                                                <option value="LU">Luxembourg</option>
                                                <option value="MO">Macao</option>
                                                <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                                <option value="MG">Madagascar</option>
                                                <option value="MW">Malawi</option>
                                                <option value="MY">Malaysia</option>
                                                <option value="MV">Maldives</option>
                                                <option value="ML">Mali</option>
                                                <option value="MT">Malta</option>
                                                <option value="MH">Marshall Islands</option>
                                                <option value="MQ">Martinique</option>
                                                <option value="MR">Mauritania</option>
                                                <option value="MU">Mauritius</option>
                                                <option value="YT">Mayotte</option>
                                                <option value="MX">Mexico</option>
                                                <option value="FM">Micronesia, Federated States of</option>
                                                <option value="MD">Moldova, Republic of</option>
                                                <option value="MC">Monaco</option>
                                                <option value="MN">Mongolia</option>
                                                <option value="ME">Montenegro</option>
                                                <option value="MS">Montserrat</option>
                                                <option value="MA">Morocco</option>
                                                <option value="MZ">Mozambique</option>
                                                <option value="MM">Myanmar</option>
                                                <option value="NA">Namibia</option>
                                                <option value="NR">Nauru</option>
                                                <option value="NL">Netherlands</option>
                                                <option value="NC">New Caledonia</option>
                                                <option value="NZ">New Zealand</option>
                                                <option value="NI">Nicaragua</option>
                                                <option value="NE">Niger</option>
                                                <option value="NG">Nigeria</option>
                                                <option value="NU">Niue</option>
                                                <option value="NF">Norfolk Island</option>
                                                <option value="MP">Northern Mariana Islands</option>
                                                <option value="NO">Norway</option>
                                                <option value="OM">Oman</option>
                                                <option value="PK">Pakistan</option>
                                                <option value="PW">Palau</option>
                                                <option value="PS">Palestinian Territory, Occupied</option>
                                                <option value="PA">Panama</option>
                                                <option value="PG">Papua New Guinea</option>
                                                <option value="PY">Paraguay</option>
                                                <option value="PE">Peru</option>
                                                <option value="PH">Philippines</option>
                                                <option value="PN">Pitcairn</option>
                                                <option value="PL">Poland</option>
                                                <option value="PT">Portugal</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="QA">Qatar</option>
                                                <option value="RE">Réunion</option>
                                                <option value="RO">Romania</option>
                                                <option value="RU">Russian Federation</option>
                                                <option value="RW">Rwanda</option>
                                                <option value="BL">Saint Barthélemy</option>
                                                <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                                <option value="KN">Saint Kitts and Nevis</option>
                                                <option value="LC">Saint Lucia</option>
                                                <option value="MF">Saint Martin (French part)</option>
                                                <option value="PM">Saint Pierre and Miquelon</option>
                                                <option value="VC">Saint Vincent and the Grenadines</option>
                                                <option value="WS">Samoa</option>
                                                <option value="SM">San Marino</option>
                                                <option value="ST">Sao Tome and Principe</option>
                                                <option value="SA">Saudi Arabia</option>
                                                <option value="SN">Senegal</option>
                                                <option value="RS">Serbia</option>
                                                <option value="SC">Seychelles</option>
                                                <option value="SL">Sierra Leone</option>
                                                <option value="SG">Singapore</option>
                                                <option value="SX">Sint Maarten (Dutch part)</option>
                                                <option value="SK">Slovakia</option>
                                                <option value="SI">Slovenia</option>
                                                <option value="SB">Solomon Islands</option>
                                                <option value="SO">Somalia</option>
                                                <option value="ZA">South Africa</option>
                                                <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                <option value="SS">South Sudan</option>
                                                <option value="ES">Spain</option>
                                                <option value="LK">Sri Lanka</option>
                                                <option value="SD">Sudan</option>
                                                <option value="SR">Suriname</option>
                                                <option value="SJ">Svalbard and Jan Mayen</option>
                                                <option value="SZ">Swaziland</option>
                                                <option value="SE">Sweden</option>
                                                <option value="CH">Switzerland</option>
                                                <option value="SY">Syrian Arab Republic</option>
                                                <option value="TW">Taiwan, Province of China</option>
                                                <option value="TJ">Tajikistan</option>
                                                <option value="TZ">Tanzania, United Republic of</option>
                                                <option value="TH">Thailand</option>
                                                <option value="TL">Timor-Leste</option>
                                                <option value="TG">Togo</option>
                                                <option value="TK">Tokelau</option>
                                                <option value="TO">Tonga</option>
                                                <option value="TT">Trinidad and Tobago</option>
                                                <option value="TN">Tunisia</option>
                                                <option value="TR">Turkey</option>
                                                <option value="TM">Turkmenistan</option>
                                                <option value="TC">Turks and Caicos Islands</option>
                                                <option value="TV">Tuvalu</option>
                                                <option value="UG">Uganda</option>
                                                <option value="UA">Ukraine</option>
                                                <option value="AE">United Arab Emirates</option>
                                                <option value="GB">United Kingdom</option>
                                                <option value="US">United States</option>
                                                <option value="UM">United States Minor Outlying Islands</option>
                                                <option value="UY">Uruguay</option>
                                                <option value="UZ">Uzbekistan</option>
                                                <option value="VU">Vanuatu</option>
                                                <option value="VE">Venezuela, Bolivarian Republic of</option>
                                                <option value="VN">Viet Nam</option>
                                                <option value="VG">Virgin Islands, British</option>
                                                <option value="VI">Virgin Islands, U.S.</option>
                                                <option value="WF">Wallis and Futuna</option>
                                                <option value="EH">Western Sahara</option>
                                                <option value="YE">Yemen</option>
                                                <option value="ZM">Zambia</option>
                                                <option value="ZW">Zimbabwe</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="phone">Phone Number <span class="asterisk">*</span></label>
                                            <input type="number" class="form-control" name="phone" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="email">Email <span class="asterisk">*</span></label>
                                            <input type="email" class="form-control" name="email" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="organizationname">Organization Name(if any)</label>
                                            <input type="text" class="form-control" name="organizationname">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('paymentmethod') ? 'has-error' : '' }}">
                                            <label for="">Payment Method</label>
                                            <select name="paymentmethod" id="" class="form-control">
                                                <option value="cashpayment">Cash</option>
                                                <option value="credit-card">Credit Card</option>
                                            </select>
                                            @if ($errors->has('paymentmethod'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('paymentmethod') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_type') ? 'has-error' : '' }}">
                                            <label for="">Card type</label>
                                            <input class="form-control" type="text" name="card_type"  value="{{ old('card_type') }}">
                                            @if ($errors->has('card_type'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_type') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_number') ? 'has-error' : '' }}">
                                            <label for="">Card Number</label>
                                            <input class="form-control" type="text" name="card_number"  value="{{ old('card_number') }}">
                                            @if ($errors->has('card_number'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_month') ? 'has-error' : '' }}">
                                            <label for="">Expiry Month</label>
                                            <input class="form-control" type="text" name="card_month"  value="{{ old('card_month') }}">
                                            @if ($errors->has('card_month'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_month') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_year') ? 'has-error' : '' }}">
                                            <label for="">Expiry Year</label>
                                            <input class="form-control" type="text" name="card_year"  value="{{ old('card_year') }}">
                                            @if ($errors->has('card_year'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_year') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-success">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script>
        $("#event_end").datepicker({
            minDate: new Date()
        });

        $("#event_start").datepicker({
            minDate: new Date(),
            onSelect: function(dateText, inst) {
                var selectedDate = $(this).datepicker("getDate");
                $("#event_end").datepicker("option", "minDate", selectedDate);
            }
        });

        // Display today date for conference start
        $(function() {
            $("#event_start").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "0");
        });


        // Display today date conference end
        $(function() {
            $("#event_end").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "0");
        });
    </script>
@endsection