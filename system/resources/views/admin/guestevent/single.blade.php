@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Guest Events
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Guest Events Event</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Guest Event Booking Details
                            <a href="{{route('admin.company.edit',$event->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Booking Details</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Name:</b>  {!! $event->title !!} {!! $event->first_name  !!} {!! $event->last_name !!}
                                </div>
                                <div class="col-md-4">
                                    <b>Address 1:</b>  {{$event->address1}}
                                </div>
                                <div class="col-md-4">
                                    <b>Address2:</b> {{$event->address2}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>City:</b>  {!! $event->city !!}
                                </div>
                                <div class="col-md-4">
                                    <b>Zip Code:</b>  {{$event->postalcode}}
                                </div>
                                <div class="col-md-4">
                                    <b>Country:</b> {{$event->country}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Phone:</b>  {!! $event->phone !!}
                                </div>
                                <div class="col-md-4">
                                    <b>Email Address:</b>  {{$event->email}}
                                </div>
                                <div class="col-md-4">
                                    <b>Oraganization Name:</b> {{$event->org}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Event Name:</b> {{$event->event_type}}
                                </div>
                                <div class="col-md-4">
                                    <b>Start Date Time:</b>  {!! $event->start_date !!} <!--  {!! $event->start_time !!}  -->
                                </div>
                                <div class="col-md-4">
                                    <b>No Of Attandance:</b> {!! $event->attendance !!}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Package : </b>{{$event->package_name}}
                                </div>
                                <div class="col-md-4">
                                    <b>Package Suggestion : </b>{{$event->package_suggestion}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Number of adults : </b>
                                    @if($event->adult_more!=null)
                                        {{$event->adult_more}}
                                    @elseif($event->adult_number!=null)
                                        {{$event->adult_number}}
                                        @else
                                        {{'NA'}}
                                        @endif

                                </div>
                                <div class="col-md-4">
                                    <b>Number of Children(0-5) : </b>
                                    @if( $event->child_more!=null)
                                        {{$event->child_more}}
                                    @elseif($event->child_number!=null)
                                        {{$event->child_number}}
                                    @else
                                        {{'NA'}}
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <b>Number of Children(6-10) : </b>
                                    @if($event->children_more!=null)
                                        {{$event->children_more}}
                                    @elseif($event->children_number!=null)
                                        {{$event->children_number}}

                                    @else
                                        {{'NA'}}
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Room Details:</b>
                                    <ul class="list-unstyled">
                                        <li>Standard Room : <span style="margin-left: 10px;"> {{$event->standard_room_quantity}} {{$event->standard_room_occupant}} Rooms  </span></li>
                                        <li>Wooden Room : <span style="margin-left: 10px;">{{$event->wooden_room_quantity}} {{$event->wooden_room_occupant}} Rooms</span></li>
                                        <li>Tented Room : <span style="margin-left: 10px;">{{$event->tented_room_quantity}} {{$event->tented_room_occupant}} Rooms </span></li>
                                        <li>Group Room : <span style="margin-left: 10px;">{{$event->group_room_quantity}} {{$event->group_room_occupant}} Rooms</span></li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <b>Room Suggestion:</b>  {!! $event->room_suggestion !!}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Payment Method: </b>{{$event->paymentmethod}}
                                </div>
                                <div class="col-md-4">
                                    <b>Credit Card Number:</b>{!! $event->cardnumber!!}
                                </div>
                                <div class="col-md-4">
                                    <b>Card Expire Date: </b>{{$event->card_month}}  {{$event->card_year}}
                                </div>

                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop