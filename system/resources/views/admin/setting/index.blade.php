    @extends('admin.layouts.app')
    <style>
        .panel-body .row{
            margin-bottom: 5px;
        }
        .panel-body ul li{
            padding: 5px 0px;
        }
    </style>
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Setting
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Site Setting
                            <a href="{{route('admin.setting.edit')}}" class="pull-right"><i class="fa fa-plus"></i>Edit</a>
                        </div>
                        <div class="panel-body">
                                @foreach($allsetting as $setting)

                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list-unstyled">
                                        <li><b>Site Title : </b>{{$setting->site_title}}</li>
                                        <li><b>Meta Title : </b>{{$setting->meta_title}}</li>
                                        <li><b>Keywords : </b>{{$setting->keywords}}</li>
                                        <li><b>Copyright : </b>{{$setting->copyright_text}}</li>
                                        <li><b>Edited By : </b>{{$setting->edited_by}}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <div class="logo">
                                        <img class="img-responsive center-block" style="max-width:100px" src="{!! asset($img_path.$setting->logo) !!}" alt="">
                                        <br>
                                        <h3 class="text-center">Logo</h3>
                                    </div>
                                </div>
                            </div>





                                    @endforeach
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop