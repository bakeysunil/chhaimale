@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Food
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Food</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Food
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.foods.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('food_name') ? 'has-error' : '' }}">
                                            <label>Title</label>
                                            <input class="form-control" type="text" name="food_name"  value="{{ old('food_name') }}">
                                            @if ($errors->has('food_name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('food_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('food_price') ? 'has-error' : '' }}">
                                            <label>Price</label>
                                            <input class="form-control" type="text" name="food_price"  value="{{ old('food_price') }}">
                                            @if ($errors->has('food_price'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('food_price') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('food_image') ? 'has-error' : '' }}">
                                            <label>Image</label>
                                            <input class="form-control" type="file" name="food_image"  value="{{ old('food_image') }}">
                                            @if ($errors->has('food_image'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('food_image') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Add</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop