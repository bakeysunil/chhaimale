@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference Booking
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conference Booking</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Book a conference
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.bookedroom.update.date',$olddata->id)}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h3 class="title">Conference Information</h3>
                                <hr>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_start') ? 'has-error' : '' }}">
                                            <label>Check In</label>
                                            <input class="form-control" type="text" name="check_in" readonly required value="{{ session('date')[0]['check-in'] }}">
                                            @if ($errors->has('conference_start'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_start') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_end') ? 'has-error' : '' }}">
                                            <label>Check Out</label>
                                            <input class="form-control" type="text"  name="check_out" readonly required  value="{{ session('date')[0]['check-out'] }}">
                                            @if ($errors->has('conference_end'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_end') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <h3 class="title">Rooms</h3>
                                <hr>
                                <div class="row">

                                    <div class="col-md-12">
                                        <table class="table">
                                            <tr>
                                                <th>Check</th>
                                                <th>Room Type</th>
                                                <th>Number Of adult</th>
                                                <th>Child</th>
                                                <th>Children</th>
                                                <th>Rooms</th>
                                            </tr>

                                            @foreach($rooms as $room)

                                                <tr>
                                                    <?php
                                                    $room_name = strtolower(str_replace(' ', '_', $room->type));
                                                    ?>
                                                    <td id=""><input id="{{$room_name}}" value="{{$room->id}}" name="room_id[]" type="checkbox"></td>
                                                    <td>{{$room->type}}</td>
                                                    <td><input class="form-control  {{$room_name}}"   value="1" type="text" style=" display:none;" name="{{$room_name}}_adult" ></td>
                                                    <td><input class="form-control  {{$room_name}}"   value="0" type="text" style="display:none;"  name="{{$room_name}}_child"></td>
                                                    <td><input class="form-control {{$room_name}}" value="0" type="text" style=" display:none;" name="{{$room_name}}_children"></td>

                                                    <td style="display: none;" id="{{$room_name}}_td">

                                                        @if($room_type_count[$room_name]!=null)
                                                            <select style="display: none;" id="{{$room_name}}_option" name="{{$room_name}}[]" multiple="multiple" class="form-control multiselect-ui">
                                                                @foreach($room_number_select[$room_name] as $room_select)
                                                                    <option  value="{{$room_select->id}}">{{$room_select->name}}</option>

                                                                @endforeach
                                                            </select>
                                                        @else
                                                            <select name="" id="example-getting-started" multiple class="form-control multiselect-ui">
                                                                <option disabled>All Booked</option>
                                                            </select>


                                                        @endif

                                                    </td>

                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                {{--end of row--}}


                                <hr>
                                <button class="btn btn-success">Continue</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script>
        @foreach($rooms as $room)
        <?php
        $room_name = strtolower(str_replace(' ', '_', $room->type));
        ?>
        $(document).ready(function() {
            $('#{{$room_name}}_option').multiselect();
        });
        @endforeach
    </script>
    <script>
        @foreach($rooms as $room)
        <?php
        $room_name = strtolower(str_replace(' ', '_', $room->type));
        ?>
        $("#{{$room_name}}").click(function(){
            if ($('#{{$room_name}}').is(':checked')){
                $('.{{$room_name}}').show();
                $('#{{$room_name}}_td').css('display','block');
                $(".{{$room_name}}").prop('required',true);
            }else{
                $('.{{$room_name}}').hide();
                $('#{{$room_name}}_td').css('display','none');
                $(".{{$room_name}}").prop('required',false);
            }
        });
        @if($room_type_count[$room_name]==0)
        $("#{{$room_name}}").css('display','none');
        $(".{{$room_name.'_select'}}").css('display','block');
        @endif
        @endforeach
    </script>
@endsection