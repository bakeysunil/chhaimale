@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Rooms Bookings
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"> Edit Rooms Bookings</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    @if(Session::has('flash_notice'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_notice')}}
                        </div>
                    @endif
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Edit Room Date
                        </div>
                        <div class="panel-body">
                                <form action="{{route('admin.bookedroom.update.room',$data->id)}}" method="POST">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">From</label>
                                        <input class="form-control" type="text" name="check_in" id="booked_from" value="{{$data->check_in}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">To</label>
                                        <input class="form-control" type="text" name="check_out" id="booked_to" value="{{$data->check_out}}">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Option</label>
                                        <button class="btn btn-default form-control" type="submit">Continue</button>
                                    </div>
                                    <div class="col-md-12">
                                        <br>
                                        <h5 class="text-danger">NOTE: If date is changed booking will be automatically cleared.</h5>
                                    </div>
                                </div>
                            </form>

                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent
    <script>
        $( "#booked_to" ).datepicker({
            minDate: new Date()
        });

        $( "#booked_from" ).datepicker({
            minDate: new Date(),
            onSelect: function(dateText, inst) {
                var selectedDate = $( this ).datepicker( "getDate" );
                $( "#booked_to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        // $(function() {
        //     $("#booked_from").datepicker({
        //         dateFormat: "yy-mm-dd"
        //     }).datepicker("setDate", "0");
        // });
        // $(function() {
        //     $("#booked_to").datepicker({
        //         dateFormat: "yy-mm-dd"
        //     }).datepicker("setDate", "1");
        // });
    </script>
@stop