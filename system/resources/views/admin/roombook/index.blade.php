@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Rooms Bookings
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Rooms Bookings</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    @if(Session::has('flash_notice'))
                        <div class="alert alert-success">
                            {{ Session::get('flash_notice')}}
                        </div>
                    @endif
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Room Bookings
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.bookedroom.search')}}" method="POST">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">From</label>
                                        <input class="form-control" type="text" name="check_in" id="booked_from">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">To</label>
                                        <input class="form-control" type="text" name="check_out" id="booked_to">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Option</label>
                                        <button class="btn btn-default form-control" type="submit">Book a room</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover"
                                       @if(count($data) >0)
                                       id="dataTables-example"
                                        @endif
                                >
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Booking By</th>
                                        <th>Phone Number</th>
                                        <th>Email Address</th>
                                        <th>Check In </th>
                                        <th>Check Out</th>
                                        <th>Room Numbers</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>

                                    @forelse($data as $d)
                                        <?php
                                         $room_number = json_decode($d['rooms_type_number']);?>



                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td> {!! $d->first_name  !!} {!! $d->last_name !!}</td>
                                            <td>{!! $d->phone !!}</td>
                                            <td>{!! $d->email !!}</td>
                                            <td>{!! $d->check_in !!}</td>

                                            <td>{!! $d->check_out!!}</td>
                                            <td>
                                                @foreach($room_number as $rooms)
                                                    @foreach($rooms as $r)
                                                        @foreach($r as $rn)
                                                        {{ $d->getRoomNumber($rn) }}
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                            </td>
                                            <td>{!! date('d F Y',strtotime($d->created_at)) !!}</td>
                                            <td>
                                                <nobr>
                                                    <a href="{{route('admin.bookedroom.view',$d->id)}}" class="btn btn-success" title="View"><i class="fa fa-search"></i></a>
                                                    <a href="{{route('admin.bookedroom.edit',$d->id)}}" class="btn btn-info" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <a href="{{route('admin.bookedroom.date.edit',$d->id)}}" class="btn btn-info" title="Change Room"><i class="fa fa-building-o"></i></a>
                                                    <a href="{{route('admin.bookedroom.delete',$d->id)}}" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                                                </nobr>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="9">No Bookings yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent
    <script>
        $( "#booked_to" ).datepicker({
            minDate: new Date()
        });

        $( "#booked_from" ).datepicker({
            minDate: new Date(),
            onSelect: function(dateText, inst) {
                var selectedDate = $( this ).datepicker( "getDate" );
                $( "#booked_to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $(function() {
            $("#booked_from").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "0");
        });
        $(function() {
            $("#booked_to").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "1");
        });
    </script>
@stop