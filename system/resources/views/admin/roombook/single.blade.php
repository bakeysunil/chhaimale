@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Booked Rooms
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Booked Room </li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Room Booking Details
                            <a href="{{route('admin.bookedroom.edit',$data->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Booking Details</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Name:</b> {!! $data->first_name  !!} {!! $data->last_name !!}
                                </div>
                                <div class="col-md-3">
                                    <b>Address 1:</b>  {{$data->address1}}
                                </div>
                                <div class="col-md-3">
                                    <b>Address2:</b> {{$data->address2}}
                                </div>
                                <div class="col-md-3">
                                    <b>City:</b>  {!! $data->city !!}
                                </div>
                            </div>
                            <hr>
                            <div class="row">

                                <div class="col-md-3">
                                    <b>Zip Code:</b>  {{$data->zip}}
                                </div>
                                <div class="col-md-3">
                                    <b>Country:</b> {{$data->country}}
                                </div>
                                <div class="col-md-3">
                                    <b>Phone:</b>  {!! $data->phone !!}
                                </div>
                                <div class="col-md-3">
                                    <b>Email Address:</b>  {{$data->email}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <?php
                                $room_number = json_decode($data['rooms_type_number']);?>
                                <div class="col-md-3">
                                    <b>Oraganization Name:</b> {{$data->org}}
                                </div>
                                <div class="col-md-3">
                                    <b>Start Date Time:</b>  {!! $data->check_in !!}
                                </div>
                                <div class="col-md-3">
                                    <b>End Date Time:</b>  {!! $data->check_out !!}
                                </div>
                                <div class="col-md-3">
                                    <b>Room Number : </b>  @foreach($room_number as $rooms)
                                        @foreach($rooms as $r)
                                            @foreach($r as $rn)
                                                {{ $data->getRoomNumber($rn) }},
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                </div>

                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-3">
                                    <b>Payment Method: </b>{{$data->payment_method}}
                                </div>
                                <div class="col-md-3">
                                    <b>Card type: </b>{{$data->card_type}}
                                </div>
                                <div class="col-md-3">
                                    <b>Credit Card Number:</b>{!! $data->card_number!!}
                                </div>
                                <div class="col-md-3">
                                    <b>Card Expire Date: </b>{{$data->card_month}}  {{$data->card_year}}
                                </div>

                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-3">
                                    <b>Room Cost : </b>{{$data->sub_total}}
                                </div>
                                <div class="col-md-3">
                                    <b>Service Charge : </b>{{$data->service_charge}}
                                </div>
                                <div class="col-md-3">
                                    <b>Vat : </b>{{$data->vat}}
                                </div>
                                <div class="col-md-3">
                                    <b>Total : </b>{{$data->total}}
                                </div>
                            </div>
                            <hr>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop