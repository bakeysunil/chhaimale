@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Room Booking
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Room Booking</li>
            </ol>
        </section>

    <?php $room_type_number_count = count(json_decode($olddata->rooms_type_number,true));
    $room_type_number = json_decode($olddata->rooms_type_number,true);
    $occupancy = json_decode($olddata->occupancy,true);

    ?>



        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Book a conference
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.bookedroom.changed.room',$olddata->id)}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h3 class="title">Room Information</h3>
                                <hr>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_start') ? 'has-error' : '' }}">
                                            <label>Check In</label>
                                            <input class="form-control" type="text" name="check_in" readonly required value="{{ session('date')[0]['check-in'] }}">
                                            @if ($errors->has('conference_start'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_start') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_end') ? 'has-error' : '' }}">
                                            <label>Check Out</label>
                                            <input class="form-control" type="text"  name="check_out" readonly required  value="{{ session('date')[0]['check-out'] }}">
                                            @if ($errors->has('conference_end'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_end') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <h3 class="title">Rooms</h3>
                                <hr>
                                <div class="row">

                                    <div class="col-md-12">
                                        <table class="table">
                                            <tr>
                                                <th>Check</th>
                                                <th>Room Type</th>
                                                <th>Number Of adult</th>
                                                <th>Child</th>
                                                <th>Children</th>
                                                <th>Rooms</th>
                                            </tr>
                                        <?php $j=0; ?>
                                            @foreach($rooms as $room)

                                                <tr>
                                                    <?php
                                                    $room_name = strtolower(str_replace(' ', '_', $room->type));
                                                    $room_checked = json_decode($olddata->room_id,true);
                                                    ?>

                                                    <td id=""><input id="{{$room_name}}" value="{{$room->id}}" name="room_id[]" type="checkbox"  {{in_array($room->id,$room_checked) ? 'checked' : ''}}></td>
                                                    <td>{{$room->type}}</td>
                                                        @if($j<$room_type_number_count)
<?php
                                                            $adult = $occupancy[$j][$room_name.'_adult'];
                                                            $child = $occupancy[$j][$room_name.'_child'];
                                                            $children = $occupancy[$j][$room_name.'_children'];
                                                            ?>
                                                            @else
                                                            <?php
                                                            $adult =1;
                                                            $child = 0;
                                                            $children =0 ;
                                                            ?>
                                                            @endif
                                                    <td><input class="form-control  {{$room_name}}"   value="{{$adult}}" type="text" style=" display:none;" name="{{$room_name}}_adult" ></td>
                                                    <td><input class="form-control  {{$room_name}}"   value="{{$child}}" type="text" style="display:none;"  name="{{$room_name}}_child"></td>
                                                    <td><input class="form-control {{$room_name}}" value="{{$children}}" type="text" style=" display:none;" name="{{$room_name}}_children"></td>

                                                    <td style="display: none;" id="{{$room_name}}_td">





                                                            <select style="display: none;" id="{{$room_name}}_option" name="{{$room_name}}[]" multiple="multiple" class="form-control multiselect-ui">
                                                                @if($j<$room_type_number_count)
                                                                    @foreach($room_type_number[$j][$room_name] as $rtn)
                                                                        <option value="{{$rtn}}" selected>{{ $room->getRoomNumber($rtn) }}</option>
                                                                    @endforeach
                                                                @endif

                                                                @if($room_type_count[$room_name]!=null)
                                                                @foreach($room_number_select[$room_name] as $room_select)
                                                                    <option  value="{{$room_select->id}}">{{$room_select->name}}</option>

                                                                @endforeach
                                                            </select>
                                                        @else



                                                        @endif

                                                    </td>

                                                </tr>
                                                <?php $j++ ?>
                                            @endforeach

                                        </table>
                                    </div>
                                </div>
                                {{--end of row--}}


                                <hr>
                                <button class="btn btn-success">Update</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script>
        @foreach($rooms as $room)
        <?php
        $room_name = strtolower(str_replace(' ', '_', $room->type));
        ?>
        $(document).ready(function() {
            $('#{{$room_name}}_option').multiselect();
        });
        @endforeach
    </script>
    <script>
        @foreach($rooms as $room)
        <?php
        $room_name = strtolower(str_replace(' ', '_', $room->type));
        ?>
        $("#{{$room_name}}").click(function(){
            if ($('#{{$room_name}}').is(':checked')){
                $('.{{$room_name}}').show();
                $('#{{$room_name}}_td').css('display','block');
                $(".{{$room_name}}").prop('required',true);
            }else{
                $('.{{$room_name}}').hide();
                $('#{{$room_name}}_td').css('display','none');
                $(".{{$room_name}}").prop('required',false);
            }
        });
        @if($room_type_count[$room_name]==0)
        $("#{{$room_name}}").css('display','none');
        $(".{{$room_name.'_select'}}").css('display','none');
        @endif

        @endforeach

        @foreach($oldrooms as $room)
        <?php
        $room_name = strtolower(str_replace(' ', '_', $room->type));
        ?>
        $( document ).ready(function() {
            if ($('#{{$room_name}}').is(':checked')){
                $('.{{$room_name}}').show();
                $('#{{$room_name}}_td').css('display','block');
                $(".{{$room_name}}").prop('required',true);
            }else{
                $('.{{$room_name}}').hide();
                $('#{{$room_name}}_td').css('display','none');
                $(".{{$room_name}}").prop('required',false);
            }
        });

        $("#{{$room_name}}").css('display','block');
        $(".{{$room_name.'_select'}}").css('display','block');

        @endforeach
    </script>
@endsection