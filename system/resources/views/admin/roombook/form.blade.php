@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference Booking
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conference Booking</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Book a conference
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.bookedroom.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h3 class="title">Personal Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('firstname') ? 'has-error' : '' }}">
                                            <label>First Name</label>
                                            <input class="form-control" type="text" name="firstname" required value="{{ old('firstname') }}">
                                            @if ($errors->has('firstname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('lastname') ? 'has-error' : '' }}">
                                            <label>Last Name</label>
                                            <input class="form-control" type="text" name="lastname" required  value="{{ old('lastname') }}">
                                            @if ($errors->has('lastname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('addressline1') ? 'has-error' : '' }}">
                                            <label>Address Line 1</label>
                                            <input class="form-control" type="text" name="addressline1" required value="{{ old('addressline1') }}">
                                            @if ($errors->has('addressline1'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('addressline1') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('addressline2') ? 'has-error' : '' }}">
                                            <label for="">Address Line 2</label>
                                            <input type="text" class="form-control" name="addressline2" value="{{old('addressline2')}}">
                                            @if ($errors->has('addressline2'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('addressline2') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('city') ? 'has-error' : '' }}">
                                            <label>City</label>
                                            <input class="form-control" type="text" name="city"  value="{{ old('city') }}">
                                            @if ($errors->has('city'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('postalcode') ? 'has-error' : '' }}">
                                            <label>Zip/postal Code</label>
                                            <input class="form-control" type="text" name="postalcode"  value="{{ old('postalcode') }}">
                                            @if ($errors->has('postalcode'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('postalcode') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('country') ? 'has-error' : '' }}">
                                            <label>Country</label>
                                            <input class="form-control" type="text" name="country"  value="{{ old('country') }}">
                                            @if ($errors->has('country'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('phone') ? 'has-error' : '' }}">
                                            <label for="">Phone</label>
                                            <input type="number" class="form-control" name="phone" required="">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('email') ? 'has-error' : '' }}">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="email" required  value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('org') ? 'has-error' : '' }}">
                                            <label>Organization Name</label>
                                            <input class="form-control" type="text" name="org"  value="{{ old('org') }}">
                                            @if ($errors->has('org'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('org') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('paymentmethod') ? 'has-error' : '' }}">
                                            <label for="">Payment Method</label>
                                            <select name="paymentmethod" id="" class="form-control">
                                                <option value="cashpayment">Cash</option>
                                                <option value="credit-card">Credit Card</option>
                                            </select>
                                            @if ($errors->has('paymentmethod'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('paymentmethod') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_type') ? 'has-error' : '' }}">
                                            <label for="">Card type</label>
                                            <input class="form-control" type="text" name="card_type"  value="{{ old('card_type') }}">
                                            @if ($errors->has('card_type'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_type') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_number') ? 'has-error' : '' }}">
                                            <label for="">Card Number</label>
                                            <input class="form-control" type="text" name="card_number"  value="{{ old('card_number') }}">
                                            @if ($errors->has('card_number'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_month') ? 'has-error' : '' }}">
                                            <label for="">Expiry Month</label>
                                            <input class="form-control" type="text" name="card_month"  value="{{ old('card_month') }}">
                                            @if ($errors->has('card_month'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_month') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_year') ? 'has-error' : '' }}">
                                            <label for="">Expiry Year</label>
                                            <input class="form-control" type="text" name="card_year"  value="{{ old('card_year') }}">
                                            @if ($errors->has('card_year'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_year') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <button class="btn btn-success">Continue</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')

@endsection