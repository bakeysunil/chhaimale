@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Slider
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Slider</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Slider
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.slider.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('slider_text') ? 'has-error' : '' }}">
                                            <label>Text</label>
                                            <input class="form-control" type="text" name="slider_text"  value="{{ old('slider_text') }}">
                                            @if ($errors->has('slider_text'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slider_text') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('slider_order') ? 'has-error' : '' }}">
                                            <label>Order</label>
                                            <input class="form-control" type="text" name="slider_order"  value="{{ old('slider_order') }}">
                                            @if ($errors->has('slider_order'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slider_order') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('slider_image') ? 'has-error' : '' }}">
                                            <label>Image</label>
                                            <input class="form-control" type="file" name="slider_image" value="{{ old('slider_image') }}">
                                            @if ($errors->has('slider_image'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('slider_image') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Add New </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop