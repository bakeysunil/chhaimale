@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Profile
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User Profile</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  User Profile
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.user.update')}}" method="post">
                                {{csrf_field()}}
                                @if(session()->has('message'))
                                    <div class="alert alert-success">
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                @if(session()->has('errors'))
                                    <div class="alert alert-danger">
                                        {{ session()->get('errors') }}
                                    </div>
                                @endif
                                @if(session()->has('unmatch'))
                                    <div class="alert alert-warning">
                                        {{ session()->get('unmatch') }}
                                    </div>
                                @endif
                                <input type="hidden" name="id" value="{!! $user->id !!}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('user_name') ? 'has-error' : '' }}">
                                            <label>Name</label>
                                            <input class="form-control" type="text" name="user_name"  value="{{ $user->name }}">
                                            @if ($errors->has('user_name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('user_email') ? 'has-error' : '' }}">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="user_email"  value="{{ $user->email }}">
                                            @if ($errors->has('user_email'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('user_password') ? 'has-error' : '' }}">
                                            <label>New Password</label>
                                            <input class="form-control" type="password" name="user_password">
                                            @if ($errors->has('user_password'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('user_password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('confirm_password') ? 'has-error' : '' }}">
                                            <label>Confirm Password</label>
                                            <input class="form-control" type="password" name="confirm_password">
                                            @if ($errors->has('confirm_password'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update Profile</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop