@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Room Number
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Room Number</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Update Room
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.rooms.number.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="text" hidden value="{{$data->id}}" name="id">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_name') ? 'has-error' : '' }}">
                                            <label>Name</label>
                                            <input class="form-control" type="text" name="room_name"  value="{{ $data->name }}">
                                            @if ($errors->has('room_name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('room_type') ? 'has-error' : '' }}">
                                            <label>Position</label>
                                            <select class="form-control" name="room_type" id="">
                                                @foreach($room_types as $room_type)
                                                    <option value="{{$room_type->id}}">{{$room_type->type}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('room_type'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_type') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Option</label>
                                        <button type="submit" class="btn btn-success form-control" value="save">Update</button>
                                    </div>


                                </div>

                                <div class="clearfix"></div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop