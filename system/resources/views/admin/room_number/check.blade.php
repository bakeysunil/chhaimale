@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurant
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Restaurant</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  All Restaurant
                            <a href="{{route('admin.rooms.number.add')}}" class="pull-right"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.rooms.number.search')}}" method="POST">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">From</label>
                                        <input class="form-control" type="text" name="booked_from" id="booked_from">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">To</label>
                                        <input class="form-control" type="text" name="booked_to" id="booked_to">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Option</label>
                                        <button class="btn btn-default form-control" type="submit">Check Avaialbilty</button>
                                    </div>
                                </div>
                            </form>
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Room Number</th>
                                    <th>Room Type</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                   <tr>
                                       <td>{{$d->name}}</td>
                                       <td>{{ $d->getRoomType($d->room_type_id)}}</td>
                                   </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop
@section('scripts')

    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
    </script>
    <script>
        $( "#booked_to" ).datepicker({
            minDate: new Date()
        });

        $( "#booked_from" ).datepicker({
            minDate: new Date(),
            onSelect: function(dateText, inst) {
                var selectedDate = $( this ).datepicker( "getDate" );
                $( "#booked_to" ).datepicker( "option", "minDate", selectedDate );
            }
        });
        $(function() {
            $("#booked_from").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "0");
        });
        $(function() {
            $("#booked_to").datepicker({
                dateFormat: "yy-mm-dd"
            }).datepicker("setDate", "1");
        });
    </script>
@endsection