@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Activities
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Activities</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Update Activity
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.activities.update')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{!! $activity->id !!}">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('activity_name') ? 'has-error' : '' }}">
                                        <label>Activity Name</label>
                                        <input class="form-control" type="text" name="activity_name"  value="{{ $activity->name }}">
                                        @if ($errors->has('activity_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('activity_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('activity_image') ? 'has-error' : '' }}">
                                        <label>Activity Image</label>
                                        <input class="form-control" type="file" name="activity_image"  value="">
                                        <img src="{!! asset($img_path.$activity->image) !!}" alt="">
                                        @if ($errors->has('activity_image'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('activity_image') }}</strong>

                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Update Activity</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop