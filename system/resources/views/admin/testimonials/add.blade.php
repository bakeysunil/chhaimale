@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Testimonial
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Testimonial</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Testimonial
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.testimonials.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('testimonial_name') ? 'has-error' : '' }}">
                                        <label>Name</label>
                                        <input class="form-control" type="text" name="testimonial_name"  value="{{ old('testimonial_name') }}">
                                        @if ($errors->has('testimonial_name'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('testimonial_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('testimonial_position') ? 'has-error' : '' }}">
                                        <label>Position</label>
                                        <input class="form-control" type="text" name="testimonial_position"  value="{{ old('testimonial_position') }}">
                                        @if ($errors->has('testimonial_position'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('testimonial_position') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('testimonial_comment') ? 'has-error' : '' }}">
                                            <label>Comment</label>
                                            <textarea class="form-control" rows="5" type="text" name="testimonial_comment"  value="{{ old('testimonial_comment') }}">
                                            </textarea>
                                            @if ($errors->has('testimonial_comment'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('testimonial_comment') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('testimonial_company') ? 'has-error' : '' }}">
                                            <label>Company</label>
                                            <input class="form-control" type="text" name="testimonial_company"  value="{{ old('testimonial_company') }}">
                                            @if ($errors->has('testimonial_company'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('testimonial_company') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('testimonial_rating') ? 'has-error' : '' }}">
                                            <label>Rating</label>
                                            <input class="form-control" type="text" name="testimonial_rating"  value="{{ old('testimonial_rating') }}">
                                            @if ($errors->has('testimonial_rating'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('testimonial_rating') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('testimonial_number') ? 'has-error' : '' }}">
                                            <label>Number</label>
                                            <input class="form-control" type="text" name="testimonial_number"  value="{{ old('testimonial_number') }}">
                                            @if ($errors->has('testimonial_number'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('testimonial_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('testimonial_image') ? 'has-error' : '' }}">
                                            <label>User Image</label>
                                            <input class="form-control" type="file" name="testimonial_image"  value="{{ old('testimonial_image') }}">
                                            @if ($errors->has('testimonial_image'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('testimonial_image') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Add New </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop