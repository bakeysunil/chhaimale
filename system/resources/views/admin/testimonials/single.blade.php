@extends('admin.layouts.app')
<style>
    .panel-body .row{
        margin-bottom: 5px;
    }
    .panel-body ul li{
        padding: 5px 0px;
    }
</style>
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Testimonial
                <small>Detail View</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Testimonial</li>
            </ol>
        </section>
        <section class="content">
                <div class="row">
                    <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Testimonial Details
                            <a href="{{route('admin.testimonials.edit',$testimonial->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Details</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul class="list-unstyled">
                                        <li><b>Name : </b> {{$testimonial->name}}</li>
                                        <li><b>Company : </b>{{$testimonial->company}}</li>
                                        <li><b>Position : </b>{{$testimonial->position}}</li>
                                        <li><b>Rating : </b>{{$testimonial->rating}}</li>
                                        <li> <b>Phone : </b>{{$testimonial->phone_number}}</li>
                                        <li><b>Comment : </b>{{$testimonial->comment}}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-responsive img-circle pull-right" style="max-width:200px; max-height: 150px; padding: 20px;" src="{!! asset($img_path.$testimonial->picture) !!}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
        </section>
    </div>
@endsection