@extends('admin.layouts.app')
<style>
    .panel-body .row{
        margin-bottom: 5px;
    }
    .panel-body ul li{
        padding: 5px 0px;
    }
</style>
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Restaurant
                <small>Detail View</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Restaurant</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Restaurant Details
                            <a href="{{route('admin.restaurant.edit',$resturant->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Details</a>
                        </div>
                        <div class="panel-body">
                           <div class="row">
                               <div class="col-md-6">
                                   <div class="title">
                                       <h3>Page Detail</h3>
                                   </div>
                                       <ul class="list-unstyled">
                                           <li><b>Page header : </b> {{$resturant->page_header}}</li>
                                           <li><b>Sub header : </b> {{$resturant->sub_header}}</li>
                                       </ul>
                               </div>
                           </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="title">
                                        <h3>Chef Detail</h3>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li><b>Name : </b> {{$resturant->chef_name}}</li>
                                        <li><b>Title : </b> {{$resturant->chef_title}}</li>
                                        <li><b>Message : </b> {{$resturant->chef_message}}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-responsive img-circle center-block" style="max-width:300px; max-height: 200px; padding: 20px;" src="{!! asset($img_path.$resturant->chef_photo) !!}" alt="">
                                </div>

                            </div>
                            <div class="row">
                               <div class="col-md-6">
                                   <div class="title">
                                       <h3>Featured Details</h3>
                                   </div>
                                   <ul class="list-unstyled">
                                       <li><b>Title : </b> {{$resturant->featured_title}}</li>
                                       <li><b>Message : </b> {{$resturant->featured_message}}</li>
                                   </ul>
                               </div>
                                <div class="col-md-6">
                                    <img class="img-responsive center-block" style="max-width:300px; max-height: 200px; padding: 20px;" src="{!! asset($img_path.$resturant->featured_image) !!}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection