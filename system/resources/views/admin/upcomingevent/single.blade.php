@extends('admin.layouts.app')
<style>
    .panel-body .row{
        margin-bottom: 5px;
    }
    .panel-body ul li{
        margin-bottom: 5px;
        font-size: 15px;
    }
</style>
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
              Upcoming Event
                <small>Detail View</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Upcoming Event</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>Upcoming Event Details
                            <a href="{{route('admin.event.edit',$event->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Details</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list-unstyled">
                                        <li><b>Title : </b>{{$event->title}}</li>
                                        <li> <b>Sub-Title : </b>{{$event->sub_title}}</li>
                                        <li><b>Date: </b> {{$event->date}}</li>
                                        <li><b>Amount: </b>{{$event->amount}}</li>
                                        <li><b>Criteria: </b>{{$event->criteria}}</li>
                                        <li><b>Total Seats: </b>{{$event->total_seats}}</li>
                                        <li> <b>Added By : </b>{{$event->added_by}}</li>
                                        <li>   <b>Edited By : </b>{{$event->updated_by}}</li>
                                        <li>
                                            <b>Description</b>
                                            {{$event->description}}
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-responsive center-block" style="max-width:300px; max-height: 200px;" src="{!! asset($img_path.$event->image) !!}" alt="">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection