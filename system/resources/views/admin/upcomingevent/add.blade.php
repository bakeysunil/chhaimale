@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Upcoming Events
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Upcoming Events</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Event
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.upcomingevents.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_title') ? 'has-error' : '' }}">
                                        <label>Title</label>
                                        <input class="form-control" type="text" name="event_title"  value="{{ old('event_title') }}">
                                        @if ($errors->has('event_title'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_amount') ? 'has-error' : '' }}">
                                        <label>Amount</label>
                                        <input class="form-control" type="text" name="event_amount"  value="{{ old('event_amount') }}">
                                        @if ($errors->has('event_amount'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_amount') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('event_date') ? 'has-error' : '' }}">
                                            <label>Event Date</label>
                                            <input class="form-control" type="date" name="event_date" rows="5" cols="60"  value="{{ old('event_date') }}">
                                            @if ($errors->has('event_date'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_date') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group {{$errors->has('event_description') ? 'has-error' : '' }}">
                                        <label>Description</label>
                                        <textarea class="form-control" type="text" name="event_description" rows="5" cols="60"  value="{{ old('event_description') }}"></textarea>
                                        @if ($errors->has('event_description'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_criteria') ? 'has-error' : '' }}">
                                        <label>Criteria</label>
                                        <input class="form-control" type="text" name="event_criteria"  value="{{ old('event_criteria') }}">
                                        @if ($errors->has('event_criteria'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_criteria') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_total_seats') ? 'has-error' : '' }}">
                                        <label>Total Seats</label>
                                        <input class="form-control" type="text" name="event_total_seats"  value="{{ old('event_total_seats') }}">
                                        @if ($errors->has('event_total_seats'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_total_seats') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_image') ? 'has-error' : '' }}">
                                        <label>Image</label>
                                        <input class="form-control" type="file" name="event_image"  value="{{ old('event_image') }}">
                                        @if ($errors->has('event_image'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_image') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_meta_title') ? 'has-error' : '' }}">
                                        <label>Meta Title</label>
                                        <input class="form-control" type="text" name="event_meta_title"  value="{{ old('event_meta_title') }}">
                                        @if ($errors->has('event_meta_title'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_meta_title') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_meta_description') ? 'has-error' : '' }}">
                                        <label>Meta description</label>
                                        <input class="form-control" type="text" name="event_meta_description"  value="{{ old('event_meta_description') }}">
                                        @if ($errors->has('event_meta_description'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_meta_description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{$errors->has('event_keywords') ? 'has-error' : '' }}">
                                        <label>Keywords</label>
                                        <input class="form-control" type="text" name="event_keywords"  value="{{ old('event_keywords') }}">
                                        @if ($errors->has('event_keywords'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('event_keywords') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Add New </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop