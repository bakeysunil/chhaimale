@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference Booking
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conference Booking</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Book a conference
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.conference.update',$conference->id)}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h3 class="title">Conference Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conferencename') ? 'has-error' : '' }}">
                                            <label>Conference Name</label>
                                            <input class="form-control" type="text" name="conferencename" required value="{{ $conference->cfname }}">
                                            @if ($errors->has('conferencename'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conferencename') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('attendence') ? 'has-error' : '' }}">
                                            <label>Number of attendance</label>
                                            <input class="form-control" type="text" name="attendence" required   value="{{ $conference->attendance }}">
                                            @if ($errors->has('attendence'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('attendence') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>




                                <h3 class="title">Personal Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('title') ? 'has-error' : '' }}">
                                            <label for="">Title</label>
                                            <select name="title" id="" class="form-control">
                                                <option value="Mr">MR.</option>
                                                <option value="Ms">Ms.</option>
                                                <option value="Mrs">Mrs.</option>
                                            </select>
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('firstname') ? 'has-error' : '' }}">
                                            <label>First Name</label>
                                            <input class="form-control" type="text" name="firstname" required value="{{ $conference->first_name }}">
                                            @if ($errors->has('firstname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('lastname') ? 'has-error' : '' }}">
                                            <label>Last Name</label>
                                            <input class="form-control" type="text" name="lastname" required  value="{{ $conference->last_name }}">
                                            @if ($errors->has('lastname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('addressline1') ? 'has-error' : '' }}">
                                            <label>Address Line 1</label>
                                            <input class="form-control" type="text" name="addressline1" required value="{{ $conference->address1 }}">
                                            @if ($errors->has('addressline1'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('addressline1') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('addressline2') ? 'has-error' : '' }}">
                                            <label for="">Address Line 2</label>
                                            <input type="text" class="form-control" name="addressline2" value="{{ $conference->address2 }}">
                                            @if ($errors->has('addressline2'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('addressline2') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('city') ? 'has-error' : '' }}">
                                            <label>City</label>
                                            <input class="form-control" type="text" name="city"  value="{{ $conference->city }}">
                                            @if ($errors->has('city'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('postalcode') ? 'has-error' : '' }}">
                                            <label>Zip/postal Code</label>
                                            <input class="form-control" type="text" name="postalcode"  value="{{ $conference->zip}}">
                                            @if ($errors->has('postalcode'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('postalcode') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('country') ? 'has-error' : '' }}">
                                            <label>Country</label>
                                            <input class="form-control" type="text" name="country"  value="{{ $conference->country }}">
                                            @if ($errors->has('country'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('phone') ? 'has-error' : '' }}">
                                            <label for="">Phone</label>
                                            <input type="number" class="form-control" name="phone" required="" value="{{ $conference->phone }}">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('email') ? 'has-error' : '' }}">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="email" required  value="{{ $conference->email }}">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('organizationname') ? 'has-error' : '' }}">
                                            <label>Organization Name</label>
                                            <input class="form-control" type="text" name="organizationname"  value="{{ $conference->org }}">
                                            @if ($errors->has('organizationname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('organizationname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('paymentmethod') ? 'has-error' : '' }}">
                                            <label for="">Payment Method</label>
                                            <select name="paymentmethod" id="" class="form-control">
                                                <option value="cashpayment">Cash</option>
                                                <option value="credit-card">Credit Card</option>
                                            </select>
                                            @if ($errors->has('paymentmethod'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('paymentmethod') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_type') ? 'has-error' : '' }}">
                                            <label for="">Card type</label>
                                            <input class="form-control" type="text" name="card_type"  value="{{ $conference->card_type }}">
                                            @if ($errors->has('card_type'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_type') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_number') ? 'has-error' : '' }}">
                                            <label for="">Card Number</label>
                                            <input class="form-control" type="text" name="card_number"  value="{{ $conference->card_number }}">
                                            @if ($errors->has('card_number'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_month') ? 'has-error' : '' }}">
                                            <label for="">Expiry Month</label>
                                            <input class="form-control" type="text" name="card_month"  value="{{ $conference->card_month }}">
                                            @if ($errors->has('card_month'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_month') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_year') ? 'has-error' : '' }}">
                                            <label for="">Expiry Year</label>
                                            <input class="form-control" type="text" name="card_year"  value="{{ $conference->card_year }}">
                                            @if ($errors->has('card_year'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_year') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-success">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script src="{{asset('assets/js/conference.js')}}"></script>
@endsection