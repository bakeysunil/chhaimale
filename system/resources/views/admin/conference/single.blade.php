@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference conferences
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conference conferences</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Conference Booking Details
                            <a href="{{route('admin.company.edit',$conference->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Booking Details</a>
                        </div>
                        <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                    <b>Name:</b>  {!! $conference->title !!} {!! $conference->first_name  !!} {!! $conference->last_name !!}
                            </div>
                            <div class="col-md-4">
                                <b>Address 1:</b>  {{$conference->address1}}
                            </div>
                                <div class="col-md-4">
                                <b>Address2:</b> {{$conference->address2}}
                            </div>
                        </div>
                            <hr>
                        <div class="row">
                            <div class="col-md-4">
                                    <b>City:</b>  {!! $conference->city !!} 
                            </div>
                            <div class="col-md-4">
                                <b>Zip Code:</b>  {{$conference->zip}}
                            </div>
                                <div class="col-md-4">
                                <b>Country:</b> {{$conference->country}}
                            </div>
                        </div>
                            <hr>
                        <div class="row">
                            <div class="col-md-4">
                                    <b>Phone:</b>  {!! $conference->phone !!} 
                            </div>
                            <div class="col-md-4">
                                <b>Email Address:</b>  {{$conference->email}}
                            </div>
                                <div class="col-md-4">
                                <b>Oraganization Name:</b> {{$conference->org}}
                            </div>
                        </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                <b>Conference Name:</b> {{$conference->cfname}}
                            </div>
                            <div class="col-md-4">
                                    <b>Start Date Time:</b>  {!! $conference->startdate !!} <!--  {!! $conference->starttime !!}  -->
                            </div>
                            <div class="col-md-4">
                                <b>End Date Time:</b>  {!! $conference->enddate !!}  <!-- {!! $conference->endtime !!}  -->
                            </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                <b>No Of Attandance:</b> {!! $conference->attendance !!}
                            </div>
                           <!--  <div class="col-md-4">
                                    <b>Setup Style:</b>  {!! $conference->setup_suggestion !!} 
                            </div> -->
                            <div class="col-md-4">
                                <b>Setup Suggestion:</b>  {!! $conference->setup_suggestion !!}
                            </div> 
                            </div>
                            <hr>
                            <?php 
                            if($conference->starttime!=null){
                                $st = json_decode($conference->starttime);
                            }
                            if($conference->endtime!=null){
                                 $et = json_decode($conference->endtime);
                            }
                            if($conference->breakfasttime!=null){
                                       $bt = json_decode($conference->breakfasttime);
                            }
                            if($conference->lunchtime!=null){
                                     $lt = json_decode($conference->lunchtime);
                            }
                            if($conference->dinnertime!=null){
                                $dt = json_decode($conference->dinnertime);
                            }
                            if($conference->style!=null){
                                $lay = json_decode($conference->style);
                            }      
        
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                <table class="table table-bordered">
                                    <th>Day</th>
                                    <th>Starttime</th>
                                    <th>Endtime</th>
                                    <th>Breakfast time</th>
                                    <th>Lunch Time</th>
                                    <th>Dinner Time</th>
                                    <th>Layout</th>
                                    @for($i=0;$i<=$length;$i++)
                                    <tr>
                                        <td>Day {{$i + 1}}</td>
                                        <td>
                                            @if($st[$i]!=null)
                                            {{$st[$i]}}
                                            @else
                                            {{'NA'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($et[$i]!=null)
                                            {{$et[$i]}}
                                            @else
                                            {{'NA'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($bt[$i]!=null)
                                            {{$bt[$i]}}
                                            @else
                                            {{'NA'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($lt[$i]!=null)
                                            {{$lt[$i]}}
                                            @else
                                            {{'NA'}}
                                            @endif

                                        </td>
                                        <td>
                                            @if($dt[$i]!=null)
                                            {{$dt[$i]}}
                                            @else
                                            {{'NA'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($lt[$i]!=null)
                                            {{$lay[$i]}}
                                            @else
                                            {{'NA'}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endfor
                                </table>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                <b>Meal Suggestion:</b>  {!! $conference->meal_suggestion !!} 
                            </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                <b>Room Details:</b> 
                                <ul class="list-unstyled">
                                    <li>Standard Room : <span style="margin-left: 10px;"> {{$conference->standard_room_quantity}} {{$conference->standard_room_occupant}} Rooms  </span></li>
                                    <li>Wooden Room : <span style="margin-left: 10px;">{{$conference->wooden_room_quantity}} {{$conference->wooden_room_occupant}} Rooms</span></li>
                                    <li>Tented Room : <span style="margin-left: 10px;">{{$conference->tented_room_quantity}} {{$conference->tented_room_occupant}} Rooms </span></li>
                                    <li>Group Room : <span style="margin-left: 10px;">{{$conference->group_room_quantity}} {{$conference->group_room_occupant}} Rooms</span></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <b>Room Suggestion:</b>  {!! $conference->room_suggestion !!} 
                            </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <b>Payment Method: </b>{{$conference->paymentmethod}}
                                </div>
                                <div class="col-md-4">
                                <b>Credit Card Number:</b>{!! $conference->card_number!!}
                                </div>
                                <div class="col-md-4">
                                    <b>Card Expire Date: </b>{{$conference->card_month}}  {{$conference->card_year}}
                                </div>

                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@stop

@section('scripts')
    @parent

@stop