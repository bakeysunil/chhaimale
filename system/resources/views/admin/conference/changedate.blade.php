@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference Booking
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conference Booking</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Book a conference
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.conference.datechanged',$conference->id)}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h3 class="title">Conference Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_start') ? 'has-error' : '' }}">
                                            <label>Start Date</label>
                                            <input class="form-control" type="text" id="start_date" name="conference_start" required value="{{ $conference->startdate }}">
                                            @if ($errors->has('conference_start'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_start') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_end') ? 'has-error' : '' }}">
                                            <label>End Date</label>
                                            <input class="form-control" type="text" id="end_date" name="conference_end" required  value="{{ $conference->enddate }}">
                                            @if ($errors->has('conference_end'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_end') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h5 class="text-danger">NOTE: Changing date result to data being removed.</h5>
                                    </div>
                                </div>
                                <h3 class="title cfroom_title" style="display: none;">Conference Rooms</h3>
                                <hr>
                                <section >
                                    <div id="cfroom">
                                        @for($i=0;$i<=$length;$i++)
                                        <div class="row">

                                                <?php
                                                    $st = json_decode($conference->starttime);
                                                    $et = json_decode($conference->endtime);
                                                    $lt = json_decode($conference->style);
                                                ?>
                                                                               <div class="col-md-3">
                                                                                        <div class="form-group}}">
                                                                                                <label>Days</label>
                                                                                                <input class="form-control" type="text" name=""  value="{{$i + 1}}" readonly">
                                                                                            </div>
                                                                                    </div>
                                                                                <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                                <label>Start Time</label>
                                                                                                <input class="form-control" type="text" name="conferencestart[]" value="{{$st[$i]}}">

                                                                                            </div>
                                                                                    </div>
                                                                                <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                                <label>End Time</label>
                                                                                                <input class="form-control" type="text" name="conferenceend[]"  value="{{$et[$i]}}">

                                                                                            </div>
                                                                                    </div>
                                                                                <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                                <label>Layout</label>
                                                                                                <select class="form-control" name="layout[]" id="">
                                                                                                        <option <?php if($lt[$i]=='layout1'){ echo 'selected';} ?> value="layout1" >Layout 1</option>
                                                                                                        <option <?php if($lt[$i]=='layout2'){ echo 'selected';} ?> value="layout1">Layout 2</option>
                                                                                                        <option <?php if($lt[$i]=='layout3'){ echo 'selected';} ?> value="layout1">Layout 3</option>
                                                                                                        <option <?php if($lt[$i]=='layout4'){ echo 'selected';} ?> value="layout1">Layout 4</option>
                                                                                                    </select>
                                                                                            </div>
                                                                                    </div>

                                                                            </div>
                                            @endfor

                                    </div>
                                    <div class="row cf_suggestion"  id="">
                                        <div class="col-md-6">
                                            <div class="form-group {{$errors->has('layout_suggestion') ? 'has-error' : '' }}">
                                                <label>Suggestion</label>
                                                <textarea class="form-control" name="layout_suggestion" id="" cols="30" rows="5">{{$conference->setup_suggestion}}</textarea>
                                                @if ($errors->has('layout_suggestion'))
                                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('layout_suggestion') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <h3 class="title cfroom_title">Meals</h3>
                                <hr>
                                <section>
                                    <div id="cf_meal">
                                        @for($i=0;$i<=$length;$i++)
                                            <?php
                                            $bf = json_decode($conference->breakfasttime);
                                            $lu = json_decode($conference->lunchtime);
                                            $din = json_decode($conference->dinnertime);
                                            ?>
                                         <div class="row">
                                             <div class="col-md-3">
                                                 <div class="form-group">
                                                                                               <label>Days</label>
                                                                                               <input class="form-control" type="text" name=""  value="{{$i + 1}}" readonly>

                                                 </div>
                                                                                  </div>
                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                               <label>BreakFast</label>
                                                                                              <input class="form-control" type="text" name="breakfasttime[]" value="{{$bf[$i]}}">

                                                                                </div>
                                                                            </div>
                                                                               <div class="col-md-3">
                                                                                      <div class="form-group">
                                                                                             <label>Lunch</label>
                                                                                               <input class="form-control" type="text" name="lunchtime[]"   value="{{$lu[$i]}}">

                                                                                      </div>
                                                                                   </div>
                                             <div class="col-md-3">
                                                                                       <div class="form-group">
                                                                                               <label>Dinner</label>
                                                                                              <input class="form-control" type="text" name="dinnertime[]"  value="{{$din[$i]}}">

                                                                                         </div>
                                                                                   </div>
                                                                              </div>
                                        @endfor
                                    </div>

                                    <div class="row cf_suggestion">

                                        <div class="col-md-6">
                                            <div class="form-group {{$errors->has('food_suggestion') ? 'has-error' : '' }}">
                                                <label>Suggestion</label>
                                                <textarea class="form-control" name="food_suggestion" id="" cols="30" rows="5">{{$conference->meal_suggestion}}</textarea>
                                                @if ($errors->has('food_suggestion'))
                                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('food_suggestion') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                </section>

                                <h3 class="title">Rooms</h3>
                                <hr>
                                <div class="row">
                                    {{--standard rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room') ? 'has-error' : '' }}">
                                            <label>Room type</label>
                                            <input class="form-control" type="text"   value="Standard Room" readonly>
                                            @if ($errors->has('standard_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room_occupant') ? 'has-error' : '' }}">
                                            <label>Occupant</label>
                                            <select name="standard_room_occupant" id="standard_occupant" class="form-control">
                                                <option <?php if($conference->standard_room_occupant=='single'){ echo 'selected'; } ?> value="single" >Single </option>
                                                <option <?php if($conference->standard_room_occupant=='double'){ echo 'selected'; } ?> value="double">Double</option>

                                            </select>
                                            @if ($errors->has('standard_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room_quantity') ? 'has-error' : '' }}">
                                            <label>Quantity</label>
                                            <input class="form-control" type="text" name="standard_room_quantity"  value="{{ $conference->standard_room_quantity }}">
                                            @if ($errors->has('standard_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--//wooden Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Wooden Room" readonly>
                                            @if ($errors->has('wooden_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room_occupant') ? 'has-error' : '' }}">

                                            <select name="wooden_room_occupant" id="standard_occupant" class="form-control">
                                                <option <?php if($conference->wooden_room_occupant=='single'){ echo 'selected'; } ?>   value="single">Single </option>
                                                <option <?php if($conference->wooden_room_occupant=='double'){ echo 'selected'; } ?> value="double">Double</option>

                                            </select>
                                            @if ($errors->has('wooden_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="wooden_room_quantity"  value="{{ $conference->wooden_room_quantity }}">
                                            @if ($errors->has('wooden_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--//group Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Group Room" readonly>
                                            @if ($errors->has('group_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room_occupant') ? 'has-error' : '' }}">

                                            <select name="group_room_occupant" id="standard_occupant" class="form-control">
                                                <option <?php if($conference->group_room_occupant=='single'){ echo 'selected'; } ?>   value="single">Single </option>
                                                <option <?php if($conference->group_room_occupant=='double'){ echo 'selected'; } ?>   value="double">Double</option>

                                            </select>
                                            @if ($errors->has('group_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="group_room_quantity"  value="{{ $conference->group_room_quantity }}">
                                            @if ($errors->has('group_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--tented Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Tented Room" readonly>
                                            @if ($errors->has('tented_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room_occupant') ? 'has-error' : '' }}">

                                            <select name="tented_room_occupant" id="standard_occupant" class="form-control">
                                                <option <?php if($conference->tented_room_occupant=='single'){ echo 'selected'; } ?>   value="single">Single </option>
                                                <option <?php if($conference->tented_room_occupant=='double'){ echo 'selected'; } ?>   value="double">Double</option>

                                            </select>
                                            @if ($errors->has('tented_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="tented_room_quantity"  value="{{ $conference->tented_room_quantity }}">
                                            @if ($errors->has('tented_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="room_suggestion">Suggestion</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="room_suggestion" value="{{$conference->room_suggestion}}">
                                            @if ($errors->has('room_suggestion'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_suggestion') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--end of row--}}
                                </div>
                                <button class="btn btn-success">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script src="{{asset('assets/js/editconference.js')}}"></script>
@endsection