@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Conference Booking
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Conference Booking</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i> Book a conference
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.conference.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <h3 class="title">Conference Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conferencename') ? 'has-error' : '' }}">
                                            <label>Conference Name</label>
                                            <input class="form-control" type="text" name="conferencename" required value="{{ old('conferencename') }}">
                                            @if ($errors->has('conferencename'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conferencename') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('attendence') ? 'has-error' : '' }}">
                                            <label>Number of attendance</label>
                                            <input class="form-control" type="text" name="attendence" required   value="{{ old('attendence') }}">
                                            @if ($errors->has('attendence'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('attendence') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_start') ? 'has-error' : '' }}">
                                            <label>Start Date</label>
                                            <input class="form-control" type="text" id="start_date" name="conference_start" required value="{{ old('conference_start') }}">
                                            @if ($errors->has('conference_start'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_start') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('conference_end') ? 'has-error' : '' }}">
                                            <label>End Date</label>
                                            <input class="form-control" type="text" id="end_date" name="conference_end" required  value="{{ old('conference_end') }}">
                                            @if ($errors->has('conference_end'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('conference_end') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <h3 class="title cfroom_title" style="display: none;">Conference Rooms</h3>
                                <hr>
                                <section >
                                        <div id="cfroom">

                                        </div>
                                    <div class="row cf_suggestion" style="display: none;" id="">
                                    <div class="col-md-6">
                                        <div class="form-group {{$errors->has('layout_suggestion') ? 'has-error' : '' }}">
                                            <label>Suggestion</label>
                                            <textarea class="form-control" name="layout_suggestion" id="" cols="30" rows="5"></textarea>
                                            @if ($errors->has('layout_suggestion'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('layout_suggestion') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    </div>
                                </section>
                                <h3 class="title cfroom_title" style="display: none;">Meals</h3>
                                <hr>
                                <section>
                                    <div id="cf_meal">

                                    </div>
                                    <div class="row cf_suggestion" style="display: none;">

                                        <div class="col-md-6">
                                            <div class="form-group {{$errors->has('food_suggestion') ? 'has-error' : '' }}">
                                                <label>Suggestion</label>
                                                <textarea class="form-control" name="food_suggestion" id="" cols="30" rows="5"></textarea>
                                                @if ($errors->has('food_suggestion'))
                                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('food_suggestion') }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>
                                </section>

                                <h3 class="title">Rooms</h3>
                                <hr>
                                <div class="row">
                        {{--standard rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room') ? 'has-error' : '' }}">
                                            <label>Room type</label>
                                            <input class="form-control" type="text"   value="Standard Room" readonly>
                                            @if ($errors->has('standard_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room_occupant') ? 'has-error' : '' }}">
                                            <label>Occupant</label>
                                            <select name="standard_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('standard_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('standard_room_quantity') ? 'has-error' : '' }}">
                                            <label>Quantity</label>
                                            <input class="form-control" type="text" name="standard_room_quantity"  value="{{ old('standard_room_quantity') }}">
                                            @if ($errors->has('standard_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('standard_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--//wooden Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Wooden Room" readonly>
                                            @if ($errors->has('wooden_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room_occupant') ? 'has-error' : '' }}">

                                            <select name="wooden_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('wooden_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('wooden_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="wooden_room_quantity"  value="{{ old('wooden_room_quantity') }}">
                                            @if ($errors->has('wooden_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('wooden_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--//group Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Group Room" readonly>
                                            @if ($errors->has('group_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room_occupant') ? 'has-error' : '' }}">

                                            <select name="group_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('group_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('group_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="group_room_quantity"  value="{{ old('group_room_quantity') }}">
                                            @if ($errors->has('group_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('group_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--tented Rooms--}}
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text"   value="Tented Room" readonly>
                                            @if ($errors->has('tented_room'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room_occupant') ? 'has-error' : '' }}">

                                            <select name="tented_room_occupant" id="standard_occupant" class="form-control">
                                                <option value="single">Single </option>
                                                <option value="double">Double</option>

                                            </select>
                                            @if ($errors->has('tented_room_occupant'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room_occupant') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('tented_room_quantity') ? 'has-error' : '' }}">

                                            <input class="form-control" type="text" name="tented_room_quantity"  value="{{ old('tented_room_quantity') }}">
                                            @if ($errors->has('tented_room_quantity'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('tented_room_quantity') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="room_suggestion">Suggestion</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="room_suggestion">
                                            @if ($errors->has('room_suggestion'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('room_suggestion') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    {{--end of row--}}
                                </div>
                                <h3 class="title">Personal Information</h3>
                                <hr>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('title') ? 'has-error' : '' }}">
                                            <label for="">Title</label>
                                            <select name="title" id="" class="form-control">
                                                <option value="Mr">MR.</option>
                                                <option value="Ms">Ms.</option>
                                                <option value="Mrs">Mrs.</option>
                                            </select>
                                            @if ($errors->has('title'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('firstname') ? 'has-error' : '' }}">
                                            <label>First Name</label>
                                            <input class="form-control" type="text" name="firstname" required value="{{ old('firstname') }}">
                                            @if ($errors->has('firstname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('lastname') ? 'has-error' : '' }}">
                                            <label>Last Name</label>
                                            <input class="form-control" type="text" name="lastname" required  value="{{ old('lastname') }}">
                                            @if ($errors->has('lastname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('addressline1') ? 'has-error' : '' }}">
                                            <label>Address Line 1</label>
                                            <input class="form-control" type="text" name="addressline1" required value="{{ old('addressline1') }}">
                                            @if ($errors->has('addressline1'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('addressline1') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('addressline2') ? 'has-error' : '' }}">
                                            <label for="">Address Line 2</label>
                                            <input type="text" class="form-control" name="addressline2" value="{{old('addressline2')}}">
                                            @if ($errors->has('addressline2'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('addressline2') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('city') ? 'has-error' : '' }}">
                                            <label>City</label>
                                            <input class="form-control" type="text" name="city"  value="{{ old('city') }}">
                                            @if ($errors->has('city'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('postalcode') ? 'has-error' : '' }}">
                                            <label>Zip/postal Code</label>
                                            <input class="form-control" type="text" name="postalcode"  value="{{ old('postalcode') }}">
                                            @if ($errors->has('postalcode'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('postalcode') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('country') ? 'has-error' : '' }}">
                                            <label>Country</label>
                                            <input class="form-control" type="text" name="country"  value="{{ old('country') }}">
                                            @if ($errors->has('country'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('phone') ? 'has-error' : '' }}">
                                            <label for="">Phone</label>
                                            <input type="number" class="form-control" name="phone" required="">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('email') ? 'has-error' : '' }}">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="email" required  value="{{ old('email') }}">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('organizationname') ? 'has-error' : '' }}">
                                            <label>Organization Name</label>
                                            <input class="form-control" type="text" name="organizationname"  value="{{ old('organizationname') }}">
                                            @if ($errors->has('organizationname'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('organizationname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('paymentmethod') ? 'has-error' : '' }}">
                                            <label for="">Payment Method</label>
                                            <select name="paymentmethod" id="" class="form-control">
                                                <option value="cashpayment">Cash</option>
                                                <option value="credit-card">Credit Card</option>
                                            </select>
                                            @if ($errors->has('paymentmethod'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('paymentmethod') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_type') ? 'has-error' : '' }}">
                                            <label for="">Card type</label>
                                            <input class="form-control" type="text" name="card_type"  value="{{ old('card_type') }}">
                                            @if ($errors->has('card_type'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_type') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_number') ? 'has-error' : '' }}">
                                            <label for="">Card Number</label>
                                            <input class="form-control" type="text" name="card_number"  value="{{ old('card_number') }}">
                                            @if ($errors->has('card_number'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_month') ? 'has-error' : '' }}">
                                            <label for="">Expiry Month</label>
                                            <input class="form-control" type="text" name="card_month"  value="{{ old('card_month') }}">
                                            @if ($errors->has('card_month'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_month') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{$errors->has('card_year') ? 'has-error' : '' }}">
                                            <label for="">Expiry Year</label>
                                            <input class="form-control" type="text" name="card_year"  value="{{ old('card_year') }}">
                                            @if ($errors->has('card_year'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('card_year') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-success">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script src="{{asset('assets/js/conference.js')}}"></script>
@endsection