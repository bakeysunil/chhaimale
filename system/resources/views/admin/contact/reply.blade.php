@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Compose Message
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.contact.send')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="text" hidden value="{{$data->name}}" name="contact_name">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('contact_email') ? 'has-error' : '' }}">
                                            <label>TO :</label>
                                            <input class="form-control" type="text" readonly name="contact_email"  value="{{ $data->email }}">
                                            @if ($errors->has('contact_email'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('contact_subject') ? 'has-error' : '' }}">
                                            <label>Subject</label>
                                            <input class="form-control" type="text" name="contact_subject"  value="{{ old('contact_subject') }}">
                                            @if ($errors->has('contact_subject'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_subject') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('contact_message') ? 'has-error' : '' }}">
                                            <label>Message</label>
                                            <textarea class="form-control" rows="5" name="contact_message"  value="{{ old('contact_message') }}">
                                            </textarea>
                                            @if ($errors->has('contact_message'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_message') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-success" value="save">Send</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop