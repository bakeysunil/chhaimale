@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  View Message
                        </div>
                        <div class="panel-body">
                            <form enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('contact_email') ? 'has-error' : '' }}">
                                            <label>Email :</label>
                                            <input class="form-control" type="text" readonly  name="contact_email"  value="{{$message->email}}">
                                            @if ($errors->has('contact_email'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('contact_subject') ? 'has-error' : '' }}">
                                            <label>Subject</label>
                                            <input class="form-control" type="text" readonly name="contact_subject"  value="{{ $message->subject }}">
                                            @if ($errors->has('contact_subject'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_subject') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('contact_name') ? 'has-error' : '' }}">
                                            <label>Name</label>
                                            <input  class="form-control" name="contact_name" readonly value="{{ $message->name }}">

                                            @if ($errors->has('contact_name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group {{$errors->has('contact_message') ? 'has-error' : '' }}">
                                            <label>Message</label>
                                            <textarea class="form-control" rows="5" readonly name="contact_message"  value="">
                                                {{ $message->message }}
                                            </textarea>
                                            @if ($errors->has('contact_message'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('contact_message') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4 form-group">
                                    <button class="btn btn-success">
                                        <a style="color:#fff;" href="{{route('admin.contact.reply',$message->id)}}">Reply</a></button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop