<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body{
            margin:0;
            padding: 0;
        }
    </style>
</head>
<body>
    <div>
        <img style="width:100px;height: 140px;" src="{{ $message->embed(asset('assets/images/logo.png')) }}">
        <h4>Chhaimale Resort</h4>
        <p>Hello, {{$name}}</p>
        <p>{{$msg}}</p>
    </div>
    <div>
        <h4>Regards</h4>
        <a href="chhaimaleresort.com">Chhaimaleresort.com</a>
    </div>

</body>
</html>