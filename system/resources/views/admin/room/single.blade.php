@extends('admin.layouts.app')
<style>
    .panel-body .row{
        margin-bottom: 5px;
    }
    .panel-body ul li{
        padding: 5px 0px;
    }
</style>
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Room
                <small>Detail View</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Room</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Room Details
                            <a href="{{route('admin.rooms.edit',$room->id)}}" class="pull-right"><i class="fa fa-plus"></i> Edit Details</a>
                        </div>
                        <?php
                        $det = json_decode($room->facilities);
                        ?>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="list-unstyled">
                                        <li><b>Type : </b> {{$room->type}}</li>
                                        <li><b>Starting price : </b> {{$room->starting_price}}</li>
                                        <li><b>Last price : </b> {{$room->last_price}}</li>
                                        <li><b>Total rooms : </b> {{$room->total_rooms}}</li>
                                        <li><b>Availability : </b> {{$room->availability}}</li>
                                        <li><b>Status : </b> {{$room->status}}</li>
                                        <li><b>Sales price : </b>{{$room->sales_price}}</li>
                                        <li><b>Criteria : </b>{{$room->criteria}}</li>
                                        <li><b>Facilities : </b>
                                            <ul>
                                                    @foreach($det as $d)
                                                        <li>{{$room->getRoomFacility($d)}}</li>
                                                        @endforeach
                                            </ul>
                                        </li>
                                        <li><b>Description : </b>{{$room->description}}</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <img class="img-responsive center-block" style="max-width:100%;" src="{!! asset($img_path.$room->image) !!}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection