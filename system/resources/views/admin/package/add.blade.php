@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Packages
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Packages</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-lg fa-fw"></i>  Add New Packages
                        </div>
                        <div class="panel-body">
                            <form action="{{route('admin.packages.save')}}" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('name') ? 'has-error' : '' }}">
                                            <label>Name</label>
                                            <input class="form-control" type="text" name="name"  value="{{ old('name') }}">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group {{$errors->has('price') ? 'has-error' : '' }}">
                                            <label>Price</label>
                                            <input class="form-control" type="number" name="price"  value="{{ old('price') }}">
                                            @if ($errors->has('price'))
                                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>


                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group {{$errors->has('description') ? 'has-error' : '' }}">
                                    <label>Description</label>
                                    <textarea class="form-control" rows="5" type="text" name="description"  value="{{ old('description') }}"></textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="col-md-4 form-group">
                            <button type="submit" class="btn btn-success" value="save">Add New </button>
                        </div>

                        </form>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
@stop