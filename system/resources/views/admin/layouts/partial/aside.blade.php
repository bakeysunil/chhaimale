
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('assets/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.user.profile',Auth::User()->id)}}">
                    <i class="fa fa-dashboard"></i> <span>Profile</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Users</span><span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.users')}}"><i class="fa fa-circle-o text-aqua"></i> <span>All Users</span></a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Home</span><span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.slider')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Home Sliders</span></a></li>
                    <li><a href="{{route('admin.testimonials')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Testimonials</span></a></li>
                    <li><a href="{{route('admin.activities')}}"> <i class="fa fa-files-o"></i><span>Activities</span></a></li>
                    <li><a href="{{route('admin.amenities')}}"><i class="fa fa-th"></i> <span>Amenities</span></a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-dashboard"></i> <span>Rooms</span><span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i></span></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{route('admin.rooms')}}"><i class="fa fa-circle-o text-red"></i> <span>Room Options</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.rooms.number')}}"><i class="fa fa-circle-o text-red"></i> <span>Room Number</span></a>
                    </li>
                    <li>
                        <a href="{{route('admin.bookedroom')}}"><i class="fa fa-circle-o text-red"></i> <span>Room Booking</span></a>
                    </li>

                </ul>
            </li>




            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Events</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.guestevent.index')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Guest Events</span></a></li>
                    <li>
                        <a href="{{route('admin.event')}}">
                            <i class="fa fa-calendar"></i> <span>Events Manager</span>
                        </a>
                    </li>
                    <li><a href="{{route('admin.upcomingevents')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Upcoming Events</span></a></li>
                </ul>
            </li>



            <li>
                <a href="{{route('admin.gallery')}}">
                    <i class="fa fa-folder"></i> <span>Gallery Manager</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Resturant & food</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('admin.restaurant')}}"><i class="fa fa-book"></i> <span>Restaurant Manager</span></a></li>
                    <li>
                        <a href="{{route('admin.foods')}}">
                            <i class="fa fa-envelope"></i> <span>Foods Options</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{route('admin.conference')}}">
                    <i class="fa fa-table"></i> <span>Conference</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.customer')}}">
                    <i class="fa fa-table"></i> <span>Customers Options</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Setting</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{route('admin.company')}}">
                            <i class="fa fa-pie-chart"></i>
                            <span>Company Details</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.contact')}}">
                            <i class="fa fa-edit"></i> <span>Mails Manager</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('admin.logs')}}">
                            <i class="fa fa-share"></i> <span>Logs Manager</span>
                        </a>
                    </li>
                    <li><a href="{{route('admin.setting')}}"><i class="fa fa-circle-o text-yellow"></i> <span>Site Settings</span></a></li>

                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>